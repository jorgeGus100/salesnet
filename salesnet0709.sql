-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for salesnet
CREATE DATABASE IF NOT EXISTS `salesnet` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `salesnet`;


-- Dumping structure for table salesnet.internalfunctions
CREATE TABLE IF NOT EXISTS `internalfunctions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `internalfunctions_code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table salesnet.internalfunctions: ~1 rows (approximately)
DELETE FROM `internalfunctions`;
/*!40000 ALTER TABLE `internalfunctions` DISABLE KEYS */;
INSERT INTO `internalfunctions` (`id`, `name`, `internalfunctions_code`) VALUES
	(1, 'function_1', 1);
/*!40000 ALTER TABLE `internalfunctions` ENABLE KEYS */;


-- Dumping structure for table salesnet.internalfunctions_roles
CREATE TABLE IF NOT EXISTS `internalfunctions_roles` (
  `roles_id` int(11) NOT NULL,
  `internalfunctions_id` int(11) NOT NULL,
  KEY `FK_internalfunctions_roles_roles` (`roles_id`),
  KEY `FK_internalfunctions_roles_internalfunctions` (`internalfunctions_id`),
  CONSTRAINT `FK_internalfunctions_roles_internalfunctions` FOREIGN KEY (`internalfunctions_id`) REFERENCES `internalfunctions` (`id`),
  CONSTRAINT `FK_internalfunctions_roles_roles` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table salesnet.internalfunctions_roles: ~0 rows (approximately)
DELETE FROM `internalfunctions_roles`;
/*!40000 ALTER TABLE `internalfunctions_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `internalfunctions_roles` ENABLE KEYS */;


-- Dumping structure for table salesnet.inter_tokens
CREATE TABLE IF NOT EXISTS `inter_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL DEFAULT '0',
  `token` varchar(250) NOT NULL DEFAULT '0',
  `generate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_inter_tokens_users` (`users_id`),
  CONSTRAINT `FK_inter_tokens_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table salesnet.inter_tokens: ~28 rows (approximately)
DELETE FROM `inter_tokens`;
/*!40000 ALTER TABLE `inter_tokens` DISABLE KEYS */;
INSERT INTO `inter_tokens` (`id`, `users_id`, `token`, `generate`) VALUES
	(1, 2, 'd1ff85bc48f684068d58c6c4a1775275f6e8e5a227dbfc5e3475001eb80eacb1c7508a75f70d8620fb2019bc39ac6abe12eb', '2016-09-06 12:50:58'),
	(2, 2, '32447b6a722353cb980a824d82f72b0ea101b3aad13cdd17c5c0452d95e0cfcde00b8c8f369b93fa638301ad60ead2998350', '2016-09-06 13:01:00'),
	(3, 2, 'b337225047fc954862c7ca105653b5156f568b5ca2ebe6e0fdef66d9a943a1fc661d1142a536f75264748561b073bf62d4a8', '2016-09-06 13:04:18'),
	(4, 2, 'eadd9172de6c35a1966aa37bdafff481d3174151c76186ae8edda3d52dba09ea38e31e691de907180b43abb7f4f53d235350', '2016-09-06 13:06:48'),
	(5, 2, 'ab518863e814f4ebca1f8e221b1026009862830d58503c72aac4847eaeb926f5baf3a7e60b8ee57790475769b7b99b8da78c', '2016-09-06 13:23:01'),
	(6, 2, '7a82c98eeae0056f5a59e0005998b950c83488c59cffe19a1a7d8a9b0ce4af3253cc4415b6e5689c997fd0f07c8b443d7533', '2016-09-06 13:23:54'),
	(7, 2, 'adcc656e3a3b05c4adc8b9f5b2c199a836caba07093faf635bc182cc1bc1796d903fd4ba3bd32f5302884d15fe6aa7e8c14f', '2016-09-06 16:23:17'),
	(8, 2, '3c3fbfc00562dc320fe09964e1c8d7e859172b125382706281f89dfdc01d514f12db681542a8cb327784decbfb3892180a33', '2016-09-06 16:27:21'),
	(9, 2, '4f1fde08e5c089e8a9a01e602a7f362b92e641111a51144f1ffb1930ae360db88137526650c03f84c8960a955c905cdaa609', '2016-09-06 16:50:33'),
	(10, 2, '4c43ccf332568402822ee96370fa05254958a48213d78a814290ac0301107511e42475f823c57334a47485b74afe17427208', '2016-09-06 16:51:44'),
	(11, 2, 'a34e6bc44c57d708d2563edfd51561c84cb968ecccd7b308fce99aabf238254800f5377125e93f92d539c2d490d4a6b1edfb', '2016-09-06 16:55:57'),
	(12, 2, 'c598a0f3800db956c27bc2a826a8299c2ec62d4afc058a514dca771096966540a5327d6edb253b715f0457dc8a5067264dee', '2016-09-06 17:09:31'),
	(13, 2, '1148920ade3cf25df463641baf21364b88eb1d7b433334e84f9b89a906cca684ba9a06346c54685d582652502003254e9c35', '2016-09-06 17:50:15'),
	(14, 2, '2db122e6c1b4dcdaf6680aee0fa20a0840d930f40f23ee4fe59ea318e321500ae874972376c9af251e7396aac5af46545ae6', '2016-09-06 17:55:07'),
	(15, 2, '5418f75631d13dfef801e6520652addbbe5df288aec7e405cdea6044bbb0474a7c19c3ce0aac0283909afd7343ed6522bea8', '2016-09-06 17:56:03'),
	(16, 2, '212b971ca7c155fdfc003cfd7f8c3bf95ac5a60a5dc03478fd6dac1a00c678ae7d5bbb2d50b0bdd4229cf6e89a54c6a22f16', '2016-09-06 17:57:20'),
	(17, 2, 'f4486dd1bf863e00f4e6c4950833f2a501f4c8bfad90da08620ef6d0d7721de02af35f97cdf3862ff0e2ef44b83db061296b', '2016-09-06 18:31:18'),
	(18, 2, 'd9e178a4003f492815d366a0f812cf269913d9890a6d6542bd10098f517e6bd6d2c476d65c473414bb5f0d410be8b085b3d9', '2016-09-06 18:37:18'),
	(19, 2, 'e24471e83315122712e4f1ee0b3049f9bc6db2869d1470c20f4bd840de9762cd86fb8a3bdcde94611d002b1ed0e8dd91ccf2', '2016-09-06 18:38:35'),
	(20, 2, '14aa58d59d4bd46b4f875ebd771d5e7da622ba0299f8c18ebfd771686873eca9b23ccff4e517fadabb61b48af53a2ce7a1af', '2016-09-06 18:39:17'),
	(21, 2, 'e05f237a3d38b536aa11559c127d38ed12034b5cdf467f28d23fd457ca5882e2217e4aeb6ed0b058073f8adec8799d79f997', '2016-09-06 18:40:44'),
	(22, 2, '66295faba7403988a898bb0e91895c2874599f99fc9c1120f7f34872162827fbe4095ce41974b9f8f7a4702f231981228f94', '2016-09-06 18:41:10'),
	(23, 2, '412c718371ee0056b0bc9c381428409ee60c144d5d6218b7b095c7ce64b869bf3cdb9322b3ba82463edde86bb2e9c4beb9f2', '2016-09-06 18:43:01'),
	(24, 3, 'b3a5aa5d4e41ca2ee8519fd8e9e95a72043dfa98a2854379760a3635c64744bbe4474c0a21abfd7b851f87bb3245fab13d06', '2016-09-06 18:56:38'),
	(25, 4, '915f7098ad7de753dc0aeac3f2b376e68f00ceb54f5d2d8404038e263dec48d46bb8d01676e5f08d75ecadd9bceed6ec214d', '2016-09-06 18:58:08'),
	(26, 3, '04ef44f887a12571e6725ea5fd1b761ded7037a86399f66454827aa5fe7d3a3ddee392f7afdc6e4d486acc52459a78e36dac', '2016-09-06 19:53:27'),
	(27, 3, 'a516cb9821373073cf7ed7152b8a197c1a6580435c662dce45d3532164c096eff92376f9427256e97b25929afa822911b4fd', '2016-09-06 19:53:52'),
	(28, 3, 'ad3f01f04f36a6956e805d05f52b31b1053e824c0765702173bba2aba9c688cd8147a74a7e71f981154f66b39907f2289867', '2016-09-06 19:53:57');
/*!40000 ALTER TABLE `inter_tokens` ENABLE KEYS */;


-- Dumping structure for table salesnet.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table salesnet.roles: ~1 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `role_name`, `created`, `modified`) VALUES
	(1, 'admin', '2016-09-05 13:57:27', '2016-09-05 13:57:27');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for table salesnet.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `FK_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table salesnet.users: ~3 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `lastname`, `username`, `password`, `email`, `role_id`, `created`, `modified`) VALUES
	(2, 'admin', 'admin', 'admin', '$2y$10$4WwlVtiW5Vme3WoVcDF8TuVs0IWHvB0rcTbffBB/y7QaR03p0BnI2', 'admin@admin.com', 1, '2016-09-05 14:05:42', '2016-09-05 20:34:55'),
	(3, 'Wilfredo', 'Vargas', 'wilfredo', '$2y$10$y6Fz8E1/ahypXkHDGOa5wOq2RPDI3hvcGIFXPHhqHsOCT8iGAvfIq', 'vargas.wilfredo@gmail.com', 1, '2016-09-06 22:56:17', '2016-09-06 22:56:17'),
	(4, 'Christian', 'Quispe', 'chris', '$2y$10$Nz7RRd7ZW1N7DoV0b1zCtukg6WMcHHZmVJZdPxDURdwMIoaWwv042', 'christian.xperius@gmail.com', 1, '2016-09-06 22:57:47', '2016-09-06 22:57:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
