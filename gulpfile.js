var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

// SASS
var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];
 gulp.task('sass', function() {
  return gulp.src('./src/scss/app.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./webroot/css'))
    .pipe(browserSync.reload({stream:true}));
});

// BROWSER SYNC
gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "http://salesnet.localhost/project/",
        notify: false
    });
});

gulp.task('default', ['sass', 'browser-sync'], function() {
  gulp.watch(['./src/scss/**/*.scss'], ['sass']);
});