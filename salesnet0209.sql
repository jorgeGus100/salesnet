-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for salesnet
CREATE DATABASE IF NOT EXISTS `salesnet` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `salesnet`;


-- Dumping structure for table salesnet.internalfunctions
CREATE TABLE IF NOT EXISTS `internalfunctions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `internalfunctions_code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table salesnet.internalfunctions: ~0 rows (approximately)
DELETE FROM `internalfunctions`;
/*!40000 ALTER TABLE `internalfunctions` DISABLE KEYS */;
/*!40000 ALTER TABLE `internalfunctions` ENABLE KEYS */;


-- Dumping structure for table salesnet.internalfunctions_roles
CREATE TABLE IF NOT EXISTS `internalfunctions_roles` (
  `roles_id` int(11) NOT NULL,
  `internalfunctions_id` int(11) NOT NULL,
  KEY `FK_internalfunctions_roles_roles` (`roles_id`),
  KEY `FK_internalfunctions_roles_internalfunctions` (`internalfunctions_id`),
  CONSTRAINT `FK_internalfunctions_roles_internalfunctions` FOREIGN KEY (`internalfunctions_id`) REFERENCES `internalfunctions` (`id`),
  CONSTRAINT `FK_internalfunctions_roles_roles` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table salesnet.internalfunctions_roles: ~0 rows (approximately)
DELETE FROM `internalfunctions_roles`;
/*!40000 ALTER TABLE `internalfunctions_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `internalfunctions_roles` ENABLE KEYS */;


-- Dumping structure for table salesnet.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table salesnet.roles: ~0 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for table salesnet.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `FK_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table salesnet.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
