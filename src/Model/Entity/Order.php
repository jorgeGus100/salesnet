<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Order Entity
 *
 * @property int $id
 * @property int $users_id
 * @property string $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\OrderItem[] $order_items
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    /**
     * @return array of allowed values
     */
    public function getStatus(){
        $allowedValues = array(
            0 => 'on hold',
            1 => 'cancelled',
            2 => 'sold'
        );
        return $allowedValues;
    }
    public function isProductInCart($productId){
        $in = TableRegistry::get('OrderItems')->find('all', ['conditions' => ['OrderItems.order_id' => $this->id, 'OrderItems.product_id' => $productId]]);
        if($in->count()){
            return true;
        }
        return false;
    }
    /**
     * to return quantity if product is in cart
     */
    public function inCartQuantity($productId){
        $in = TableRegistry::get('OrderItems')->find('all', ['conditions' => ['OrderItems.order_id' => $this->id, 'OrderItems.product_id' => $productId]]);
        if($in->count()){
            return $in->first()->quantity;
        }
        return false;
    }

    public function getTotal(){
        return $this->total;
    }
}
