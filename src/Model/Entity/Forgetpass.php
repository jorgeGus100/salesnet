<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;


/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $lastname
 * @property string $username
 * @property string $password
 * @property string $email
 * @property int $role_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Role $role
 */
class Forgetpass extends Entity
{
	public function validationDefault(Validator $validator)
    {
    	$validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        return $validator;
    }
    

}
