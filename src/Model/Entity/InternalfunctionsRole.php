<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InternalfunctionsRole Entity
 *
 * @property int $roles_id
 * @property int $internalfunctions_id
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\Internalfunction $internalfunction
 */
class InternalfunctionsRole extends Entity
{

}
