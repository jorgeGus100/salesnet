<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $product_name
 * @property float $product_price
 * @property int $product_code
 * @property int $brands_id
 * @property int $kardexes_id
 *
 * @property \App\Model\Entity\Brand $brand
 * @property \App\Model\Entity\Kardex $kardex
 * @property \App\Model\Entity\ProductsDiscount[] $products_discounts
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    /**
     * getting product pics
     */
    public function getPic(){
        // to return main pic
        if(isset($this->products_pics[0])){
            return $this->products_pics[0]->pic_path;
        } else{
            return null;
        }
    }
    /**
     * to getting kardexes quantity
     */
    public function getQuantity(){
        // to return desired quantity
        if(isset($this->kardexes[0])){
            return $this->kardexes[0]->quantity;
        } else{
            return null;
        }
    }
    /**
     * to getting product discounts
     */
    public function getDiscount(){
        if(isset($this->products_discounts[0])){
            return $this->products_discounts[0]->discount_percentage;
        } else{
            return null;
        }
    }
    /**
     * to getting valid date for product discounts
     */
    public function getEndDate(){
        if(isset($this->products_discounts[0])){
            return $this->products_discounts[0]->end_date;
        } else{
            return null;
        }
    }
    /**
     * to get ratings average
     */
    public function getRating(){
        $ratings = TableRegistry::get('UsersProductsRatings')->find('all',
            [
            'conditions' => ['UsersProductsRatings.product_id' => $this->id]
            ]
        );
        $ratingsCount = $ratings->count();
        $totalRate = 0;
        foreach ($ratings as $rate) {
            $totalRate += $rate->rate_value;
        }
        if($ratingsCount){
            $result = $totalRate / $ratingsCount;
        } else{
            $result = null;
        }
        return $result;
    }
    /**
     * to get availability getting already orderItems in cart
     * regarding to the same product, the phisical updating should be done in OrderController/confirmOrder
     * HERE SHOULD BE THE LOGIC FOR PRODUCT BOOKING
     */
    public function getAvailability(){
        // currently the availability is virtual
        $currentQuantity = $this->kardexes[0]->quantity;
        $orderItems = TableRegistry::get('OrderItems')->find('all', ['conditions' => ['OrderItems.product_id' => $this->id], 'contain' => ['Orders']]);
        // pr($orderItems->toArray());
        $toSubstract = 0;
        foreach ($orderItems->toArray() as $item) {
            // if the order was not confirmed
            if($item->order->status == 0) {
                $toSubstract += $item->quantity;
            }
        }
        return ($currentQuantity - $toSubstract);
    }

    public function getRelatedProducts(){
        $result = array();
        $limit = 5;// limit number to showing
        // pr($this->related_products);
        if(!empty($this->related_products)) {
            $rProdIds = array();
            foreach ($this->related_products as $key => $rProd) {
                array_push($rProdIds, $rProd->related_product_id);
            }
            // getting related products from admin
            $relatedProducts = TableRegistry::get('Products')->find('all')->where(['Products.id IN' => $rProdIds])->limit($limit)->contain(['ProductsPics']);
            $result = $relatedProducts->toArray();
            if($relatedProducts->count() < 5){
                $toAdd = $limit - $relatedProducts->count();
                // getting related products from categories
                // pr($this->categories[0]->id);
                if(isset($this->categories[0])){
                    $prodsCat = TableRegistry::get('ProductsCategories')->find('all')->where(['ProductsCategories.categories_id' => $this->categories[0]->id])->limit($toAdd);
                } else{
                    $prodsCat = TableRegistry::get('ProductsCategories')->find('all')/*->where(['ProductsCategories.categories_id' => $this->categories[0]->id])*/->limit($toAdd);
                }
                $idsProdCat = array();
                foreach ($prodsCat->toArray() as $id) {
                    array_push($idsProdCat, $id->products_id);
                }
                $notInIds = array();
                foreach ($relatedProducts->toArray() as $related) {
                    array_push($notInIds, $related->id);
                }
                array_push($notInIds, $this->id);
                $relatedProductCat = TableRegistry::get('Products')->find('all')->where(['Products.id IN' => $idsProdCat, 'Products.id NOT IN' => $notInIds, 'Products.product_status' => 1])->limit($toAdd)->contain(['ProductsPics']);
                // still lacks number completion, getting related from brands
                $notInIds = array();
                if($toAdd - $relatedProductCat->count() != 0){
                    // distinct for categories
                    foreach ($relatedProductCat as $id) {
                        array_push($notInIds, $id->id);
                    }
                    foreach ($relatedProducts->toArray() as $relatedAdmin) {
                        array_push($notInIds, $relatedAdmin->id);
                    }
                    array_push($notInIds, $this->id);
                }
            }
            if(isset($notInIds) && !empty($notInIds)){
                $relatedProductBrand = TableRegistry::get('Products')->find('all')->where(['Products.brands_id' => $this->brand->id, 'Products.id NOT IN' => $notInIds, 'Products.product_status' => 1])->limit($toAdd - $relatedProductCat->count())->contain(['ProductsPics']);
            } else{
                $relatedProductBrand = TableRegistry::get('Products')->find('all')->where(['Products.brands_id' => $this->brand->id, 'Products.product_status' => 1])->limit((isset($relatedProductCat) ? $toAdd - $relatedProductCat->count() : $limit))->contain(['ProductsPics']);
            }
            // by name
            $words = preg_split('/((^\p{P}+)|(\p{P}*\s+\p{P}*)|(\p{P}+$))/', $this->product_name, -1, PREG_SPLIT_NO_EMPTY);
            if(isset($relatedProductCat)){
                // pr($notInIds);
                $byWords = TableRegistry::get('Products')->find('all')->where(['Products.product_name LIKE' => $words[0].'%', 'Products.product_status' => 1, 'Products.id IS NOT' => $this->id, 'Products.id NOT IN' => $notInIds])->limit($toAdd - ($relatedProductCat->count() - $relatedProductBrand->count()))->contain(['ProductsPics']);
            } else{
                $byWords = TableRegistry::get('Products')->find('all')->where(['Products.product_name LIKE' => $words[0].'%', 'Products.product_status' => 1, 'Products.id IS NOT' => $this->id])->limit($limit)->contain(['ProductsPics']);
            }
            if(isset($relatedProductCat) && ($limit - $relatedProducts->count() != 0)){
                foreach ($relatedProductCat->toArray() as $key => $value) {
                    array_push($result, $value);
                }
            }
            if(isset($relatedProductBrand) && isset($relatedProductCat) && ($limit - ($relatedProducts->count() + $relatedProductCat->count()) != 0)){
                $brand = array();
                foreach ($relatedProductBrand as $prodBrand) {
                    array_push($result, $prodBrand);
                }
            }
            if(count($result) < 5){
                if((isset($relatedProductCat)) && ($toAdd - ($relatedProductCat->count() + $relatedProductBrand->count()) != 0)) {
                    if(!empty($byWords->toArray())) {
                        foreach ($byWords->toArray() as $item) {
                            array_push($result, $item);
                        }
                    }
                }
            }
        }
        else{
            $prodsCat = TableRegistry::get('ProductsCategories')->find('all')->where(['ProductsCategories.categories_id' => $this->categories[0]->id]);
            $result = array();
            $idsProdCat = array();
            foreach ($prodsCat->toArray() as $id) {
                array_push($idsProdCat, $id->products_id);
            }
            $notInIds = array();
            foreach ($prodsCat->toArray() as $related) {
                array_push($notInIds, $related->id);
            }
            array_push($notInIds, $this->id);
            $relatedProductCat = TableRegistry::get('Products')->find('all')->where(['Products.id IN' => $idsProdCat, 'Products.id NOT IN' => $notInIds, 'Products.product_status' => 1])->limit($limit)->contain(['ProductsPics']);
            if($limit > $relatedProductCat->count()){
                $toAdd = $limit - $relatedProductCat->count();
            } else{
                $toAdd = 5;
            }
            if(isset($notInIds) && !empty($notInIds)){
                $relatedProductBrand = TableRegistry::get('Products')->find('all')->where(['Products.brands_id' => $this->brand->id, 'Products.id NOT IN' => $notInIds, 'Products.product_status' => 1])->limit($toAdd)->contain(['ProductsPics']);
            } else{
                $relatedProductBrand = TableRegistry::get('Products')->find('all')->where(['Products.brands_id' => $this->brand->id, 'Products.product_status' => 1])->limit((isset($relatedProductCat) ? $toAdd : $limit))->contain(['ProductsPics']);
            }

            // by name
            $words = preg_split('/((^\p{P}+)|(\p{P}*\s+\p{P}*)|(\p{P}+$))/', $this->product_name, -1, PREG_SPLIT_NO_EMPTY);
            // pr('here');
            if(isset($relatedProductCat)){
                $byWords = TableRegistry::get('Products')->find('all')->where(['Products.product_name LIKE' => $words[0].'%', 'Products.product_status' => 1, 'Products.id IS NOT' => $this->id])->limit($toAdd)->contain(['ProductsPics']);
            } else{
                $byWords = TableRegistry::get('Products')->find('all')->where(['Products.product_name LIKE' => $words[0].'%', 'Products.product_status' => 1, 'Products.id IS NOT' => $this->id])->limit($limit)->contain(['ProductsPics']);
            }
            // pr($relatedProductCat->toArray());
            if(isset($relatedProductCat)){
                foreach ($relatedProductCat->toArray() as $key => $value) {
                    array_push($result, $value);
                }
            }
            if(isset($relatedProductBrand) && isset($relatedProductCat) && ($limit - (0 + $relatedProductCat->count()) != 0)){
                $brand = array();
                foreach ($relatedProductBrand as $prodBrand) {
                    array_push($result, $prodBrand);
                }
            }
            // pr(count($result));
            if(count($result) < 5){
                if((isset($relatedProductCat)) && ($toAdd - ($relatedProductCat->count() + $relatedProductBrand->count()) != 0)) {
                    if(!empty($byWords->toArray())) {
                        foreach ($byWords->toArray() as $item) {
                            array_push($result, $item);
                        }
                    }
                }
            }
        }// empty($this->related_products)
        // pr($result);
        return $result;
    }
}
