<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;


/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $lastname
 * @property string $password
 * @property string $email
 * @property string $gender
 * @property int $role_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Role $role
 */
class User extends Entity
{

    protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }
    /**
     * get gender constants
     */
    public function getGenderValues(){
    	$allowedValues = array(
    		0 => 'woman',
    		1 => 'man'
    	);
    	return $allowedValues;
    }
    /**
     * to get if product was rated
     */
    public function isProductRated($idProduct){
        $table = TableRegistry::get('UsersProductsRatings');
        $isRated = $table->find('all', [
            'conditions' => ['UsersProductsRatings.product_id' => $idProduct, 'UsersProductsRatings.user_id' => $this->id]
        ]);
        return $isRated;
    }
    /**
     * get cart
     */
    public function getCart(){
        // get active cart only
        $order = null;
        if(!empty($this->orders)){
            foreach ($this->orders as $ord) {
                // always get the last unconfirmed one
                if($ord->status == 0){
                    $order = $ord;
                }
            }
        }
        if(isset($order)){
            return $order;
        } else{
            return false;
        }
    }
    /**
     * get product rating by user
     */
    public function getRate($idProduct){
        $table = TableRegistry::get('UsersProductsRatings');
        $isRated = $table->find('all', [
            'conditions' => ['UsersProductsRatings.product_id' => $idProduct, 'UsersProductsRatings.user_id' => $this->id]
        ]);
        if($isRated->count())
            return $isRated->first()->rate_value;
        else
            return false;
    }
}
