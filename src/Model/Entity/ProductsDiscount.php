<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductsDiscount Entity
 *
 * @property int $id
 * @property string $discount_name
 * @property int $discount_percentage
 * @property int $product_id
 * @property \Cake\I18n\Time $init_date
 * @property \Cake\I18n\Time $end_date
 *
 * @property \App\Model\Entity\Product $product
 */
class ProductsDiscount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
