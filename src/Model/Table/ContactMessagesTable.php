<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ContactMessagesTable extends Table {
	public function initialize(array $config) {
		parent::initialize($config);
		$this->addBehavior('Timestamp');
		$this->table('contact_messages');
		$this->displayField('id');
		$this->primaryKey('id');
	}

	/**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
	public function validationDefault(Validator $validator) {
		$validator
		->integer('id')
		->allowEmpty('id', 'create');

		$validator
		->requirePresence('name', 'create')
		->notEmpty('name');

		$validator
		->requirePresence('email', 'create')
		->notEmpty('email');

		$validator
		->requirePresence('message', 'create')
		->notEmpty('message');

		return $validator;
	}
}