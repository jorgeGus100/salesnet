<?php
	namespace App\Model\Table;

	use Cake\ORM\Query;
	use Cake\ORM\RulesChecker;
	use Cake\ORM\Table;
	use Cake\Validation\Validator;

	class UsersProductsRatingsTable extends Table{
		/**
		 * [initialize description]
		 */
		public function initialize(array $config){
			parent::initialize($config);
			$this->table('users_products_ratings');
			$this->displayField('id');
			$this->primaryKey('id');

			$this->belongsTo('Users', [
				'foreignKey' => 'user_id',
				'joinType' => 'INNER'
			]);

			$this->belongsTo('Products', [
				'foreignKey' => 'product_id',
				'joinType' => 'INNER'
			]);
		}
		/**
     * Default validation rules.
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }
		/**
		 * Returns a rules checker object that will be used for validating  application integrity.
		 */
		public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
	}