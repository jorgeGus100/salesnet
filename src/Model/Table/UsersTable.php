<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Orders', [
            'className' => 'Orders',
        ]);
        $this->addBehavior('Captcha', [
        'field' => 'securitycode',
        'message' => 'Incorrect captcha code value'
        ]);
        $this->hasOne('Preferences');
        $this->hasMany('UsersProductsRatings', [
            'className' => 'UsersProductsRatings'
        ]);
        $this->hasMany('Orders', [
            'className' => 'Orders',
            'foreignKey' => 'users_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('lastname', 'create')
            ->notEmpty('lastname');
        $validator
            ->requirePresence('gender', 'create')
            ->notEmpty('gender');    
        
        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');
         $validator->add(
            'password',
            ['lenght'=>[
                'rule'=>['minLength', 6],
                'message'=> 'Password needs to be 6 characters or more',
                ]]
            );
         
             $validator->add(
            'password',
            ['restrict'=>[
                'rule'=>['custom', '/^[a-zA-Z0-9 \! \( \) \[ \] \{ \} \? \- \_ \/ \. \Q \ \E ]*$/'],
                'message'=> 'The password must have only letters and characters "!()[]{}.\"',
            ]]
            );
          /* 
         $validator->add(
            'password',
            ['restrict'=>[
                'rule'=>['custom', '/^(?=.*[a-z])(?=.*[A-Z])/'],
                'message'=> 'The password needs to include one capital letter and one lower case',
            ]]
            );
          */
         $validator->add(
            'password',
            ['restrictChar'=>[
                'rule'=>['custom', '/^(?=.*[\! \( \) \[ \] \{ \} \- \_ \. \/ \Q \ \E \?])(?=.*[a-z])(?=.*[A-Z])/'],
                'message'=> 'include one character "!()[]{}.\" one capital letter and one lower case',
            ]]
            );
        
         $validator->add('password', [
            'compare' => [
            'rule' => ['compareWith', 'confirm_password'],
            'message'=> 'The password needs to match',
    ]
            ]);
        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');
        $validator->add(
        'email', 
                ['unique' => [
                    'rule' => 'validateUnique', 
                    'provider' => 'table', 
                    'message' => 'Email already in use']
                ]
            );    

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
       
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }
    public function isOwnedBy($userId, $authUser) {
        return $userId == $authUser->id;
    }
    
}
