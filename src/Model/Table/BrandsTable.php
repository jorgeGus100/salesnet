<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Brands Model
 *
 * @method \App\Model\Entity\Brand get($primaryKey, $options = [])
 * @method \App\Model\Entity\Brand newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Brand[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Brand|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Brand patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Brand[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Brand findOrCreate($search, callable $callback = null)
 */
class BrandsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->hasMany('Products', [
            'foreignKey' => 'brands_id',
            'joinType' => 'INNER'
        ]);

        $this->table('brands');
        $this->displayField('brand_name');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('brand_name');

        $validator
            ->integer('code')
            ->allowEmpty('code');

        $validator
            ->allowEmpty('logo');

        $validator
            ->allowEmpty('description');

        $validator
            ->dateTime('creation')
            ->allowEmpty('creation');

        return $validator;
    }
    /**
     * featured brands
     */
    public function getFeaturedBrands(){
        $brands = $this->find('all')->where(['brand_status' => 1])->order('rand()')->limit(5);
        // $brands = $this->find('all')->first();
        return $brands->toArray();
    }
}
