<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Internalfunctions Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Roles
 *
 * @method \App\Model\Entity\Internalfunction get($primaryKey, $options = [])
 * @method \App\Model\Entity\Internalfunction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Internalfunction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Internalfunction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Internalfunction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Internalfunction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Internalfunction findOrCreate($search, callable $callback = null)
 */
class InternalfunctionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('internalfunctions');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsToMany('Roles', [
            'foreignKey' => 'internalfunction_id',
            'targetForeignKey' => 'role_id',
            'joinTable' => 'internalfunctions_roles'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('internalfunctions_code')
            ->requirePresence('internalfunctions_code', 'create')
            ->notEmpty('internalfunctions_code');

        return $validator;
    }
}
