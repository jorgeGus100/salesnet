<?php
	namespace App\Model\Table;

	use Cake\ORM\Query;
	use Cake\ORM\RulesChecker;
	use Cake\ORM\Table;
	use Cake\Validation\Validator;

	class RelatedProductsTable extends Table {
		public function initialize(array $config) {
        parent::initialize($config);

        $this->table('related_products');
        $this->addBehavior('Timestamp');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Products', [
        	'foreignKey' => 'product_id'
      	]);
    }

    /**
     * Default validation rules.
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }
    /**
		 * Returns a rules checker object that will be used for validating  application integrity.
		 */
		public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['related_product_id'], 'Products'));

        return $rules;
    }
	}