<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InternalfunctionsRoles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Roles
 * @property \Cake\ORM\Association\BelongsTo $Internalfunctions
 *
 * @method \App\Model\Entity\InternalfunctionsRole get($primaryKey, $options = [])
 * @method \App\Model\Entity\InternalfunctionsRole newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InternalfunctionsRole[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InternalfunctionsRole|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InternalfunctionsRole patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InternalfunctionsRole[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InternalfunctionsRole findOrCreate($search, callable $callback = null)
 */
class InternalfunctionsRolesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('internalfunctions_roles');

        $this->belongsTo('Roles', [
            'foreignKey' => 'roles_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Internalfunctions', [
            'foreignKey' => 'internalfunctions_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['roles_id'], 'Roles'));
        $rules->add($rules->existsIn(['internalfunctions_id'], 'Internalfunctions'));

        return $rules;
    }
}
