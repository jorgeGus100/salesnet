<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Brands
 * @property \Cake\ORM\Association\BelongsTo $Kardexes
 * @property \Cake\ORM\Association\HasMany $ProductsDiscounts
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null)
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('products');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Brands', [
            'foreignKey' => 'brands_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Kardexes', [
            'foreignKey' => 'products_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ProductsDiscounts', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('ProductsPics', [
            'foreignKey' => 'products_id'
        ]);
        $this->hasMany('UsersProductsRatings', [
            'className' => 'UsersProductsRatings'
        ]);
        /*$this->hasMany('ProductsCategories', [
            'className' => 'ProductsCategories',
            'foreignKey' => 'products_id'
        ]);*/
        $this->belongsToMany('Categories', [
            'foreignKey' => 'products_id',
            'targetForeignKey' => 'categories_id',
            'joinTable' => 'products_categories'
        ]);
        $this->hasMany('OrderItems', [
            'className' => 'OrderItems',
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('RelatedProducts', [
            'className' => 'RelatedProducts',
            'foreignKey' => 'product_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('product_name', 'create')
            ->notEmpty('product_name');

        $validator
            ->decimal('product_price')
            ->requirePresence('product_price', 'create')
            ->notEmpty('product_price');

        $validator
            ->requirePresence('product_code', 'create')
            ->notEmpty('product_code');
        $validator
            ->add('product_code', 'validValue', [
                    'rule' => array('custom', '/^[a-z0-9]{3,}$/i'),
                    'message' => __('Only letters and integers, min 3 characters')
                ])
            ;

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['brands_id'], 'Brands'));
        $rules->add($rules->existsIn(['kardexes_id'], 'Kardexes'));

        return $rules;
    }
    /**
     * to get featured products
     */
    public function getFeaturedProducts(){
        $products = $this->find('all')->where(['product_status' => 1])->order('rand()')->limit(5)->contain(['ProductsPics', 'Kardexes', 'Brands', 'Categories', 'ProductsDiscounts']);
        return $products;
    }
}
