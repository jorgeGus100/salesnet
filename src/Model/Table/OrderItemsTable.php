<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * OrderItems Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Products
 * @property \Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\OrderItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrderItem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrderItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrderItem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrderItem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrderItem findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrderItemsTable extends Table
{
    public $trackingQuantity = null;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('order_items');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmpty('quantity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }
    public function beforeSave(Event $event, \App\Model\Entity\OrderItem $entity, $options){
        // pr($entity->isNew());
        if($entity->isNew()){
            // pr('is new it does not have id');
        } else{
            // pr('is new yes it has id');
            $oldEntity = $this->get($entity->id);
            $this->trackingQuantity = $oldEntity->quantity;
        }
    }

    public function afterSave(Event $event, \App\Model\Entity\OrderItem $entity, $options){
        // updating order's total price
        $order = TableRegistry::get('Orders')->get($entity->order_id);
        $items = $this->find('all', ['conditions' => ['OrderItems.order_id' => $entity->order_id], 'contain' => ['Products']]);
        $updatedTotal = 0;
        if(!empty($items)){
            foreach ($items as $item) {
                // pr($item->product->product_price);
                // pr($item->quantity);
                $updatedTotal += ($item->product->product_price * $item->quantity);
            }
        }
        $order->total = $updatedTotal;
        TableRegistry::get('Orders')->save($order);
        // updating kardexes
        $this->updatingKardexes($entity);
    }
    public function updatingKardexes($entity){
        if($this->trackingQuantity != null){
            $oldEntity = $this->get($entity->id, ['contain' => ['Products', 'Products.Kardexes']])->product->kardexes[0];
            $oldQuantity = $oldEntity->quantity;
            if($this->trackingQuantity > $entity->quantity){
                $updatedInventory = $oldQuantity + $entity->quantity;
            } else{
                $updatedInventory = $oldQuantity - $entity->quantity;
            }
            $oldEntity->quantity = $updatedInventory;
            // $this->Products->Kardexes->save($oldEntity);
        } else{
            $orderItem = $this->get($entity->id, ['contain' => ['Products', 'Products.Kardexes']]);
            $updatedInventory = $orderItem->product->kardexes[0]->quantity - $entity->quantity;
            $orderItem->product->kardexes[0]->quantity = $updatedInventory;
            // $this->Products->Kardexes->save($orderItem->product->kardexes[0]);
        }
        // die();
    }

    public function afterDelete(Event $event,  $entity){
        // updating total
        $order = TableRegistry::get('Orders')->get($entity->order_id);
        $items = $this->find('all', ['conditions' => ['OrderItems.order_id' => $entity->order_id], 'contain' => ['Products']]);
        $updatedTotal = 0;
        if(!empty($items)){
            foreach ($items as $item) {
                $updatedTotal += ($item->product->product_price * $item->quantity);
            }
        }
        $order->total = $updatedTotal;
        TableRegistry::get('Orders')->save($order);
    }

    public function beforeDelete(Event $event, $entity){
        // updating kardexes
        // due the quantities are virtuals until the order is confirmed
        /*$loadedEntity =  $this->get($entity->id, ['contain' => ['Products' => ['Kardexes']]]);
        $updatedInventory = $loadedEntity->product->kardexes[0]->quantity + $loadedEntity->quantity;
        $loadedEntity->product->kardexes[0]->quantity = $updatedInventory;
        $this->Products->Kardexes->save($loadedEntity->product->kardexes[0]);*/
    }
}
