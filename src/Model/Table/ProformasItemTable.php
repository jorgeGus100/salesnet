<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProformasItem Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Proformas
 * @property \Cake\ORM\Association\BelongsTo $Products
 * @property \Cake\ORM\Association\BelongsTo $Discounts
 *
 * @method \App\Model\Entity\ProformasItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProformasItem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProformasItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProformasItem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProformasItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProformasItem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProformasItem findOrCreate($search, callable $callback = null)
 */
class ProformasItemTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('proformas_item');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Proformas', [
            'foreignKey' => 'proforma_id'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id'
        ]);
        $this->belongsTo('Discounts', [
            'foreignKey' => 'discount_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('product_quantity')
            ->allowEmpty('product_quantity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['proforma_id'], 'Proformas'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['discount_id'], 'Discounts'));

        return $rules;
    }
}
