<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Kardex'), ['action' => 'edit', $kardex->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Kardex'), ['action' => 'delete', $kardex->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kardex->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Kardexes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Kardex'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="kardexes view large-9 medium-8 columns content">
    <h3><?= h($kardex->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $kardex->has('product') ? $this->Html->link($kardex->product->id, ['controller' => 'Products', 'action' => 'view', $kardex->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($kardex->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity') ?></th>
            <td><?= $this->Number->format($kardex->quantity) ?></td>
        </tr>
        <tr>
            <th><?= __('Unity') ?></th>
            <td><?= $this->Number->format($kardex->unity) ?></td>
        </tr>
    </table>
</div>
