<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Kardex'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="kardexes index large-9 medium-8 columns content">
    <h3><?= __('Kardexes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('quantity') ?></th>
                <th><?= $this->Paginator->sort('unity') ?></th>
                <th><?= $this->Paginator->sort('products_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($kardexes as $kardex): ?>
            <tr>
                <td><?= $this->Number->format($kardex->id) ?></td>
                <td><?= $this->Number->format($kardex->quantity) ?></td>
                <td><?= $this->Number->format($kardex->unity) ?></td>
                <td><?= $kardex->has('product') ? $this->Html->link($kardex->product->id, ['controller' => 'Products', 'action' => 'view', $kardex->product->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $kardex->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $kardex->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $kardex->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kardex->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
        
    </div>
</div>
