<div class="row">
	<h2 class="page-title"><?php echo __('Order Form'); ?></h2>
	<div class="shipping-info medium-12 columns">
		<h4><?php echo __('Ship to'); ?>:</h4>
		<table class="columns">
			<tbody>
				<tr>
					<th>Name:</th>
					<td><?php echo $order->user->name; ?></td>
				</tr>
				<tr>
					<th>Last name:</th>
					<td><?php echo $order->user->lastname; ?></td>
				</tr>
				<tr>
					<th>Email:</th>
					<td><?php echo $order->user->email; ?></td>
				</tr>
				<tr>
					<th class="edit-information"><a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'profile', $user->id]); ?>">Change information</a></th>
				</tr>
			</tbody>
		</table>
		<div class="row review-items">
			<h3>Review Items</h3>
			<?php foreach($orderItems as $item): ?>
				<div class="row product-item">
					<div class="large-4 columns product-images">
						<?php if(!empty($item->product->products_pics)): ?>
							<img src="<?php echo $this->request->webroot.'upload/products_img/'.$item->product->products_pics[0]->pic_path; ?>">
						<?php else: ?>
							<img src="http://placehold.it/350x250">
						<?php endif; ?>
					</div><!-- product-images -->
					<div class="large-8 columns content">
						<!-- <h3 class="product-name small-12 columns"><?= h($item->product->product_name) ?></h3> -->
						<h3 class="product-name small-12 columns"><?php echo $this->Html->link($item->product->product_name, ['controller' => 'Products', 'action' => 'guestView', $item->product->id]); ?></h3>
						<div class="box_resume_2">
							<div>
								<?php echo $this->Form->create('AddCart', ['url' => ['controller' => 'OrderItems', 'action' => 'add'], 'class' => 'add-cart-form']); ?>
									<?php echo $this->Form->input('quantity', ['type' => 'number', 'min' => '1', 'max' => ((!empty($item->product->kardexes) ? $item->product->kardexes[0]->quantity : '')), 'value' => $item->quantity]); ?>
								<p class="error" style="display: none;"><?php echo __('The field cannot be empty.'); ?></p>
								<?php echo $this->Form->input('product_id', ['value' => $item->product->id, 'type' => 'hidden']); ?>
								<p class="availability">Available: <?php echo (!empty($item->product->kardexes)) ? $item->product->kardexes[0]->quantity : ''; ?></p>
								<?php echo $this->Form->end(); ?>
								<div class="remove">
									<form action="<?php echo $this->Url->build(['controller' => 'order_items', 'action' => 'delete_from_order_form']) ?>"><input type="hidden" name="order_item_id" value="<?php echo $item->id; ?>"></form>
									<a href="#"><?php echo __('Remove'); ?></a>
									<?php echo $this->Html->image('loading.gif', ['class' => 'loading-gif']); ?></p>
								</div>
							</div>
							<div class="product-price">
								<p>Price: <?php echo $this->Number->currency($item->product->product_price); ?></p>
							</div>
						</div><!-- box_resume_2 -->
						<?php if(!empty($item->product->products_discounts) && ($item->product->products_discounts[0]->discount_percentage != 0)): ?>
						<fieldset>
							<!-- <legend>Discount</legend> -->
							<?php //pr($item->product->products_discounts[0]->discount_percentage); ?>
								<p class="discount"><span class="discount-name"><?php echo $item->product->products_discounts[0]->discount_name?></span> <?php echo $item->product->products_discounts[0]->discount_percentage ?>% <span>off</span></p>
						</fieldset>
						<?php endif; ?>
					</div>
					<!-- /.large-8 columns content -->
				</div>
			<?php endforeach; ?>
		</div><!-- review-items -->
		<div class="medium-5 columns summary">
			<h3>Items (<?php echo count($orderItems); ?>): <span> <?php echo $this->Number->currency($order->total); ?></span></h3>
			<?php if($totalDiscounts != 0): ?>
				<fieldset>
	            <!-- <legend>Discount if it applies</legend> -->
	            <p class="total-discounts">Discounts: <span>-  <?php
	                echo $this->Number->currency($totalDiscounts);
	            ?></span></p>
	        </fieldset>
        <?php endif; ?>
        <h3 class="order-total">Total: <span><?php echo $this->Number->currency($orderTotal); ?></span></h3>
        <?php echo $this->Form->postLink(__('Confirm and Send Email'), ['action' => 'confirmOrder', $order->id], ['class' => 'button confirm-btn']); ?>
		</div><!-- summary -->
	</div>
</div>