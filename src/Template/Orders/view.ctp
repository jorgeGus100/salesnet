<?php if(!empty($filteredProducts)): ?>
<div class="row shopping-cart-container">
    <h1><?php echo __('Shopping Cart'); ?></h1>
    <?php foreach ($filteredProducts as $product): ?>
        <div class="row product-item">
            <div class="large-4 columns product-images">
                <?php if(!empty($product->products_pics)): ?>
                    <img src="<?php echo $this->request->webroot.'upload/products_img/'.$product->products_pics[0]['pic_path'] ?>" alt="">
                <?php else: ?>
                    <img src="http://placehold.it/350x250">
                <?php endif; ?>
            </div>
            <div class="products view large-8 medium-8 columns">
                <h4 class="product-name"><?php echo $this->Html->link($product->product_name, ['controller' => 'products', 'action' => 'guestView', $product->id]);//echo $product->product_name; ?></h4>
                <div class="specifications medium-4 columns">
                    <div class="description">
                        <p><?php echo $product->product_description; ?></p>
                    </div>
                    <?php if($product->products_discounts[0]->discount_percentage != 0): ?>
                        <div class="discounts">
                            <?php if(!empty($product->products_discounts)): ?>
                                <!-- <fieldset> -->
                                    <!-- <legend>Discount</legend> -->
                                    <p class="discount"><span><?php echo __('Discount'); ?>: </span><?php echo $product->products_discounts[0]->discount_percentage; ?>%</p>
                                <!-- </fieldset> -->
                            <?php else: ?>
                                <!-- <fieldset>
                                    <legend>Discount</legend>
                                </fieldset> -->
                            <?php endif; ?>
                        </div><!-- discounts -->
                    <?php endif; ?>
                </div>
                <div class="quantity medium-4 columns">
                    <label for="quantity" class="small-4 columns">Quantity:</label>
                    <?php
                            $orderItemId = null;
                            foreach ($product->order_items as $orderItem) {
                                if($orderItem->order_id == $order->id){
                                    $orderItemId = $orderItem;
                                }
                            }
                        ?>
                    <?php echo $this->Form->create($orderItem, ['url' => ['controller' => 'OrderItems', 'action' => 'edit', $orderItem->id]]); ?>
                    <?php echo $this->Form->hidden('product_id', ['value' => $orderItem->product_id]); ?>
                    <?php echo $this->Form->hidden('order_id', ['value' => $orderItem->order_id]); ?>
                    <?php //pr($orderItem); ?>
                    <?php echo $this->Form->input('quantity', ['label' => false, 'class' => 'quantity-input small-4 columns', 'min' => 1, 'max' => ((!empty($product->kardexes)) ? $product->kardexes[0]->quantity : ''), 'autocomplete' => 'off', 'value' => $orderItem->quantity]); ?>
                    <?php echo $this->Form->button(__('Update'), ['class' => 'update-quantity button small-4 columns']); ?>
                    <?php echo $this->Html->image('loading.gif', ['class' => 'loading-gif']); ?>
                    <?php echo $this->Html->image('saved-icon.png', ['class' => 'success']); ?>
                    <?php echo $this->Form->end(); ?>
                    <?php //if(!empty($product->kardexes)): ?>
                        <!-- <p><?php //echo $product->kardexes[0]->quantity; ?> Available</p> -->
                        <p><?php echo $product->getAvailability(); ?> Available</p>
                    <?php //endif; ?>
                    <?php
                        $orderItemId = null;
                        foreach ($product->order_items as $orderItem) {
                            if($orderItem->order_id == $order->id){
                                $orderItemId = $orderItem;
                            }
                        }
                    ?>
                    <div class="remove"><?php echo $this->Form->postLink(__('Remove'), ['controller' => 'OrderItems', 'action' => 'delete', $orderItemId->id], ['confirm' => __('Are you sure want to delete the product')]); ?></div>
                </div>
                <div class="price medium-4 columns">
                    <p><strong>Price</strong>: <?php echo $this->Number->currency($product->product_price); ?></p>
                </div>
            </div>
        </div><!-- product-item -->
    <?php endforeach;// filteredProducts ?>
    <div class="medium-5 columns summary">
        <h3>SubTotal (<?php echo count($filteredProducts); ?> Items): <span><?php echo $this->Number->currency($order->total); ?></span></h3>
            <?php if($totalDiscounts != 0): ?>
                <p class="total-discounts">Discounts: <span>-  <?php
                    echo $this->Number->currency($totalDiscounts);
                ?></span></p>
            <?php endif; ?>
        <h3 class="order-total">Total: <span><?php echo $this->Number->currency($orderTotal); ?></span></h3>
            <?php echo $this->Html->link(__('Continue Shopping'), ['controller' => 'pages', 'action' => 'home'], ['class' => 'secondary button']); ?>
            <?php echo $this->Form->postLink(__('Empty Cart'), ['action' => 'delete', $order->id], ['confirm' => __('Are you sure to want to delete the entire order'), 'class' => 'alert button']); ?>
            <?php echo $this->Form->postLink(__('Confirm order'), ['action' => 'orderForm', $order->id], ['class' => 'button']); ?>
    </div>
</div>
<?php else: ?>
    <div class="row shopping-cart-container empty-cart">
        <h1><?php echo __('Your Shopping Cart is empty'); ?></h1>
        <?php echo $this->Html->link(__('Start shopping'), ['controller' => 'pages', 'action' => 'home'], ['class' => 'button']); ?>
    </div>
<?php endif; ?>
<?php echo $this->Html->script('jquery-ui.min', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('jquery-ui.min'); ?>