<?php $this->Html->addCrumb('Brands', ['controller' => 'Brands', 'action' => 'guestView']); ?>
<?php $this->Html->addCrumb('View', null); ?>

<div class="row">
<div class="categories view large-12 medium-8 columns content float-left " id="brands-products">

    <h3><?= h($brand->brand_name) ?></h3>
    <?php //pr($brand); ?>

</div>
<div class="large-12 columns featured" id="featured-products">
          <!-- <p>SUGGESTED PRODUCTS</p> -->
          <?php foreach($brand->products as $product): ?>
            <div class="col5-unit columns product">
              <a href="<?php
                echo $this->Url->build([
                  "controller" => "Products",
                  "action" => "guestView",
                  $product->id
                ]);
              ?>">
                <?php if(!empty($product->products_pics)): ?>
                  <img src="<?php echo $this->request->webroot.'upload/products_img/'.$product->products_pics[0]['pic_path']; ?>" class="cover">
                <?php else: ?>
                  <img src="http://placehold.it/150x150" alt="">
                <?php endif; ?>
                <div class="product-data">
                  <p class="product-name"><?php echo $product->product_name; ?></p>
                  <p class="produc-price text-center">price: <?php echo $this->Number->currency($product->product_price); ?></p>
                  <p class="product-rate">
                    <?php if($product->getRating()): ?>
                      <div class="product-rating" data-rating="<?php echo $product->getRating(); ?>" data-readonly="true"></div>
                    <?php else: ?>
                      <span class="star-rating"><?php echo $this->Html->image('blank-star-rating.png'); ?></span>
                    <?php endif; ?>
                  </p>
                </div>
                <?php echo $this->element('add_cart_btn', ['product' => $product, 'loggedUser' => (isset($user) ? $user : '')]); ?>
              </a>
            </div>
          <?php endforeach; ?>
        </div>
<?php echo $this->Html->script('jquery.star-rating-svg', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('star-rating-svg'); ?>
<?php echo $this->Html->script('jquery.zoom', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->script('jquery-ui.min', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('jquery-ui.min'); ?>