<?php
    $this->Html->addCrumb('Brands', ['controller' => 'Brands', 'action' => 'index']);
    $this->Html->addCrumb('Edit', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'trash', $brand->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $brand->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Brands'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="brands form large-9 medium-8 columns content">
    <?= $this->Form->create($brand, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Edit Brand') ?></legend>
        <?php
            echo $this->Form->input('brand_name');
            echo $this->Form->input('code');
            echo $this->Form->input('description');
            echo $this->Form->input('upload', ['type' => 'file']);
            if(!empty($brand->logo)){
                $pic=$this->request->webroot.$brand->logo;
            }else{
                $pic=$this->request->webroot.'/upload/empty.png';
            }
            echo '<div id="image-holder"><img src="'.$pic.'"></div>';
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'button submit']) ?>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->script('upload',['block' => true]); ?>