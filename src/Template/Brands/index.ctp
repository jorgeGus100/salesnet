<?php $this->Html->addCrumb('Brands', null); ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Brand'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="brands index large-9 medium-8 columns content">
    <h3><?= __('Brands') ?></h3>
    <table cellpadding="0" cellspacing="0" class="text-center">
        <thead>
            <tr>
               
                <th class="text-center"><?= $this->Paginator->sort('brand_name') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('code') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('created') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('modified') ?></th>
                <!-- <th class="text-center"><?= $this->Paginator->sort('creation') ?></th> -->
                <th class="actions text-center"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($brands as $brand): ?>
            <tr>
                <td><?=   $this->Html->link(h($brand->brand_name), ['action' => 'view', $brand->id]) ?></td>
                <td><?=   h($brand->code) ?></td>
                <td><?=   $brand->created ?></td>
                <td><?=   h($brand->modified) ?></td>
                <!-- <td><?= h($brand->creation) ?></td> -->
                <td class="actions">
                   
                    <a href="<?php echo $this->Url->build(['controller' => 'brands', 'action' => 'view/'.$brand->id]) ?>"><i class="fi-eye"></i></a>
                    <a href="<?php echo $this->Url->build(['controller' => 'brands', 'action' => 'edit/'.$brand->id]) ?>"><i class="fi-pencil"></i></a>
                    <a href="<?php echo $this->Url->build(['controller' => 'brands', 'action' => 'trash/'.$brand->id]) ?>" onclick="if (confirm('Are you sure you wish to delete this brand?')) { return true; } return false;"><i class="fi-trash"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
        
    </div>
</div>
