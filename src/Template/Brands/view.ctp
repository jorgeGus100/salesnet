<?php
    $this->Html->addCrumb('Brands', ['controller' => 'Brands', 'action' => 'index']);
    $this->Html->addCrumb('View', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Brand'), ['action' => 'edit', $brand->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Brand'), ['action' => 'delete', $brand->id], ['confirm' => __('Are you sure you want to delete # {0}?', $brand->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Brands'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Brand'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="brands view large-9 medium-8 columns content">
    <h3><?= h($brand->brand_name) ?></h3>
    <table class="vertical-table">
        
        <tr>
            <th></th>
            <td><?php 
                if(!empty($brand->logo)){
                $pic=$this->request->webroot.$brand->logo;
            }else{
                $pic=$this->request->webroot.'/upload/empty.png';
            }
             echo '<img src="'.$pic.'">'; ?></td>
        </tr>
        
        <tr>
            <th><?= __('Code') ?></th>
            <td><?= $this->Number->format($brand->code) ?></td>
        </tr>
        <tr>
            <th><?= __('Creation') ?></th>
            <td><?= h($brand->creation) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($brand->description)); ?>
    </div>
</div>
