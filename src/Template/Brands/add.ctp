<?php
    $this->Html->addCrumb('Brands', ['controller' => 'Brands', 'action' => 'index']);
    $this->Html->addCrumb('New', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Brands'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="brands form large-9 medium-8 columns content">
    <?= $this->Form->create($brand, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Add Brand') ?></legend>
        <?php
            echo $this->Form->input('brand_name');
            echo $this->Form->input('code');
            echo $this->Form->input('description');
            echo $this->Form->input('upload', ['type' => 'file']);
            echo '<div id="image-holder"></div>';
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), [
        'class' => 'button submit'
    ]) ?>
    <?= $this->Form->end() ?>
</div>
<?= $this->Html->script('jquery.min.js') ?>
<?php echo $this->Html->script('upload.js') ?>
