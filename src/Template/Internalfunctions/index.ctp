<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Internalfunction'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="internalfunctions index large-9 medium-8 columns content">
    <h3><?= __('Internalfunctions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('internalfunctions_code') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($internalfunctions as $internalfunction): ?>
            <tr>
                <td><?= $this->Number->format($internalfunction->id) ?></td>
                <td><?= h($internalfunction->name) ?></td>
                <td><?= $this->Number->format($internalfunction->internalfunctions_code) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $internalfunction->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $internalfunction->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $internalfunction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $internalfunction->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
