<!DOCTYPE html>
<html>
<head>
	<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('foundation-icons/foundation-icons.css') ?>
    <?= $this->Html->css('app.css') ?>
    <?= $this->fetch('css') ?>
</head>

<body>
<section id="body-content">
	
	<div class="screen">
		<h3 class="title_1"><?= $title ?></h3>

		<div class="content_text_1">
			<?php echo $content; ?>
		</div>
	</div>
</section>	
</body>
</html>