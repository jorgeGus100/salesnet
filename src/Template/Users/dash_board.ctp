<style type="text/css">
  .breadcrumb-wrapper.columns{ display: none; }
</style>
<div id="admin">
<div class="index large-12 medium-8 columns content">
    <a href="<?php echo $this->Url->build(['controller' => 'Users','action' => 'index']); ?>"><div class="dashPanel dPanRed large-5 columns"><h5>Users</h5> <i class="iconDash fi-torsos-male-female large"></i></div></a>
    <a href="<?php echo $this->Url->build(['controller' => 'Categories','action' => 'index']); ?>"><div class="dashPanel dPanOrange large-4 columns"><h5>Categories</h5><i class="iconDash fi-thumbnails large"></i></div></a>
    <a href="<?php echo $this->Url->build(['controller' => 'Brands','action' => 'index']); ?>"><div class="dashPanel dPanGreen large-3 columns"><h5>Brands</h5><i class="iconDash fi-ticket large"></i></div></a>
</div>
<div class="index large-12 medium-8 columns content">
    <a href="<?php echo $this->Url->build(['controller' => 'Products','action' => 'index']); ?>"><div class="dashPanel dPanBlue large-4 columns"><h5>Products</h5><i class="iconDash fi-shopping-bag large"></i></div></a>
    <a href="<?php echo $this->Url->build(['controller' => 'ProductsDiscounts','action' => 'index']); ?>"><div class="dashPanel dPanGreen large-3 columns"><h5>Discounts</h5><i class="iconDash fi-burst-sale large"></i></div></a>
    <a href="<?php echo $this->Url->build(['controller' => 'Roles','action' => 'index']); ?>"><div class="dashPanel dPanRed large-3 columns"><h5>Roles</h5><i class="iconDash fi-torso-business large"></i></div></a>
    <a href="<?php echo $this->Url->build(['controller' => 'BannerPics','action' => 'index']); ?>"><div class="dashPanel dPanOrange large-2 columns"><h5>Banner</h5><i class="iconDash fi-photo large"></i></div></a>
</div>
<div class="large-12 columns featured" id="featured-products">
          <!-- <p>SUGGESTED PRODUCTS</p> -->
          <?php foreach($featuredProducts as $fProduct): ?>
            <div class="col5-unit columns product">
              <a href="<?php
                echo $this->Url->build([
                  "controller" => "Products",
                  "action" => "edit",
                  $fProduct['id']
                ]);
              ?>">
                <?php if(!empty($fProduct['products_pics'])): ?>
                  <img src="<?php echo $this->request->webroot.'upload/products_img/'.$fProduct['products_pics'][0]['pic_path']; ?>" class="cover">
                <?php else: ?>
                  <img src="http://placehold.it/150x150" alt="">
                <?php endif; ?>
                <div class="product-data">
                  <p class="product-name"><?php echo $fProduct['product_name']; ?></p>
                  <p class="produc-price text-center">price: $<?php echo $fProduct['product_price']; ?></p>
                  <p class="product-rate">Rate:
                    <?php if($fProduct->getRating()): ?>
                      <div class="product-rating" data-rating="<?php echo $fProduct->getRating(); ?>" data-readonly="true"></div>
                    <?php else: ?>
                      <span class="star-rating"><?php echo $this->Html->image('blank-star-rating.png'); ?></span>
                    <?php endif; ?>
                  </p>
                </div>
              </a>
            </div>
          <?php endforeach; ?>
        </div><!-- featured-products -->
</div>
<?php echo $this->Html->script('jquery.star-rating-svg', ['block' => 'scriptBottom']); ?>