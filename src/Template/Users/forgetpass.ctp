<div class="forgetpass form row">
	<div class="medium-6 columns medium-centered">
		<?= $this->Flash->render('auth') ?>
		<?= $this->Form->create() ?>
			<h3><?php echo __('Forgot your password'); ?> ?</h3>
			<p><?php echo __('Enter your email address. We will send you a link to reset your password.'); ?></p>
		    <fieldset>
		        <!-- <legend><?= __('Please enter your email address. We will send you a link to reset your password.') ?></legend> -->
		        <div class="medium-10 columns medium-centered">
		        	<div class="medium-4 columns"><?php echo __('Email Address'); ?>:</div>
		        	<div class="medium-8 columns"><?= $this->Form->input('email', ['label' => false]) ?></div>
		        </div>
		        <?php if(isset($msg)): ?>
		        	<p class="text-center confirm"><?php echo $msg; ?></p>
		        <?php endif; ?>

		    </fieldset>
		<?= $this->Form->button(__('Send Email'), ['class' => 'secondary button submit']); ?>
		<?= $this->Form->end() ?>
	</div>
</div>