<?php
    $this->Html->addCrumb('Users', ['controller' => 'Users', 'action' => 'index']);
    $this->Html->addCrumb(h($user->name).' '.h($user->lastname), null);
?>
<?php //$this->Html->addCrumb('USERS/ '.h($user->name).' '.h($user->lastname), null); ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Update Information'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
   
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Lastname') ?></th>
            <td><?= h($user->lastname) ?></td>
        </tr>
        
        
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Role') ?></th>
            <td><?= $user->has('role') ? $user->role->role_name : '' ?></td>
        </tr>
         <tr>
            <th><?= __('Gender') ?></th>
            <td><?php switch ($user->gender) {
                case '0':
                    echo 'Male';
                    break;
                case '1':
                    echo 'Female';
                default:
                    echo 'Other';
                    break;
            } h($user->gender) ?></td>
        </tr>

       
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
</div>
