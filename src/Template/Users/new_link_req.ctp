<!DOCTYPE html>
<html>
<head>
	<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('foundation-icons/foundation-icons.css') ?>
    <?= $this->Html->css('app.css') ?>
    <?= $this->fetch('css') ?>
</head>

<body>
<section id="body-content">
	<?php //$this->Html->addCrumb('Registration', null); ?>
	
	<div class="screen">
		<h3 class="title_1">Duplicate registration</h3>

		<div class="content_text_1">
			<p>We have your email in our records, if you follow this link we will send another email to activate your account </p>
			<p>New email to activate your account <a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'newActivationTok/'.$email]) ?>">New Email</a></p>
		</div>
	</div>
</section>	
</body>
</html>