<style type="text/css">
	.breadcrumb-wrapper.columns{ display: none; }
</style>
<div class="users form row login-form">
<div class="medium-6 columns medium-centered">
	<h4><?php echo __('log in'); ?></h4>
	
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create() ?>
    <fieldset>
        <!-- <legend><?= __('Please enter your username and password') ?></legend> -->
				<div class="medium-10 columns medium-centered"><div class="medium-4 columns"><?php echo __('Email address'); ?>:</div>
					        <div class="medium-8 columns">
					        	<?= $this->Form->input('email', ['label' => false]) ?>
					      	</div>
					      	<div class="medium-4 columns"><?php echo __('Password'); ?>:</div>
					        <div class="medium-8 columns">
					        	<?= $this->Form->input('password', ['label' => false]) ?></div>
      	</div>
    </fieldset>
<div class="captcha-box">
	<p><?php echo __('Show you are not a robot please:'); ?></p>
	<div class="medium-12 columns medium-centered">
		<!-- show captcha image html -->
		<?= captcha_image_html() ?>
		<!-- Captcha code user input textbox -->
		<?= $this->Form->input('CaptchaCode', [
		  'label' => 'Type the characters from the picture:',
		  'maxlength' => '10',
		  'style' => 'width: 270px; float: right;',
		  'id' => 'CaptchaCode'
		]) ?>
	</div>
</div>

<p class="text-center" style="clear: both;"><?= $this->Html->link(__('Forgot your password ?'), ['controller' => 'Users', 'action' => 'forgetpass']) ?></p>
<?= $this->Form->button(__('Login'), ['class' => 'button submit']); ?>
<p class="forget"><a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'add']); ?>">Don't have an account? Register</a></p>
<?= $this->Form->end() ?>
</div>

</div>

<?= $this->Html->css(captcha_layout_stylesheet_url(), ['inline' => false]) ?>