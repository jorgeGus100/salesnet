<?php $this->Html->addCrumb('Register', null); ?>

<div class="wrapper users-add columns">
    <div class="users add large-9 medium-8 columns content">
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __('Register') ?></legend>
            <p>I don't have an account</p>
        <div class="medium-10 columns medium-centered">
                <div class="medium-4 columns">
                    <?php echo __('Name'); ?>:
                </div>
                <div class="medium-8 columns">
                    <?php echo $this->Form->input('name', ['label' => false]); ?>
                </div>
            <div class="medium-4 columns">
                <?php echo __('Lastname'); ?>:
            </div>
            <div class="medium-8 columns">
                <?php echo $this->Form->input('lastname', ['label' => false]); ?>
            </div>
            <div class="medium-4 columns">
                <?php echo __('Email'); ?>:
            </div>
            <div class="medium-8 columns">
                <?php echo $this->Form->input('email', ['label' => false]); ?>
            </div>
            <div class="medium-4 columns">
                <?php echo __('Password'); ?>:
            </div>
            <div class="medium-8 columns">
                <?php echo $this->Form->input('password', ['label' => false]); ?>
            </div>
            <div class="medium-4 columns">
                <?php echo __('Confirm Password'); ?>:
            </div>
            <div class="medium-8 columns">
                <?php echo $this->Form->input('password', array('id'=>'confirm_password','name'=>'confirm_password','value'=>'', 'label'=>'Confirm Password', 'label' => false));
            ?>
            </div>
            <?php if($role===1): ?>
            <div class="medium-4 columns">
                <?php echo __('Role'); ?>:
            </div>
                
            <div class="medium-8 columns">
                <?php echo $this->Form->input('role_id', ['options' => $roles, 'label' => false]); ?>
            </div>
            <?php endif; ?>
            <div class="medium-4 columns">
                <?php echo __('Gender'); ?>:
            </div>
            <div class="medium-8 columns">
                <?php echo $this->Form->input('gender', array(
                                'options' => array('0'=>'Male', '1'=>'Female','2'=>'Other'),
                                'empty' => '(choose one)',
                                'label' => false
                                )); ?>
            </div>
            <div class="captcha-box">
                <p><?php echo __('Before continuing, we want to ensure that a real person is who access this account.'); ?></p>
                <div class="medium-10 columns medium-centered">
                    <!-- show captcha image html -->
                    <?= captcha_image_html() ?>
                    <!-- Captcha code user input textbox -->
                    <?= $this->Form->input('CaptchaCode', [
                      'label' => 'Retype the characters from the picture:',
                      'maxlength' => '10',
                      'style' => 'width: 270px;',
                      'id' => 'CaptchaCode'
                    ]) ?>
                </div>
            </div>
        </div><!-- medium-centered -->
        </fieldset>
        <?= $this->Form->button(__('Submit'), ['class' => 'button submit']) ?>
        <?= $this->Form->end() ?>
    </div>
    <div class="large-3 medium-4 columns users add banner">
        <fieldset>
            <legend>Banner of discounts</legend>
        </fieldset>
    </div>
</div>

<?= $this->Html->css(captcha_layout_stylesheet_url(), ['inline' => false]) ?>