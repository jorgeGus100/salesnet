<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'trash', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Reset Password'), ['action' => 'resetPassAdmin', $user->id]) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('lastname');
            echo $this->Form->input('email');
            echo $this->Form->input('role_id', ['options' => $roles]);
             echo $this->Form->input('gender', array(
                    'options' => array('0'=>'Male', '1'=>'Female','2'=>'O'),
                    'empty' => '(choose one)',
                    'label' => false
                    ));
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'button submit']) ?>
    <?= $this->Form->end() ?>
    
</div>
