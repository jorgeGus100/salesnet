 <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Reset Password'), ['action' => 'resetPassUser', $user->id]) ?></li>
    </ul>
</nav>
<!--<div class="user-profile form large-6 large-centered medium-8  medium-centered content">-->
<div class="user-profile form large-6 large-offset-3 medium-8  content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Personal information') ?></legend>
        <div class="medium-5 columns">
            <label for="name"><?php echo __('First name'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php echo $this->Form->input('name', ['label' => false, ]); ?>
        </div>
        <div class="medium-5 columns">
            <label for="lastname"><?php echo __('Last name'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php echo $this->Form->input('lastname', ['label' => false, ]); ?>
        </div>
        <div class="medium-5 columns">
            <label for="email"><?php echo __('Email'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php echo $this->Form->input('email', ['label' => false, 'disabled' => true]); ?>
        </div>
        <div class="medium-5 columns">
            <label for="phone"><?php echo __('Phone number'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php echo $this->Form->input('phone', ['label' => false]); ?>
        </div>
        <div class="medium-5 columns">
            <label for="location"><?php echo __('Location'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php echo $this->Form->input('location', ['label' => false, ]); ?>
        </div>
        <div class="medium-5 columns">
            <label for="address"><?php echo __('Address'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php echo $this->Form->input('address', ['label' => false, ]); ?>
        </div>
        <div class="medium-5 columns">
            <label for="gender"><?php echo __('Gender'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php echo $this->Form->input('gender', [
                'label' => false,
                'type' => 'select',
                'multiple' => false,
                'options' => $genderValues
            ]); ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?= __('Preferences') ?></legend>
        <div class="medium-5 columns">
            <label for="name"><?php echo __('Categories'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php if($choosedPreferences): ?>
                <?php echo $this->Form->input('pref_categories', ['label' => false,
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $categoryValues,
                    'value' => $choosedPreferences->categories
                ]); ?>
            <?php else: ?>
                <?php echo $this->Form->input('pref_categories', ['label' => false,
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $categoryValues,
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="medium-5 columns">
            <label for="name"><?php echo __('Brands'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php if($choosedPreferences): ?>
                <?php echo $this->Form->input('pref_brands', ['label' => false,
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $brandsValues,
                    'value' => $choosedPreferences->brands
                ]); ?>
            <?php else: ?>
                <?php echo $this->Form->input('pref_brands', ['label' => false,
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $brandsValues,
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="medium-5 columns">
            <label for="name"><?php echo __('I am interested in'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php if($choosedPreferences): ?>
                <?php echo $this->Form->input('pref_interest', ['label' => false,
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $interestsValues,
                    'value' => $choosedPreferences->interest
                ]); ?>
            <?php else: ?>
                <?php echo $this->Form->input('pref_interest', ['label' => false,
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $interestsValues,
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="medium-5 columns">
            <label for="name"><?php echo __('Send me an email with current offers'); ?></label>
        </div>
        <div class="medium-7 columns">
            <?php if($choosedPreferences): ?>
                <?php echo $this->Form->input('pref_emailsending', ['label' => false,
                    'type' => 'radio',
                    'options' => $sendingOptions,
                    'value' => $choosedPreferences->email_sending
                ]); ?>
            <?php else: ?>
                <?php echo $this->Form->input('pref_emailsending', ['label' => false,
                    'type' => 'radio',
                    'options' => $sendingOptions,
                ]); ?>
            <?php endif; ?>
        </div>
    </fieldset>
    <?php echo $this->Form->input('id', ['type' => 'hidden']); ?>
    <?php echo $this->Form->button(__('Save'), ['class' => 'button submit']) ?>
    <?= $this->Form->end() ?>
</div>
