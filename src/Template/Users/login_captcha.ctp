<div class="users form">
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Please enter your username and password and fill captcha') ?></legend>
        <?= $this->Form->input('username') ?>
        <?= $this->Form->input('password') ?>
        <?= $this->Captcha->create('securitycode', ['type'=>'image' //or 'math'
						, 'theme'=>'random']); ?>
    </fieldset>
<?= $this->Form->button(__('Login')); ?>
<?= $this->Html->link(__('Forget password'), ['controller' => 'Users', 'action' => 'forgetpass']) ?>
<?= $this->Form->end() ?>
</div>
<script 
src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
jQuery('.creload').on('click', function() {
    var mySrc = $(this).prev().attr('src');
    alert(mySrc);
    var glue = '?';
    if(mySrc.indexOf('?')!=-1)  {
        glue = '&';
    }
    $(this).prev().attr('src', mySrc + glue + new Date().getTime());
    return false;
});
</script>