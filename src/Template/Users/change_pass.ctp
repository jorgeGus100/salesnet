<div class="users form large-6 medium-8 columns medium-centered large-centered content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Change Your Password') ?></legend>
        <p><?php echo __('Set your new password please. Use at least 8 characters.'); ?></p>
        <div class="medium-4 columns">
            <?php echo __('Current Password'); ?>:
        </div>
        <div class="medium-8 columns">
            <?php echo $this->Form->input('password', ['label' => false, 'value' => '']); ?>
        </div>
        <div class="medium-4 columns">
            <?php echo __('New Password'); ?>:
        </div>
        <div class="medium-8 columns">
            <?php echo $this->Form->input('password', array('id'=>'new password','name'=>'new password','value'=>'', 'label'=>'New Password', 'label' => false));// new password
            ?>
        </div>
        <div class="medium-4 columns">
            <?php echo __('Confirm your new password'); ?>:
        </div>
        <div class="medium-8 columns">
            <?php echo $this->Form->input('password', array('id'=>'confirm password','name'=>'confirm password','value'=>'', 'label'=>'Confirm Password', 'label' => false));
            ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'button submit']) ?>
    <?= $this->Form->end() ?>
</div>
