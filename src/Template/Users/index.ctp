<?php $this->Html->addCrumb('Users', null); ?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        
    </ul>
</nav>
<div class="users index large-10 medium-8 columns content">
    
    <table cellpadding="0" cellspacing="0" >
        <thead>
            <tr>
               
               
                <th class="text-center"><?= $this->Paginator->sort('lastname','Full Name') ?></th>
                
                <!-- <th><?= $this->Paginator->sort('password') ?></th> -->
                <th class="text-center"><?= $this->Paginator->sort('email') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('role_id') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('created') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions text-center"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                
                <td class="text-left"><?= $this->Html->link(h($user->name).' '.h($user->lastname), ['action' => 'view', $user->id]) ?></td>
               
                <!-- <td><?= h($user->password) ?></td> -->
                <td class="text-left"><?= h($user->email) ?></td>
                <td class="text-left"><?= $user->has('role') ? $user->role->role_name : ''; ?></td>
                <td class="text-left"><?= h($user->created) ?></td>
                <td class="text-left"><?= h($user->modified) ?></td>
                <td class="actions">
                    <a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'view/'.$user->id]) ?>"><i class="fi-eye"></i></a>
                    <a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'edit/'.$user->id]) ?>"><i class="fi-pencil"></i></a>
                    <a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'trash/'.$user->id]) ?>" onclick="if (confirm('Are you sure you wish to delete this user?')) { return true; } return false;"><i class="fi-trash"></i></a>

                    
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
    </div>
</div>
