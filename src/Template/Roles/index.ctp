<?php $this->Html->addCrumb('Roles', null); ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Role'), ['action' => 'add']) ?></li>
        
    </ul>
</nav>
<div class="roles index large-9 medium-8 columns content">
    <h3><?= __('Roles') ?></h3>
    <table cellpadding="0" cellspacing="0" class="text-center">
        <thead>
            <tr>
                <th class="text-center"><?= $this->Paginator->sort('role_name') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('created') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions text-center"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($roles as $role): ?>
            <tr>
                <td><?= $this->Html->link( h($role->role_name) , ['action' => 'view', $role->id]) ?></td>
                <td class="center"><?= h($role->created) ?></td>
                <td class="center"><?= h($role->modified) ?></td>
                <td class="actions">
                    <a href="<?php echo $this->Url->build(['controller' => 'roles', 'action' => 'view/'.$role->id]) ?>"><i class="fi-eye"></i></a>
                    <a href="<?php echo $this->Url->build(['controller' => 'roles', 'action' => 'edit/'.$role->id]) ?>"><i class="fi-pencil"></i></a>
                    <a href="<?php echo $this->Url->build(['controller' => 'roles', 'action' => 'trash/'.$role->id]) ?>" onclick="if (confirm('Are you sure you wish to delete this role?')) { return true; } return false;"><i class="fi-trash"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
        
    </div>
</div>
