<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'default';
$this->Html->addCrumb('Error');
?>

<section class="content">
    <div class="error-page">
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"><img src="<?php echo $this->request->webroot.'/img/error-icon.png'?>" class="error-img"></i><?php echo __('Oops! Something went wrong.'); ?></h3>
            <p>
                <?php echo __('We will work on fixing that right away. If you want, please let us know about this error'); ?>
                <?php echo $this->Html->link(__('Contact us'), ['controller' => 'pages', 'action' => 'contactUs']); ?>
            </p>
        </div>
    </div>
</section>