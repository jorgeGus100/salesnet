<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'default';
$this->Html->addCrumb('Error');
?>

<section class="content">
    <div class="error-page">
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"><img src="<?php echo $this->request->webroot.'/img/error-icon.png'?>" class="error-img"></i><?php echo __('Oops! Something went wrong.'); ?></h3>
            <p>
                <?php echo __('It seems like you are trying to enter to an invalid url, please go back and enter to a valid action'); ?>.
            </p>
        </div>
    </div>
</section>