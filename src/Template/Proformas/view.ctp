<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Proforma'), ['action' => 'edit', $proforma->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Proforma'), ['action' => 'delete', $proforma->id], ['confirm' => __('Are you sure you want to delete # {0}?', $proforma->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Proformas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Proforma'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Proformas Item'), ['controller' => 'ProformasItem', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Proformas Item'), ['controller' => 'ProformasItem', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="proformas view large-9 medium-8 columns content">
    <h3><?= h($proforma->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $proforma->has('user') ? $this->Html->link($proforma->user->name, ['controller' => 'Users', 'action' => 'view', $proforma->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Current Email') ?></th>
            <td><?= h($proforma->current_email) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($proforma->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Seller Id') ?></th>
            <td><?= $this->Number->format($proforma->seller_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Creation Date') ?></th>
            <td><?= h($proforma->creation_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Exp Date') ?></th>
            <td><?= h($proforma->exp_date) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Proformas Item') ?></h4>
        <?php if (!empty($proforma->proformas_item)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Proforma Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Product Quantity') ?></th>
                <th><?= __('Discount Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($proforma->proformas_item as $proformasItem): ?>
            <tr>
                <td><?= h($proformasItem->id) ?></td>
                <td><?= h($proformasItem->proforma_id) ?></td>
                <td><?= h($proformasItem->product_id) ?></td>
                <td><?= h($proformasItem->product_quantity) ?></td>
                <td><?= h($proformasItem->discount_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ProformasItem', 'action' => 'view', $proformasItem->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ProformasItem', 'action' => 'edit', $proformasItem->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ProformasItem', 'action' => 'delete', $proformasItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $proformasItem->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
