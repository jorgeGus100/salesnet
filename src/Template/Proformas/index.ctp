<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Proforma'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Proformas Item'), ['controller' => 'ProformasItem', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Proformas Item'), ['controller' => 'ProformasItem', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="proformas index large-9 medium-8 columns content">
    <h3><?= __('Proformas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('client_id') ?></th>
                <th><?= $this->Paginator->sort('seller_id') ?></th>
                <th><?= $this->Paginator->sort('current_email') ?></th>
                <th><?= $this->Paginator->sort('creation_date') ?></th>
                <th><?= $this->Paginator->sort('exp_date') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($proformas as $proforma): ?>
            <tr>
                <td><?= $this->Number->format($proforma->id) ?></td>
                <td><?= $proforma->has('user') ? $this->Html->link($proforma->user->name, ['controller' => 'Users', 'action' => 'view', $proforma->user->id]) : '' ?></td>
                <td><?= $this->Number->format($proforma->seller_id) ?></td>
                <td><?= h($proforma->current_email) ?></td>
                <td><?= h($proforma->creation_date) ?></td>
                <td><?= h($proforma->exp_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $proforma->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $proforma->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $proforma->id], ['confirm' => __('Are you sure you want to delete # {0}?', $proforma->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
        
    </div>
</div>
