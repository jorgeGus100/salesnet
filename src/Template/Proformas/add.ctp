<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Proformas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Proformas Item'), ['controller' => 'ProformasItem', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Proformas Item'), ['controller' => 'ProformasItem', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="proformas form large-9 medium-8 columns content">
    <?= $this->Form->create($proforma) ?>
    <fieldset>
        <legend><?= __('Add Proforma') ?></legend>
        <?php
            echo $this->Form->input('client_id', ['options' => $users]);
            echo $this->Form->input('seller_id');
            echo $this->Form->input('current_email');
            echo $this->Form->input('creation_date');
            echo $this->Form->input('exp_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
