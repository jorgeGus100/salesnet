<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Preference'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="preferences index large-9 medium-8 columns content">
    <h3><?= __('Preferences') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('value') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($preferences as $preference): ?>
            <tr>
                <td><?= $this->Number->format($preference->id) ?></td>
                <td><?= h($preference->name) ?></td>
                <td><?= h($preference->value) ?></td>
                <td><?= $preference->has('user') ? $this->Html->link($preference->user->name, ['controller' => 'Users', 'action' => 'view', $preference->user->id]) : '' ?></td>
                <td><?= h($preference->created) ?></td>
                <td><?= h($preference->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $preference->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $preference->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $preference->id], ['confirm' => __('Are you sure you want to delete # {0}?', $preference->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
        
    </div>
</div>
