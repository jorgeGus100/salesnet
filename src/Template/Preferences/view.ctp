<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Preference'), ['action' => 'edit', $preference->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Preference'), ['action' => 'delete', $preference->id], ['confirm' => __('Are you sure you want to delete # {0}?', $preference->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Preferences'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Preference'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="preferences view large-9 medium-8 columns content">
    <h3><?= h($preference->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($preference->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Value') ?></th>
            <td><?= h($preference->value) ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $preference->has('user') ? $this->Html->link($preference->user->name, ['controller' => 'Users', 'action' => 'view', $preference->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($preference->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($preference->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($preference->modified) ?></td>
        </tr>
    </table>
</div>
