<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $preference->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $preference->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Preferences'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="preferences form large-9 medium-8 columns content">
    <?= $this->Form->create($preference) ?>
    <fieldset>
        <legend><?= __('Edit Preference') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('value');
            // echo $this->Form->input('user_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
