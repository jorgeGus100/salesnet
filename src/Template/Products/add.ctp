<?php
    $this->Html->addCrumb('Products', ['controller' => 'Products', 'action' => 'index']);
    $this->Html->addCrumb('New', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?></li>
        
    </ul>
</nav>
<div class="products form large-9 medium-8 columns content">
    <?= $this->Form->create($product, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Add Product') ?></legend>
        <?php
            echo $this->Form->input('product_code');
            echo $this->Form->input('product_name');
            echo $this->Form->input('categories_id', ['options' => $categories]);
            echo $this->Form->input('brands_id', ['options' => $brands]);
            echo $this->Form->input('product_price', ['step' => 0.01]);
            echo $this->Form->input('upload', ['type' => 'file', 'label' => 'Product image']);
            echo $this->Form->input('product_description');
            echo '<div id="image-holder"></div>'; 
            echo $this->Form->input('intKardex',['label' => 'Quantity']);
          //  echo $this->Form->input('discount', ['type' => 'checkbox']);
            echo $this->Form->input('intDisc',['label' => 'Discounts']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), [
        'class' => 'button submit'
    ]) ?>
    <?= $this->Form->end() ?>
</div>
<?= $this->Html->script('jquery.min.js') ?>
<?php echo $this->Html->script('upload.js') ?>