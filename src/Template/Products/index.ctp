<?php $this->Html->addCrumb('Products', null); ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?></li>

    </ul>
</nav>
<?php //pr(count($products)); ?>
<div class="products index large-9 medium-8 columns content">
    <table cellpadding="0" cellspacing="0" class="text-center">
        <thead>
            <tr>
                <th class="text-center"><?= $this->Paginator->sort('product_code', 'Code') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('product_name','Name') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('product_price', 'Price') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('brands_id', 'Brand') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('categories_id', 'Category') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('kardexes_id', 'Quantity') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('created') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions text-center"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product): ?>
            <tr>
                <td><span class="code"><?php echo $product->product_code; ?></span></td>
                <td><?= $this->Html->link(htmlentities($product->product_name), ['action' => 'view', $product->id]) ?></td>
                <td><span class="price"><?= $this->Number->currency($product->product_price, 'USD') ?></span></td>
                <td><?= $product->has('brand') ? $this->Html->link($product->brand->brand_name, ['controller' => 'Brands', 'action' => 'view', $product->brand->id]) : '' ?></td>
                <td><?php echo ((isset($product->categories[0])) ? $this->Html->link($product->categories[0]->category_name, ['controller' => 'Categories', 'action' => 'view', $product->categories[0]->id]) : '') ?></td>
                <td><span class="price"><?= $product->kardexes[0]->quantity ?></span></td>
                <td><?=   $product->created ?></td>
                <td><?=   h($product->modified) ?></td>
                <td class="actions">
                    <a href="<?php echo $this->Url->build(['controller' => 'products', 'action' => 'view/'.$product->id]) ?>"><i class="fi-eye"></i></a>

                    <a href="<?php echo $this->Url->build(['controller' => 'products', 'action' => 'edit/'.$product->id]) ?>"><i class="fi-pencil"></i></a>

                    <a href="<?php echo $this->Url->build(['controller' => 'products', 'action' => 'trash/'.$product->id]) ?>" onclick="if (confirm('Are you sure you wish to delete this product?')) { return true; } return false;"><i class="fi-trash"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>

            <?= $this->Paginator->next(' ') ?>

        </ul>

    </div>
</div>
