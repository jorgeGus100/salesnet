<?php
    $this->Html->addCrumb('Brands', ['controller' => 'Brands', 'action' => 'guestView']);
    $this->Html->addCrumb($product->brand->brand_name, ['controller' => 'Brands', 'action' => 'productsBrand', $product->brand->id]);
    $this->Html->addCrumb('View', null);
?>
<div class="row first-row">
    <div class="large-4 columns product-images">
        <div class="main"><?php
            if(!empty($listPics))
                echo '<img src="'.$this->request->webroot.'upload/products_img/'.$listPics[0]['pic_path'].'" >';
            else
                echo '<img src="http://placehold.it/350x250">';
            ?></div>
        <div class="general-rating"><?php if( $product->getRating())://general rating ?>
                <div class="product-rating small-9x columns" data-rating="<?php echo $product->getRating(); ?>" data-readonly="true"></div>
            <?php else: ?>
                <span class="star-rating"><?php echo $this->Html->image('blank-star-rating.png'); ?></span>
            <?php endif; ?></div>
    </div>


    <div class="products view large-8 medium-8 columns content">

        <h3 class="product-name small-12 columns"><?= h($product->product_name) ?></h3>
        <div class="large-6 columns product-info">
            <div class="box_resume">
                <p><span><?php echo __('Code'); ?>:</span><?php echo $product->product_code ?></p>
               <p><span><?php echo __('Brand'); ?>:</span><?= $product->has('brand') ? $this->Html->link($product->brand->brand_name, ['controller' => 'Brands', 'action' => 'productsBrand', $product->brand->id]) : '' ?></p>
               <p><span><?php echo __('Category'); ?>:</span><?php echo (!empty($categories->category_name)) ? $this->Html->link($categories->category_name, ['controller' => 'Categories', 'action' => 'productsCategory', $categories->id]): '' ; ?></p>
               <p><span><?php echo __('Description'); ?>:</span><?php echo $product->product_description; ?></p>
            </div>
            <div class="box_resume_2">
                <div>
                    <?php echo $this->Form->create('AddCart', ['url' => ['controller' => 'OrderItems', 'action' => 'add'], 'class' => 'add-cart-form']); ?>
                    <?php //pr($inCart) ?>
                    <?php if(isset($inCart) && $inCart): ?>
                        <?php echo $this->Form->input('quantity', ['type' => 'number', 'min' => '0', 'disabled' => true, 'value' => $cartQuantity]); ?>
                    <?php else: ?>
                        <?php echo $this->Form->input('quantity', ['type' => 'number', 'min' => '1', 'max' => ((!empty($product->kardexes) ? $product->kardexes[0]->quantity : '')), 'autocomplete' => 'off', 'value' => 1]); ?>
                    <?php endif; ?>
                    <p class="error" style="display: none;"><?php echo __('The field cannot be empty.'); ?></p>
                    <?php echo $this->Form->input('product_id', ['value' => $product->id, 'type' => 'hidden']); ?>
                    <p class="availability">Available: <?php //echo (!empty($listKardex[0]['quantity'])) ? $listKardex[0]['quantity'] : ''; ?><?php echo $product->getAvailability(); ?></p>
                </div>
                    <?php if(isset($inCart) && $inCart): ?>
                        <p style="text-shadow: 0 0 10px #000;font-size: 2em;"><?php echo __('The product is already on cart'); ?></p>
                    <?php else: ?>
                        <?php if(!isset($loggedUserId)): ?>
                            <?php echo $this->Html->link(__('Add to Cart'), ['controller' => 'OrderItems', 'action' => 'add', $product->id], ['class' => 'button guest']); ?>
                        <?php else: ?>
                            <?php echo $this->Form->button(__('Add to cart'), ['class' => 'button add-cart-button']); ?>
                        <?php endif; ?>
                    <?php endif;// inCart ?>
                    <?php if(isset($loggedUserId)): ?>
                        <div class="flag-auth"></div>
                    <?php endif;// loggedUserId ?>
                    <?php echo $this->Form->end(); ?>
               <p class="price"><?= __('Price') ?>: <?= $this->Number->currency($product->product_price) ?> <?php echo $this->Html->image('loading.gif', ['class' => 'loading-gif']); ?></p>
            </div>

            <?php if($this->request->session()->read('Auth.User')): ?>
                        <?php $loggedUser = $this->request->session()->read('Auth.User'); ?>
                        <?php echo $this->Form->create('Rating', ['url' => ['controller' => 'ratings', 'action' => 'rating'], 'class' => 'star-form']); ?>
                            <div class="columns rating-input">
                            <?php echo $this->Form->input('rate', [
                                'label' => false,
                                'required' => false,
                                'autocomplete' => 'off',
                                'type' => 'number',
                                'max' => 5
                                ]); ?>
                                <?php echo $this->Form->hidden('product_id', ['value' => $product->id]); ?>
                                <?php echo $this->Form->hidden('user_id', ['value' => $this->request->session()->read('Auth.User')['id']]); ?>

                            </div>
                            <div class=" columns search-icon">
                                <?php
                                echo $this->Form->button('rate', [
                                    'escape' => false,
                                    'class' => 'button'
                                    ]);
                                    ?>
                                    <div class="loading-gif-wrapper"><?php echo $this->Html->image('loading.gif', ['class' => 'loading-gif']); ?></div>
                            </div>
                            <?php echo $this->Form->end();// ratingForm ?>
                    <?php endif; ?>
                    <?php if(($this->request->session()->read('Auth.User'))): ?>
                        <?php
                            $isRated = $loggedUserC->isProductRated($product->id);
                            $userRate = $loggedUserC->getRate($product->id);
                        ?>
                        <?php if($isRated->count()): ?>
                            <!-- edit rating -->
                            <span class="small-3 columns rate-label">Rate: </span><div class="product-rating columns" data-rating="<?php echo $userRate; ?>"></div>
                        <?php else: ?>
                            <!-- add rating -->
                            <span class="small-3 columns rate-label">Rate: </span><div class="product-rating columns"></div>
                        <?php endif; ?>
                    <?php endif;// Auth.User ?>
        </div>
        <div class="large-6 columns percentages">
        <p class="text-center fb-like"><img src="<?php echo $this->request->webroot.'img/fb-like-sample.jpg'; ?>"></p>
        <?php //pr($product); ?>
        <?php if(!empty($listDiscounts[0]['discount_percentage'])): ?>
            <p class="product-discount text-center label success">Discount: <?php echo $listDiscounts[0]['discount_percentage']; ?>%</p>
        <?php endif; ?>
        </div>


    </div>
</div>
<div class="row">
    <div class="large-12 columns product-specifications">
        <h3><?php echo __('Item Specifications'); ?></h3>
    </div>
    <div class="large-12 columns related-products">
        <fieldset>
        <legend><?php echo __('Related Products'); ?></legend>
            <?php if(isset($relatedProducts)): ?>
                <?php foreach($relatedProducts as $rProd): ?>
                    <div class="col5-unit columns product">
                        <?php //pr($rProd->id);continue; ?>
                        <a href="<?php echo $this->Url->build([
                              "controller" => "Products",
                              "action" => "guestView",
                              $rProd->id
                            ]); ?>">
                            <?php if(!empty($rProd->products_pics)): ?>
                                <img src="<?php echo $this->request->webroot.'upload/products_img/'.$rProd->products_pics[0]['pic_path']; ?>" class="cover">
                            <?php else: ?>
                                 <img src="http://placehold.it/150x150" alt="">
                            <?php endif; ?>
                            <div class="product-data">
                                <p><?php echo $rProd->product_name; ?></p>
                                <p class="product-price"><?php echo $this->Number->currency($rProd->product_price); ?></p>
                                <?php //pr($rProd);continue; ?>
                                <p class="product-rate">
                                    <?php if($rProd->getRating()): ?>
                                      <div class="product-rating" data-rating="<?php echo $rProd->getRating(); ?>" data-readonly="true"></div>
                                    <?php else: ?>
                                      <span class="star-rating"><?php echo $this->Html->image('blank-star-rating.png'); ?></span>
                                    <?php endif; ?>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <?php for($i = 0; $i < 5; $i++): ?>
                    <div class="col5-unit columns product">
                        <a href="#">
                            <img src="http://placehold.it/150x150">
                            <div class="product-data">
                                <p>product name</p>
                                <p class="product-price">Price: $</p>
                                <p class="product-rate"><?php echo $this->Html->image('blank-star-rating.png'); ?></p>
                            </div>
                        </a>
                    </div>
                <?php endfor; ?>
            <?php endif;// isset relatedProducts ?>
        </fieldset>
    </div>
</div>
<?php echo $this->Html->script('jquery.star-rating-svg', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('star-rating-svg'); ?>
<?php echo $this->Html->script('jquery.zoom', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->script('jquery-ui.min', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('jquery-ui.min'); ?>