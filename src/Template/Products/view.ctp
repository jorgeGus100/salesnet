<?php
    $this->Html->addCrumb('Products', ['controller' => 'Products', 'action' => 'index']);
    $this->Html->addCrumb('View', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <?php if(($this->request->session()->read('Auth.User'))): ?>
            <li><?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->id]) ?> </li>
            <li><?= $this->Form->postLink(__('Delete Product'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?> </li>
        <?php endif; ?>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?> </li>
        <?php if(($this->request->session()->read('Auth.User'))): ?>
            <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?> </li>
        <?php endif; ?>
        <li><?= $this->Html->link(__('List Brands'), ['controller' => 'Brands', 'action' => 'index']) ?> </li>
        <?php if(($this->request->session()->read('Auth.User'))): ?>
            <li><?= $this->Html->link(__('New Brand'), ['controller' => 'Brands', 'action' => 'add']) ?> </li>
        <?php endif; ?>
        <li><?= $this->Html->link(__('List Kardexes'), ['controller' => 'Kardexes', 'action' => 'index']) ?> </li>
        <?php if(($this->request->session()->read('Auth.User'))): ?>
            <li><?= $this->Html->link(__('New Kardex'), ['controller' => 'Kardexes', 'action' => 'add']) ?> </li>
        <?php endif; ?>
        <li><?= $this->Html->link(__('List Products Discounts'), ['controller' => 'ProductsDiscounts', 'action' => 'index']) ?> </li>
        <?php if(($this->request->session()->read('Auth.User'))): ?>
            <li><?= $this->Html->link(__('New Products Discount'), ['controller' => 'ProductsDiscounts', 'action' => 'add']) ?> </li>
        <?php endif; ?>
        <?php //if(isset($inCart) && $inCart): ?>
            <!-- <li class="add-to-cart">
                <p style="text-shadow: 0 0 10px #000;font-size: 2em;">The product is already on cart</p>
            </li> -->
        <?php //else: ?>
            <!-- <li class="add-to-cart">
                <?//= $this->Html->link(__('Add to Cart'), ['controller' => 'OrderItems', 'action' => 'add', $product->id], ['class' => 'button']) ?>
            </li> -->
        <?php //endif; ?>
    </ul>
</nav>
<div class="products view large-9 medium-8 columns content">
<?php //print_r($listKardex); ?>
    <table class="vertical-table">
        <tr>
            <th><?= __('Product Code') ?></th>
            <td><?= h($product->product_code) ?></td>
        </tr>
        <tr>
            <th><?= __('Product Name') ?></th>
            <td><?= h($product->product_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Brand') ?></th>
            <td><?= $product->has('brand') ? $this->Html->link($product->brand->brand_name, ['controller' => 'Brands', 'action' => 'view', $product->brand->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Category') ?></th>
            <td>
                <?php
                    echo (!empty($categories->category_name)) ? $categories->category_name: '' ; ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Quantity') ?></th>
            <td><?php
                    echo (!empty($listKardex[0]['quantity'])) ? $listKardex[0]['quantity'] : '';
                    // $listKardex[0]['quantity']
                ?></td>
        </tr>
        <tr>
            <th><?= __('Product Price') ?></th>
            <td><?= $this->Number->format($product->product_price) ?></td>
        </tr>
        <tr>
            <th><?= __('') ?></th>
            <td><?php
                    if(!empty($listPics))
                    echo '<img src="'.$this->request->webroot.'upload/products_img/'.$listPics[0]['pic_path'].'"'; ?></td>
        </tr>
        <tr>
            <th><?= __('Product Description') ?></th>
            <td><?php
                    echo $product->product_description; ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Product Rating') ?></th>
            <td class="rating-wrapper">
                <?php if($this->request->session()->read('Auth.User')): ?>
                    <?php $loggedUser = $this->request->session()->read('Auth.User'); ?>
                    <?php echo $this->Form->create('Rating', ['url' => ['controller' => 'ratings', 'action' => 'rating'], 'class' => 'star-form']); ?>
                    <div class="large-10 columns rating-input">
                        <?php echo $this->Form->input('rate', [
                            'label' => false,
                            'required' => false,
                            'autocomplete' => 'off',
                            'type' => 'number',
                            'max' => 5
                            ]); ?>
                            <?php echo $this->Form->hidden('product_id', ['value' => $product->id]); ?>
                            <?php echo $this->Form->hidden('user_id', ['value' => $this->request->session()->read('Auth.User')['id']]); ?>
                        </div>
                        <div class="large-2 columns search-icon">
                            <?php
                            echo $this->Form->button('rate', [
                                'escape' => false,
                                'class' => 'button'
                                ]);
                                ?>
                                <div class="loading-gif-wrapper"><?php echo $this->Html->image('loading.gif', ['class' => 'loading-gif']); ?></div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                <?php endif; ?>
                <?php if(($this->request->session()->read('Auth.User'))): ?>
                    <?php if($product->getRating()): ?>
                        <div class="product-rating" data-rating="<?php echo $product->getRating(); ?>" data-readonly="true"></div>
                    <?php else: ?>
                        <div class="product-rating"></div>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if($product->getRating()): ?>
                        <div class="product-rating" data-readonly="true" data-rating="<?php echo $product->getRating(); ?>"></div>
                    <?php else: ?>
                        <div class="product-rating" data-readonly="true"></div>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>
</div>
<?php echo $this->Html->script('jquery.star-rating-svg', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('star-rating-svg'); ?>