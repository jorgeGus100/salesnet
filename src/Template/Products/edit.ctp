<?php
    $this->Html->addCrumb('Products', ['controller' => 'Products', 'action' => 'index']);
    $this->Html->addCrumb('Edit', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $product->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Brands'), ['controller' => 'Brands', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Brand'), ['controller' => 'Brands', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Kardexes'), ['controller' => 'Kardexes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Kardex'), ['controller' => 'Kardexes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products Discounts'), ['controller' => 'ProductsDiscounts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Products Discount'), ['controller' => 'ProductsDiscounts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Manage Related Products'), ['controller' => 'RelatedProducts', 'action' => 'manage', $product->id]) ?></li>
    </ul>
</nav>
<div class="products form large-9 medium-8 columns content">
    <?= $this->Form->create($product, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Edit Product') ?></legend>
        <?php
            //print_r($product);
            echo $this->Form->input('product_code');
            echo $this->Form->input('product_name');
            if(!empty(($listCategories))){
                echo $this->Form->input('categories_id', ['options' => $categories, 'value'=>$listCategories[0]['categories_id']]);
            } else{
                echo $this->Form->input('categories_id', ['options' => $categories]);
            }
            echo $this->Form->input('brands_id', ['options' => $brands, 'value'=>$product[0]['brands_id']]);
            echo $this->Form->input('product_price', ['step' => 0.01]);
            echo $this->Form->input('upload', ['type' => 'file', 'label' => 'Product image']);
            echo $this->Form->input('product_description');
            if(!empty($listPics["0"]["pic_path"])){
                $pic=$this->request->webroot.$listPics["0"]["pic_path"];
            }else{
                $pic=$this->request->webroot.'/upload/empty.png';
            }
            echo '<div id="image-holder"><img src="'.$pic.'"></div>';
            if(!empty($listKardex)){
                echo $this->Form->input('quantity', ['value'=>$listKardex[0]['quantity']]);
            } else{
                echo $this->Form->input('quantity');
            }
            if(!empty($listDiscounts)){
                echo $this->Form->input('discount', ['value'=>$listDiscounts[0]['discount_percentage']]);
            } else{
                echo $this->Form->input('discount');
            }
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'button submit']) ?>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->script('upload',['block' => true]); ?>