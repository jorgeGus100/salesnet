<?php	$this->Html->addCrumb('Search', null); ?>
<p class="related">Related subject for <span><?php echo ($subject); ?></span>:
	<a href="#">link 1</a>
	<a href="#">link 2</a>
	<a href="#">link 3</a>
</p>
<section class="results-wrapper">
	<?php if($result->count()): ?>
		<?php foreach($result as $item): ?>
			<?php //pr($item); ?>
			<article class="result-item row">
				<div class="medium-3 columns image">
					<?php if(!empty($item->getPic())): ?>
						<a href="<?php echo $this->Url->build(['controller' => 'products', 'action' => 'guestView', $item->id]); ?>"><img src="<?php echo $this->request->webroot.'upload/products_img/'.$item->getPic(); ?>"></a>
					<?php else: ?>
						<a href="<?php echo $this->Url->build(['controller' => 'products', 'action' => 'view', $item->id]); ?>"><img src="http://placehold.it/150x150" alt=""></a>
					<?php endif; ?>
				</div>
				<div class="medium-3 columns description">
				<p class="name"><a href="<?php echo $this->Url->build(['controller' => 'products', 'action' => 'guestView', $item->id]); ?>"><?php echo $item->product_name; ?></a></p>
					<p class="description">
					<?php if(isset($item->product_description) && $item->product_description != ''): ?>
						<?php echo $item->product_description; ?>
					<?php else: ?>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.
					<?php endif; ?>
					</p>
				</div>
				<div class="medium-3 columns details">
					<p class="quantity text-center"><span><?php echo $item->getQuantity(); ?></span> Available</p>
					<p class="rate text-center">Rate:
						<?php if($item->getRating()): ?>
							<div class="product-rating" data-rating="<?php echo $item->getRating(); ?>" data-readonly="true"></div>
						<?php else: ?>
							<span class="star-rating"><?php echo $this->Html->image('blank-star-rating.png'); ?></span>
						<?php endif; ?>
					</p>
					<p class="price">Price: $<?php echo $item->product_price; ?></p>
				</div>
				<div class="medium-3 columns discount">
				<?php if(!empty($item->products_discounts[0]['discount_percentage'])): ?>
						<p class="product-discount text-center label success">Discount: <?php echo $item->products_discounts[0]['discount_percentage']; ?>%</p>
					<!-- </fieldset> -->
				<?php //else: ?>
				<?php endif; ?>
				</div><!-- discount -->
			</article>
		<?php endforeach; ?>
		<ul class="pagination text-center" role="navigation" aria-label="Pagination">
			<?php if($this->Paginator->counter('{{pages}}') > 1): ?>
				<?php echo $this->Paginator->prev('&laquo; previous', [
					'class' => 'pagination-previous',
					'tag' => 'li',
					'escape' => false
				]); ?>
				<?php echo $this->Paginator->numbers(); ?>
				<?php echo $this->Paginator->next('next &raquo;', [
					'tag' => 'li',
					'class' => 'pagination-next',
					'escape' => false
				]); ?>
			<?php endif; ?>
		</ul>
	<?php else: ?>
		<p class="empty">No records available, for the subject <span><?php echo $subject; ?></span></p>
	<?php endif; ?>
</section><!-- results-wrapper -->

<?php echo $this->Html->script('jquery.star-rating-svg', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('star-rating-svg'); ?>