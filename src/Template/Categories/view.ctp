<?php
    $this->Html->addCrumb('Categories', ['controller' => 'Categories', 'action' => 'index']);
    $this->Html->addCrumb('View', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Category'), ['action' => 'edit', $category->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Category'), ['action' => 'trash', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->category_name)]) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['action' => 'add']) ?> </li>
        
    </ul>
</nav>
<div class="categories view large-9 medium-8 columns content">
    <h3><?= h($category->category_name) ?></h3>
    <table class="vertical-table">
        
        <tr>
            <th></th>
            <td><?php 
                if(!empty($category->category_pic)){
                $pic=$this->request->webroot.$category->category_pic;
            }else{
                $pic=$this->request->webroot.'/upload/empty.png';
            }
             echo '<img src="'.$pic.'">'; ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $category->has('user') ? $this->Html->link($category->user->name, ['controller' => 'Users', 'action' => 'view', $category->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Code') ?></th>
            <td><?= $this->Number->format($category->code) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($category->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($category->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($category->description)); ?>
    </div>
</div>
