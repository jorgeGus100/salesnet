<?php   $this->Html->addCrumb('Categories', null); ?>
<div class="large-12 columns categories-products">
          <!-- <p>SUGGESTED PRODUCTS</p> -->
          <?php foreach($allCategories2 as $category): ?>
            <div class="large-12 columns">
              <h3><a href="<?php echo $this->Url->build(['controller' => 'categories', 'action' => 'products_category/'.$category->id]) ?>"><?= h($category->category_name) ?></a></h3>
                <?php $n=0;  ?>
                <?php foreach ($category->products as $product): ?>
                  <?php if($product->product_status == 1): ?>
                      <div class="col5-unit columns product">
                      <a href="<?php echo $this->Url->build([
                          "controller" => "Products",
                          "action" => "guestView",
                          $product->id
                        ]); ?>">
                         <?php if(!empty($product->products_pics)): ?>
                            <img src="<?php echo $this->request->webroot.'upload/products_img/'.$product->products_pics[0]['pic_path']; ?>" class="cover">
                          <?php else: ?>
                            <img src="http://placehold.it/150x150" alt="">
                          <?php endif;// products_pics ?>
                        <div class="product-data">
                          <p class="product-name"><?php echo $product->product_name; ?></p>
                          <p class="produc-price text-center">price: <?php echo $this->Number->currency($product->product_price); ?></p>
                          <p class="product-rate">
                          <?php if($product->getRating()): ?>
                              <div class="product-rating" data-rating="<?php echo $product->getRating(); ?>" data-readonly="true"></div>
                            <?php else: ?>
                              <span class="star-rating"><?php echo $this->Html->image('blank-star-rating.png'); ?></span>
                            <?php endif; ?>
                          </p>
                        </div><!-- product-data -->
                      <?php echo $this->element('add_cart_btn', ['product' => $product, 'loggedUser' => (isset($user) ? $user : '')]); ?>

                      </a>
                      </div><!-- col5-unit -->
                                          <?php //$n++; ?>
                  <?php endif;// product_status ?>
                <?php endforeach; ?>
            </div><!-- large-12 columns -->
          <?php endforeach;// allCategories2 ?>
</div><!-- categories-products -->

<?php echo $this->Html->script('jquery.star-rating-svg', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('star-rating-svg'); ?>
<?php echo $this->Html->script('jquery.zoom', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->script('jquery-ui.min', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('jquery-ui.min'); ?>