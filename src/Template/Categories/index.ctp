<?php $this->Html->addCrumb('Categories', null); ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Category'), ['action' => 'add']) ?></li>
        
    </ul>
</nav>
<div class="categories index large-9 medium-8 columns content">
    
    <table cellpadding="0" cellspacing="0" >
        <thead>
            <tr>
               
                <th class="text-center"><?= $this->Paginator->sort('category_name') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('code') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('created') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions text-center"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categories as $category): ?>
            <tr>
                
                <td class="text-left"><?=  $this->Html->link(h($category->category_name), ['action' => 'view', $category->id]) ?></td>
                <td><span class="price"><?= $this->Number->format($category->code) ?></span></td>
                <td><?=   $category->created ?></td>
                <td><?=   h($category->modified) ?></td>
                <td class="actions">
                   
                    <a href="<?php echo $this->Url->build(['controller' => 'categories', 'action' => 'view/'.$category->id]) ?>"><i class="fi-eye"></i></a>
                  
                    <a href="<?php echo $this->Url->build(['controller' => 'categories', 'action' => 'edit/'.$category->id]) ?>"><i class="fi-pencil"></i></a>

                    <a href="<?php echo $this->Url->build(['controller' => 'categories', 'action' => 'trash/'.$category->id]) ?>" onclick="if (confirm('Are you sure you wish to delete this category?')) { return true; } return false;"><i class="fi-trash"></i></a>
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
    </div>
</div>
