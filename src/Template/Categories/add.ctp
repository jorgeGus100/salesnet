<?php
    $this->Html->addCrumb('Categories', ['controller' => 'Categories', 'action' => 'index']);
    $this->Html->addCrumb('New', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['action' => 'index']) ?></li>
       
    </ul>
</nav>
<div class="categories form large-9 medium-8 columns content">
    <?= $this->Form->create($category, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Add Category') ?></legend>
        <?php
            echo $this->Form->input('category_name');
            echo $this->Form->input('code');
            echo $this->Form->input('description');
           echo $this->Form->input('upload', ['type' => 'file']);
             echo '<div id="image-holder"></div>';
           
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), [
        'class' => 'button submit'
    ]) ?>
    <?= $this->Form->end() ?>
</div>

<?= $this->Html->script('jquery.min.js') ?>
<?php echo $this->Html->script('upload.js') ?>
