<?php
    $this->Html->addCrumb('Categories', ['controller' => 'Categories', 'action' => 'index']);
    $this->Html->addCrumb('Edit', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $category->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $category->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['action' => 'index']) ?></li>
       
    </ul>
</nav>
<div class="categories form large-9 medium-8 columns content">
    <?= $this->Form->create($category, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Edit Category') ?></legend>
        <?php
            echo $this->Form->input('category_name');
            echo $this->Form->input('code');
            echo $this->Form->input('description');
            echo $this->Form->input('upload', ['type' => 'file']);
            if(!empty($category->category_pic)){
                $pic=$this->request->webroot.$category->category_pic;
            }else{
                $pic=$this->request->webroot.'/upload/empty.png';
            }
            echo '<div id="image-holder"><img src="'.$pic.'"></div>';
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->script('upload',['block' => true]); ?>