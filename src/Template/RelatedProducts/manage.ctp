<?php
    $this->Html->addCrumb('Products', ['controller' => 'Products', 'action' => 'index']);
    $this->Html->addCrumb('Edit', ['controller' => 'Products', 'action' => 'edit', $productId]);
    $this->Html->addCrumb('Manage Related Products', null);
?>
<div class="wrapper related">
	<h2><?php echo __('Manage Related Products for'); ?>: <?php echo $currentEntity->product_name; ?></h2>
	<p><?php echo __('Please select the desired products to be associated.'); ?></p>
	<?php echo $this->Form->create('RelatedProducts'); ?>

	<?php $counting = 0; ?>
	<?php // related product with pagin v2 ?>
	<?php foreach($template2 as $key => $cat): ?>
		<?php
			$counting++;
			$prodCollection = array();
			if(($counting == 0) || ($counting % 2 == 0)):
		?>
			<div class="row">
		<?php endif; ?>
				<div class="large-6 columns options">
					<h3><?php echo $cat->category_name; ?></h3>
					<p><input type="checkbox" name="check-all" class="check-all"><span><?php echo __('Select related items'); ?></span><span class="arrow"></span></p>
					<?php foreach($cat->products as $product):// preparing data ?>
						<?php $prodCollection[$product->id] = $product->product_name; ?>
					<?php endforeach; ?>
					<?php echo $this->Form->input('custom_related_' . $cat->id, [
						'multiple' => 'checkbox',
						'options' => $prodCollection,
						'val' => $matched,
						'label' => false
					]); ?>
				</div>
		<?php if(($counting == 0) || ($counting % 2 == 0)): ?>
			</div><!-- row -->
		<?php endif; ?>
	<?php endforeach; ?>
	<?php echo $this->Form->hidden('product_id', ['value' => $productId]); ?>

	<?php echo $this->Form->button('Save', ['class' => 'button']); ?>
	<?php echo $this->Form->end(); ?>
</div>
<ul class="pagination text-center" role="navigation" aria-label="Pagination">
	<?php if($this->Paginator->counter('{{pages}}') > 1):// if pagination is required ?>
		<?php echo $this->Paginator->prev('&laquo; previous', [
			'class' => 'pagination-previous',
			'tag' => 'li',
			'escape' => false
			]); ?>
			<?php echo $this->Paginator->numbers();// prints out the numbers ?>
			<?php echo $this->Paginator->next('next &raquo;', [
				'tag' => 'li',
				'class' => 'pagination-next',
				'escape' => false
				]); ?>
			<?php endif; ?>
		</ul>
<?php echo $this->Html->script('jquery-ui.min', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('jquery-ui.min'); ?>