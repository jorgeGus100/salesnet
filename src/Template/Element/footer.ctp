<?= $this->element('sticky_cart') ?>
<footer id="footer" class="row">
        <div id="social-section" class="large-4 columns">
          <p>
            <a href="https://www.facebook.com" style="color: #3D5C9F;"><span><i class="fi-social-facebook"></i></span></a>
            <a href="https://www.twitter.com"><span><i class="fi-social-twitter"></i></span></a>
            <a href="https://plus.google.com" style="color: #DC4E40;"><span><i class="fi-social-google-plus"></i></span></a>
            <a href="https://www.linkedin.com"><span><i class="fi-social-linkedin"></i></span></a>
          </p>
        </div><!-- social-section -->
        <div id="footer-menu" class="large-8 columns">
          <div class="large-6 columns">
            <!-- <h3>Information</h3> -->
            <ul>
              <li><a href="<?php echo $this->Url->build(['controller' => 'pages', 'action' => 'contact_us']) ?>">Contact us</a></li>
              <li><a href="#">Privacy policy</a></li>
              <li><a href="#">Terms & conditions</a></li>
            </ul>
          </div>
          <div class="large-6 columns">
            <!-- <h3>Costumer</h3> -->
            <ul>
              <li><a href="<?php echo $this->Url->build(['controller' => 'pages', 'action' => 'frecuentlyQuestions']); ?>">Help FAQs</a></li>
              <li><a href="#">Buying guide</a></li>
            </ul>
          </div>
        </div><!-- footer-menu -->
        <div id="second-footer-menu" class="large-12 align-center">
          <ul>
            <li><a href="<?php echo $this->Url->build(['controller' => 'pages', 'action' => 'home']); ?>">Home</a></li>
            <li><a href="<?php echo $this->Url->build(['controller' => 'categories', 'action' => 'guestView']); ?>">Categories</a></li>
            <li><a href="<?php echo $this->Url->build(['controller' => 'brands', 'action' => 'guestView']); ?>">Brands</a></li>
            <li><a href="<?php echo $this->Url->build(['controller' => 'productsDiscounts', 'action' => 'guestView']); ?>">Offers</a></li>
            <li><a href="<?php echo $this->Url->build(['controller' => 'pages', 'action' => 'aboutUs']); ?>">About us</a></li>
          </ul>
        </div>
        <div class="copyright">
          <p>Copyright @2016</p>
          <p>Powered by: Xperius S.R.L.</p>
        </div>
      </footer>