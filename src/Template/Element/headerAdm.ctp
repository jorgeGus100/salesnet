<header class="row" id="header">
  <div class="logo-section medium-5 column">
    <a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'dashBoard']); ?>"><img src="<?php echo $this->request->webroot.'/upload/salesLogo.png'?>" class="logo_link"></a>
  </div><!-- logo-section -->
  <div class="top-section medium-7 columns">
    
    <?php $loggedUser = $this->request->session()->read('Auth.User'); ?>
    <ul class="menu top-menu medium-12 columns align-right">
      <?php if($loggedUser): ?>
        
      <?php else: ?>
        <li class=""><a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'add']); ?>">Register</a></li>
      <?php endif; ?>
      <?php if($loggedUser): ?>
        <li class="welcome_li"><span>welcome: <?php echo '<a href="'.$this->Url->build(['controller' => 'users', 'action' => 'profile', $loggedUser['id']]).'">'.$loggedUser['name'].'</a>'; ?></span>
        </li>
      <?php
            endif;
          ?>
      <?php if($loggedUser): ?>
        <li class="logout">
        <a id="logout_link" href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'logout']); ?>">Logout</a>
        </li>

      <?php else: ?>
          <?php
            if($this->request->action == 'login'):
            ?>
            <style type="text/css">
              .top-menu .login{
                display: none;
              }
            </style>
          <?php
            endif;
          ?>
        <li class="login"><a href="#" data-toggle="login-dropdown">Login</a></li>
      <?php endif; ?>
      <?php if($loggedUser): ?>
        <?php //pr($this->Cart->getCartItems($loggedUser['id'])->order_id); ?>
        
      <?php endif; ?>
    </ul>

    <div id="login-dropdown" class="dropdown-pane" data-dropdown data-auto-focus="true" data-close-on-click="true">
      <p id="wrong-data" style="display: none;"><?php echo __('Your username or password is incorrect'); ?></p>
      <a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'login']); ?>" class="main-login-page" style="display: none;">to main login</a>
      <h1>HEADER 2</h1>
        
        <p>Have an account: SIGN IN</p>
        <form id="top-nav-login" action="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'login']); ?>" method="post">
            <div class="row">
                <label>Email Address</label>
                <input type="email" name="username" placeholder="email@example.com" tabindex="1" required/>
            </div>
            <div class="row">
                <label>Password</label>
                <input type="password" name="password" placeholder="********" tabindex="2" required/>
            </div>
            <div class="row submit-row">
              <p class="forgot"><a href="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'forgetpass']); ?>">Forgot your password ?</a></p>
                <?php echo $this->Html->image('loading.gif', ['class' => 'loading-gif']); ?>
                <input type="submit" class="button submit" value="Login" tabindex="3"/>
            </div>
        </form>
    </div><!-- login-dropdown -->


    <div class="search-section medium-9 columns">
      <?= $this->element('search_bar') ?>
    </div>
    <div class="medium-3 columns">
      <select>
        <option value="english">English</option>
        <option value="spanish">Spanish</option>
      </select>
    </div>
  </div><!-- top-section -->
  <div class="title-bar" data-responsive-toggle="main-menu" data-hide-for="medium" id="responsive-toggle-menu">
    <button class="menu-icon" type="button" data-toggle></button>
    <div class="title-bar-title">Menu</div>
  </div>
  
</header>