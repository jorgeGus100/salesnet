<nav class="large-12 columns top-bar" id="main-menu">
  <div class="top-bar-left">
    <ul class="menu vertical medium-horizontal" data-responsive-menu="drilldown medium-dropdown">
      <li><a href="<?php echo $this->Url->build(['controller' => 'pages', 'action' => 'home']); ?>">home</a></li>
      <li class="has-submenu">
        <a href="<?php echo $this->Url->build(['controller' => 'categories', 'action' => 'guestView']); ?>">categories</a>
        <ul class="submenu menu vertical" data-submenu>
          <?php echo $this->cell('Menu::renderCategoriesMenu'); ?>
        </ul>
      </li>
      <li class="has-submenu"><a href="<?php echo $this->Url->build(['controller' => 'brands', 'action' => 'guestView']); ?>">brands</a>
          <ul class="submenu menu vertical" data-submenu>
          <?php echo $this->cell('Menu::renderBrandsMenu'); ?>
        </ul>
      </li>
      <li class="has-submenu"><a href="<?php echo $this->Url->build(['controller' => 'productsDiscounts', 'action' => 'guestView']); ?>">offers</a>
          
      </li>
      <li><a href="<?php echo $this->Url->build(['controller' => 'pages', 'action' => 'aboutUs']); ?>">About Us</a></li>
    </ul>
  </div>
</nav>