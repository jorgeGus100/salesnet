<?php	if(isset($user)): ?>
	<a href="#" data-open="add-cart-prod-<?php echo $product->id; ?>" onclick="return false;" class="button add-cart-trigger"><?php echo __('Add to Cart'); ?></a>
	<div class="reveal form" id="add-cart-prod-<?php echo $product->id; ?>" data-reveal>
		<div class="row add-product">
			<h3 class="product-name small-12 columns modal"><?= h($product->product_name) ?></h3>
			<div class="image large-5 columns">
				<?php if(!empty($product->products_pics)): ?>
					<img src="<?php echo $this->request->webroot.'upload/products_img/'.$product->products_pics[0]['pic_path'] ?>" >
				<?php else: ?>
					<img src="http://placehold.it/350x250">
				<?php endif; ?>
				<!-- <p class="product-discount text-center label success large-centered columns">
					<?php if(!empty($product->products_discounts[0]['discount_percentage'])): ?>
						<?php echo __('Discount'); ?>: <?php echo $product->products_discounts[0]['discount_percentage'].' %'; ?>
					<?php endif; ?>
				</p> -->
			</div><!-- image -->
			<?php if(!empty($product->products_discounts) && $product->products_discounts[0]['discount_percentage'] != 0): ?>
				<p class="product-discount text-center label success large-centered columns">
					<?php echo __('Discount'); ?>: <?php echo $product->products_discounts[0]['discount_percentage'].' %'; ?>
				</p>
			<?php endif; ?>
			<div class="spec large-7 columns">
			<p class="price large-6 columns"><span><?php echo __('Price') ?></span>: <?php echo $this->Number->currency($product->product_price); ?></p>
			<p class="available large-6 columns"><span><?php echo __('Available'); ?></span>: <?php echo $product->kardexes[0]->quantity; ?></p>
				<div class="box_resume">
					<p><span><?php echo __('Code'); ?></span>: <?php echo $product->product_code; ?></p>
					<p><span><?php echo __('Brand'); ?></span>: <?= $product->has('brand') ? $this->Html->link($product->brand->brand_name, ['controller' => 'Brands', 'action' => 'productsBrand', $product->brand->id]) : '' ?></p>
					<p><span><?php echo __('Category'); ?></span>: 
						<?php foreach($product->categories as $cat): ?>
							<?php echo $this->Html->link($cat->category_name, ['controller' => 'Categories', 'action' => 'productsCategory', $cat->id]); ?>
						<?php endforeach; ?>
					</p>
					<p><?php echo $product->product_description; ?></p>
					</div>
						<?php
							echo $this->Form->create('AddCart', ['url' => ['controller' => 'OrderItems', 'action' => 'add'], 'class' => 'add-cart-form']);
							echo $this->Form->input('quantity', ['type' => 'number', 'min' => '1', 'max' => ((!empty($product->kardexes) ? $product->kardexes[0]->quantity : '')), 'autocomplete' => 'off', 'value' => 1]);
							echo $this->Form->input('product_id', ['value' => $product->id, 'type' => 'hidden']);
							echo $this->Form->button(__('Add to cart'), ['class' => 'button add-cart-button']);
							echo $this->Form->end();
					?>
					<?php //echo $this->Html->image('loading.gif', ['class' => 'loading-gif']); ?></p>
			</div><!-- spec -->
		</div><!-- add-product.row -->
	</div><!-- reveal -->
<?php	else:// for unlogged users ?>
		<a href="#" data-open="add-cart-prod-<?php echo $product->id; ?>" onclick="return false;" class="button add-cart-trigger"><?php echo __('Add to Cart'); ?></a>
		<div class="reveal form" id="add-cart-prod-<?php echo $product->id; ?>" data-reveal>
			<div class="row add-product">
				<h3 class="product-name small-12 columns modal"><?= h($product->product_name) ?></h3>
				<div class="image large-5 columns">
					<?php if(!empty($product->products_pics)): ?>
						<img src="<?php echo $this->request->webroot.'upload/products_img/'.$product->products_pics[0]['pic_path'] ?>" >
					<?php else: ?>
						<img src="http://placehold.it/350x250">
					<?php endif; ?>
					<!-- <p class="product-discount text-center label success large-centered columns">
						<?php if(!empty($product->products_discounts[0]['discount_percentage'])): ?>
							<?php echo __('Discount'); ?>: <?php echo $product->products_discounts[0]['discount_percentage'].' %'; ?>
						<?php endif; ?>
					</p> -->
				</div><!-- image -->
				<?php if(!empty($product->products_discounts) && $product->products_discounts[0]['discount_percentage'] != 0): ?>
					<p class="product-discount text-center label success large-centered columns">
						<?php echo __('Discount'); ?>: <?php echo $product->products_discounts[0]['discount_percentage'].' %'; ?>
					</p>
				<?php endif; ?>
				<div class="spec large-7 columns">
				<p class="price large-6 columns"><span><?php echo __('Price') ?></span>: <?php echo $this->Number->currency($product->product_price); ?></p>
				<p class="available large-6 columns"><span><?php echo __('Available'); ?></span>: <?php echo $product->kardexes[0]->quantity; ?></p>
					<div class="box_resume">
						<p><span><?php echo __('Code'); ?></span>: <?php echo $product->product_code; ?></p>
						<p><span><?php echo __('Brand'); ?></span>:<?php //pr($product); ?> <?= $product->has('brand') ? $this->Html->link($product->brand->brand_name, ['controller' => 'Brands', 'action' => 'productsBrand', $product->brand->id]) : '' ?></p>
						<p><span><?php echo __('Category'); ?></span>: 
							<?php foreach($product->categories as $cat): ?>
								<?php echo $this->Html->link($cat->category_name, ['controller' => 'Categories', 'action' => 'productsCategory', $cat->id]); ?>
							<?php endforeach; ?>
						</p>
						<p><?php echo $product->product_description; ?></p>
						<p><?php //$product->has('brand') ? $this->Html->link($product->brand->brand_name, ['controller' => 'Brands', 'action' => 'productsBrand', $product->brand->id]) : '' ?></p>
						</div>
							<?php
								echo $this->Form->create('AddCart', ['url' => ['controller' => 'OrderItems', 'action' => 'add'], 'class' => 'add-cart-form']);
								echo $this->Form->input('quantity', ['type' => 'number', 'min' => '1', 'max' => ((!empty($product->kardexes) ? $product->kardexes[0]->quantity : '')), 'autocomplete' => 'off', 'value' => 1]);
								echo $this->Form->input('product_id', ['value' => $product->id, 'type' => 'hidden']);
								echo $this->Html->link(__('Add to Cart'), ['controller' => 'orderItems', 'action' => 'add', $product->id], ['class' => 'button add-cart-button']);
								echo $this->Form->end();
						?>
						<?php //echo $this->Html->image('loading.gif', ['class' => 'loading-gif']); ?></p>
				</div><!-- spec -->
			</div><!-- add-product.row -->
		</div><!-- reveal -->

<?php	endif; ?>