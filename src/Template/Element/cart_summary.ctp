<?php
	$loggedUser = $this->request->session()->read('Auth.User');
?>

<?php
	$items = $this->Cart->getCartSummary($this->Cart->getCartId($loggedUser['id']))
?>

<!-- for side -->
<div class="dropdown-pane right cart-resume" id="example-dropdown-side" data-dropdown data-hover="true" data-hover-pane="true" data-position-class="side-cart-resume">
  <?php foreach($items as $item): ?>
  		<div class="row">
  		<a href="<?php echo $this->Url->build(['controller' => 'products', 'action' => 'guestView', $item->product->id]); ?>">
  			<div class="product-image">
  				<?php if(!empty($item->product->products_pics)): ?>
  				    <img src="<?php echo $this->request->webroot.'upload/products_img/'.$item->product->products_pics[0]['pic_path'] ?>" alt="">
  				<?php else: ?>
  				    <img src="http://placehold.it/350x250">
  				<?php endif; ?>
  			</div>
  			<h4 class="product_name"><?php echo $item->product->product_name; ?></h4>

  			<p class="text-center"><span class="quantity"><strong><?php echo __('Qty'); ?>:</strong><span><?php echo $item->quantity; ?></span></span><strong><?php echo __('Price'); ?>:</strong><span class="price"><?= $this->Number->currency($item->product->product_price);  ?></span>
  			</p>
  			<p class="text-center">
  				<?php if($item->product->products_discounts[0]->discount_percentage != 0): ?>
  					<strong><?= __('Discount'); ?>:</strong>
  					<?php if(!empty($item->product->products_discounts)): ?>
  						<span class="discount"><?php echo $item->product->products_discounts[0]->discount_percentage; ?>%</span>
  					<?php else: ?>
  						<!-- <legend>Discount</legend> -->
  					<?php endif; ?>
  				<?php endif; ?>
  			</p>
  			</a>
			</div><!-- row -->
	<?php endforeach; ?>
</div>


<!-- for header -->
<div class="dropdown-pane cart-resume" id="example-dropdown-header" data-dropdown data-hover="true" data-hover-pane="true">
  <?php foreach($items as $item): ?>
  		<div class="row">
  		<a href="<?php echo $this->Url->build(['controller' => 'products', 'action' => 'guestView', $item->product->id]); ?>">
  			<div class="product-image">
  				<?php if(!empty($item->product->products_pics)): ?>
  				    <img src="<?php echo $this->request->webroot.'upload/products_img/'.$item->product->products_pics[0]['pic_path'] ?>" alt="">
  				<?php else: ?>
  				    <img src="http://placehold.it/350x250">
  				<?php endif; ?>
  			</div>
  			<h4 class="product_name"><?php echo $item->product->product_name; ?></h4>

  			<p class="text-center"><span class="quantity"><strong><?php echo __('Qty'); ?>:</strong><span><?php echo $item->quantity; ?></span></span><strong><?php echo __('Price'); ?>:</strong><span class="price"><?= $this->Number->currency($item->product->product_price);  ?></span>
  			</p>
  			<p class="text-center">
  				<?php if($item->product->products_discounts[0]->discount_percentage != 0): ?>
  					<strong><?= __('Discount'); ?>:</strong>
  					<?php if(!empty($item->product->products_discounts)): ?>
  						<span class="discount"><?php echo $item->product->products_discounts[0]->discount_percentage; ?>%</span>
  					<?php else: ?>
  						<!-- <legend>Discount</legend> -->
  					<?php endif; ?>
  				<?php endif; ?>
  			</p>
  			</a>
			</div><!-- row -->
	<?php endforeach; ?>
</div>