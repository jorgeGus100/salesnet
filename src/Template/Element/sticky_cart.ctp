<!-- <a href="#" id="cart-sticky">cart link</a> -->
<?php $loggedUser = $this->request->session()->read('Auth.User'); ?>
<?php if($loggedUser): ?>
	<?php $response = $this->Cart->getCartItems($loggedUser['id']); ?>
	<?php if($response['count']): ?>
		<a id="cart-sticky" href="<?php echo $this->Url->build(['controller' => 'orders', 'action' => 'view', $response['cart_id']]) ?>" data-toggle="example-dropdown-side"><i class="fi-shopping-cart"></i> <span class="count"><?php echo $response['count'] ?></span> <span>Items - </span><span class="total">$<?php echo $response['total_order'] ?></span></a>
	<?php else:// there are no items in cart ?>
		<?php if($this->Cart->getCartId($loggedUser['id']) != false):
		?>
		<a id="cart-sticky" href="<?php echo $this->Url->build(['controller' => 'orders', 'action' => 'view', $this->Cart->getCartId($loggedUser['id'])]) ?>" alt="your cart is empty"><i class="empty-cart"><?php echo $this->Html->image('empty-cart.png'); ?></i> <span class="count"><?php echo $response['count'] ?></span> <span>Items - </span><span class="total">$<?php echo $response['total_order'] ?></span></a>
	<?php endif;// if user has cart, it shows up ?>
<?php endif;// !empty getCartItems ?>
<?php endif;// loggedUser ?>

<?= $this->element('cart_summary') ?>