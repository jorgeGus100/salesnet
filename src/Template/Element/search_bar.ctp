<?php echo $this->Form->create('Search', ['url' => ['controller' => 'searches', 'action' => 'search']]); ?>
	<div class="large-10 columns search-input">
		<?php echo $this->Form->input('search', [
			'label' => false,
			'required' => false,
			'autocomplete' => 'off'
		]); ?>
	</div>
	<div class="large-2 columns search-icon">
		<?php
			 echo $this->Form->button('<span class="postfix"><label for="search-form"><i class="fi-magnifying-glass"></i></label></span>', [
			 		'escape' => false
			 	]);
		?>
	</div>
<?php echo $this->Form->end(); ?>
<div id="search-results">
</div>