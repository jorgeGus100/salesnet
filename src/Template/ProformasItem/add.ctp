<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Proformas Item'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Proformas'), ['controller' => 'Proformas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Proforma'), ['controller' => 'Proformas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="proformasItem form large-9 medium-8 columns content">
    <?= $this->Form->create($proformasItem) ?>
    <fieldset>
        <legend><?= __('Add Proformas Item') ?></legend>
        <?php
            echo $this->Form->input('proforma_id', ['options' => $proformas, 'empty' => true]);
            echo $this->Form->input('product_id', ['options' => $products, 'empty' => true]);
            echo $this->Form->input('product_quantity');
            echo $this->Form->input('discount_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
