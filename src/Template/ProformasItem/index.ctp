<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Proformas Item'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Proformas'), ['controller' => 'Proformas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Proforma'), ['controller' => 'Proformas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="proformasItem index large-9 medium-8 columns content">
    <h3><?= __('Proformas Item') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('proforma_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('product_quantity') ?></th>
                <th><?= $this->Paginator->sort('discount_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($proformasItem as $proformasItem): ?>
            <tr>
                <td><?= $this->Number->format($proformasItem->id) ?></td>
                <td><?= $proformasItem->has('proforma') ? $this->Html->link($proformasItem->proforma->id, ['controller' => 'Proformas', 'action' => 'view', $proformasItem->proforma->id]) : '' ?></td>
                <td><?= $proformasItem->has('product') ? $this->Html->link($proformasItem->product->id, ['controller' => 'Products', 'action' => 'view', $proformasItem->product->id]) : '' ?></td>
                <td><?= $this->Number->format($proformasItem->product_quantity) ?></td>
                <td><?= $this->Number->format($proformasItem->discount_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $proformasItem->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $proformasItem->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $proformasItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $proformasItem->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
        
    </div>
</div>
