<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Proformas Item'), ['action' => 'edit', $proformasItem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Proformas Item'), ['action' => 'delete', $proformasItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $proformasItem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Proformas Item'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Proformas Item'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Proformas'), ['controller' => 'Proformas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Proforma'), ['controller' => 'Proformas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="proformasItem view large-9 medium-8 columns content">
    <h3><?= h($proformasItem->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Proforma') ?></th>
            <td><?= $proformasItem->has('proforma') ? $this->Html->link($proformasItem->proforma->id, ['controller' => 'Proformas', 'action' => 'view', $proformasItem->proforma->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $proformasItem->has('product') ? $this->Html->link($proformasItem->product->id, ['controller' => 'Products', 'action' => 'view', $proformasItem->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($proformasItem->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Product Quantity') ?></th>
            <td><?= $this->Number->format($proformasItem->product_quantity) ?></td>
        </tr>
        <tr>
            <th><?= __('Discount Id') ?></th>
            <td><?= $this->Number->format($proformasItem->discount_id) ?></td>
        </tr>
    </table>
</div>
