<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')):
    throw new NotFoundException('Please replace src/Template/Pages/home.ctp with your own version.');
endif;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('foundation-icons/foundation-icons.css') ?>
    <?php echo $this->Html->css('star-rating-svg'); ?>
    <?php echo $this->Html->css('jquery-ui.min'); ?>
    <?= $this->Html->css('app.css') ?>
    <?= $this->fetch('css') ?>
    <?= $this->Html->css('ie-only.css') ?>
</head>
<body class="home">
    <div id="page" class="rowx">
      <?= $this->element('header') ?>
      <!-- #header -->
      <?= $this->Flash->render() ?>
      <?= $this->Flash->render('auth') ?>
      <section class="row" id="body-content">
      <?php 
      foreach($bannerPics as $pic){
        if($pic->position==1){
          $pic1=$pic->pic_path;
          $title1= $pic->title;
          $desc1=$pic->description;
        }
        if($pic->position==2){
          $pic2=$pic->pic_path;
          $title2= $pic->title;
          $desc2=$pic->description;
        }
        if($pic->position==3){
          $pic3=$pic->pic_path;
          $title3= $pic->title;
          $desc4=$pic->description;
        }
        if($pic->position==4){
          $pic4=$pic->pic_path;
          $title4= $pic->title;
          $desc4=$pic->description;
        } 
      } ?>
        <div class="large-12 columns home" id="banner">
          <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
            <ul class="orbit-container">
              <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
              <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>

              <li class="orbit-slide">

                <img src="<?php
                   if(!empty($pic1)){
                    echo $this->request->webroot.$pic1;
                   }else{
                   echo $this->request->webroot.'/img/group-laptop.jpg';
                   }?>" alt="slide 1" height='550' style="display: block; margin: 0 auto;"/>
                <div>
                  <h3 class="text-center"><?php if(!empty($title1)){
                      echo $title1;
                    }else{
                      echo "Original Title";
                      } ?></h3>
                  <p class="text-center"><?php if(!empty($desc1)){
                      echo $desc1;
                    }else{
                      echo "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde harum rem";
                      } ?>
                  .</p>
                </div>

              </li>
              <li class="orbit-slide">

              <img src="<?php
                   if(!empty($pic2)){
                    echo $this->request->webroot.$pic2;
                   }else{
                   echo $this->request->webroot.'/img/group-multi-coloured-modern-cars-43654371.jpg';
                   }?>" alt="slide 1" height='550' style="display: block; margin: 0 auto;"/>
                <div>
                  <h3 class="text-center"><?php if(!empty($title2)){
                      echo $title2;
                    }else{
                      echo "Original Title 2";
                      } ?></h3>
                  <p class="text-center"><?php if(!empty($desc2)){
                      echo $desc2;
                    }else{
                      echo "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde harum rem";
                      } ?>
                  .</p>
                </div>
                
              </li>
              <li class="orbit-slide">
                 <img src="<?php
                   if(!empty($pic3)){
                    echo $this->request->webroot.$pic3;
                   }else{
                   echo $this->request->webroot.'/img/group-young-women-26028571.jpg';
                   }?>" alt="slide 1" height='550' style="display: block; margin: 0 auto;"/>
                <div>
                  <h3 class="text-center"><?php if(!empty($title3)){
                      echo $title3;
                    }else{
                      echo "Original Title 3";
                      } ?></h3>
                  <p class="text-center"><?php if(!empty($desc3)){
                      echo $desc3;
                    }else{
                      echo "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde harum rem";
                      } ?>
                  .</p>
                </div>
                
              </li>
              <li class="orbit-slide">
              <img src="<?php
                   if(!empty($pic4)){
                    echo $this->request->webroot.$pic4;
                   }else{
                   echo $this->request->webroot.'/img/group-men-fashion-tip.jpg';
                   }?>" alt="slide 1" height='550' style="display: block; margin: 0 auto;"/>
                <div>
                  <h3 class="text-center"><?php if(!empty($title4)){
                      echo $title4;
                    }else{
                      echo "Original Title 4";
                      } ?></h3>
                  <p class="text-center"><?php if(!empty($desc4)){
                      echo $desc4;
                    }else{
                      echo "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde harum rem";
                      } ?>
                  .</p>
                </div>
              

              </li>
            </ul>
            <nav class="orbit-bullets">
              <button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
              <button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
              <button data-slide="2"><span class="show-for-sr">Third slide details.</span></button>
              <button data-slide="3"><span class="show-for-sr">Fourth slide details.</span></button>
            </nav>
          </div><!-- .orbit -->
        </div><!-- #banner -->
        <div class="large-12 columns featured" id="featured-products">
          <!-- <p>SUGGESTED PRODUCTS</p> -->
          <?php foreach($featuredProducts as $fProduct): ?>
            <div class="col5-unit columns product">
              <a href="<?php
                echo $this->Url->build([
                  "controller" => "Products",
                  "action" => "guestView",
                  $fProduct['id']
                ]);
              ?>">
                <?php if(!empty($fProduct['products_pics'])): ?>
                  <img src="<?php echo $this->request->webroot.'upload/products_img/'.$fProduct['products_pics'][0]['pic_path']; ?>" class="cover">
                <?php else: ?>
                  <img src="http://placehold.it/150x150" alt="">
                <?php endif; ?>
                <div class="product-data">
                  <p class="product-name"><?php echo $fProduct['product_name']; ?></p>
                  <p class="produc-price text-center">price: <?php echo $this->Number->currency($fProduct['product_price']); ?></p>
                  <p class="product-rate">
                    <?php if($fProduct->getRating()): ?>
                      <div class="product-rating" data-rating="<?php echo $fProduct->getRating(); ?>" data-readonly="true"></div>
                    <?php else: ?>
                      <span class="star-rating"><?php echo $this->Html->image('blank-star-rating.png'); ?></span>
                    <?php endif; ?>
                  </p>
                  <?php echo $this->element('add_cart_btn', ['product' => $fProduct, 'loggedUser' => (isset($user) ? $user : '')]); ?>
                </div>
              </a>
            </div>
          <?php endforeach; ?>
        </div><!-- featured-products -->
        <div class="large-12 columns featured" id="featured-brands">
          <!-- <p>FEATURED BRANDS</p> -->
          <?php foreach($featuredBrands as $fBrand): ?>
            <div class="col5-unit columns brand">
              <a href="<?php echo $this->Url->build([
              'controller' => 'Brands',
              'action' => 'productsBrand',
              $fBrand['id']
              ]); ?>">
                <img src="http://placehold.it/150x150" alt="">
                <div>
                  <p class="brand-name"><?php echo $fBrand->brand_name; ?></p>
                </div>
              </a>
            </div>
          <?php endforeach; ?>
        </div><!-- featured-brands -->
      </section>
      <?= $this->element('footer') ?>
    </div>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('jquery.waypoints.min.js') ?>
    <?= $this->Html->script('inview.min.js') ?>
    <?= $this->Html->script('foundation.js') ?>
    <?= $this->Html->script('js.cookie.js') ?>
    <?php echo $this->Html->script('jquery.star-rating-svg'); ?>
    <?php echo $this->Html->script('jquery-ui.min'); ?>
    <?php echo $this->Html->script('jquery.zoom'); ?>
    <?= $this->Html->script('app.js') ?>
    <?= $this->fetch('script') ?>
</body>
</html>
