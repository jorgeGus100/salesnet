<?php	$this->Html->addCrumb('Contact Us', null); ?>
<?php if(isset($msg)): ?>
	<div class="row">
		<div class="medium-6 medium-centered large-6 large-centered columns thanks-message">
			<span class="close rounded thick"></span>
			<h2><?php echo __('Thanks'); ?></h2>
			<p><?php echo $msg; ?></p>
			<?php echo $this->Html->link(__('Return to SalesNet'), ['controller' => 'Pages', 'action' => 'home']); ?>
		</div>
	</div>
<?php else: ?>

<div class="row">
	<div class="medium-6 medium-centered large-6 large-centered columns">
		<h3><?php echo __('Contact Us'); ?></h3>
		<?php echo $this->Form->create('Contact', ['url' => ['controller' => 'Pages', 'action' => 'contactUs']]); ?>
			<?php echo $this->Form->input('name', [
				'required' => true,
				'autocomplete' => 'off',
				'value' => (isset($user) ? $user->name.' '.$user->lastname : '')
				]); ?>
			<?php echo $this->Form->input('email', [
				'required' => true,
				'autocomplete' => 'off',
				'value' => (isset($user) ? $user->email : '')
				]); ?>
			<?php echo $this->Form->input('message', [
				'type' => 'textarea',
				'required' => true,
				'autocomplete' => 'off'
				]); ?>
			<?php	echo $this->Form->button('Send', [
				'class' => ['button float-right']
				]);	?>
				<?= captcha_image_html() ?>
				<!-- Captcha code user input textbox -->
				<?= $this->Form->input('CaptchaCode', [
					'label' => 'Retype the characters from the picture:',
					'maxlength' => '10',
					'style' => 'width: 270px;',
					'id' => 'CaptchaCode',
					'class' => 'float-left'
					]) ?>
			<?php echo $this->Form->end(); ?>
	</div>
</div>

<?= $this->Html->css(captcha_layout_stylesheet_url(), ['inline' => false]) ?>

<?php endif; ?>