<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('foundation-icons/foundation-icons.css') ?>
    <?php echo $this->Html->css('jquery-ui.min'); ?>
    <?= $this->Html->css('app.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?= $this->Html->css('ie-only.css') ?>
</head>
<body class="<?php echo ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $this->request->params['controller'])), '_'); ?> <?php echo ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $this->request->action)), '_'); ?>">
    <div id="page" class="rowx">
      <?= $this->element('header') ?>
      <!-- #header -->
    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth') ?>

    <div class="row" id="body-content">
        <div class="breadcrumb-wrapper columns" role="navigation">
            <?php
                echo $this->Html->getCrumbList(
                    [
                        'firstClass' => false,
                        'lastClass' => 'active',
                        'class' => 'breadcrumbs'
                    ],
                    'Home'
                );
            ?>
        </div>
        <?= $this->fetch('content') ?>
    </div>
    <?= $this->element('footer') ?>
    </div>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('jquery.waypoints.min.js') ?>
    <?= $this->Html->script('inview.min.js') ?>
    <?= $this->Html->script('foundation.js') ?>
    <?= $this->Html->script('js.cookie.js') ?>
    <?php echo $this->Html->script('jquery-ui.min'); ?>
    <?= $this->fetch('scriptBottom') ?>
    <?= $this->Html->script('app.js') ?>
</body>
</html>
