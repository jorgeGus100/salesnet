<?php
    $this->Html->addCrumb('Banner', ['controller' => 'BannerPics', 'action' => 'index']);
    $this->Html->addCrumb('New', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Banner Pics'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bannerPics form large-9 medium-8 columns content">
    <?= $this->Form->create($bannerPic, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Add Banner Pic') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('description');
            echo $this->Form->input('status', array(
                                'options' => array('0'=>'Inactive', '1'=>'Active'),
                                'empty' => '(choose one)',
                                'label' => false
                                ));
            echo $this->Form->input('position', array(
                                'options' => array('1'=>'Frist', '2'=>'Second', '3'=>'third', '4'=>'fourth'),
                                'empty' => '(choose one)',
                                'label' => false
                                ));
           
            echo $this->Form->input('upload', ['type' => 'file']);
            echo '<div id="image-holder"></div>';
        ?>
    </fieldset>
   <?= $this->Form->button(__('Submit'), [
        'class' => 'button submit'
    ]) ?>
    <?= $this->Form->end() ?>
</div>
<?= $this->Html->script('jquery.min.js') ?>
<?php echo $this->Html->script('upload.js') ?>

<script>

</script>
