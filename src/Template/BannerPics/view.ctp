<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Banner Pic'), ['action' => 'edit', $bannerPic->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Banner Pic'), ['action' => 'trash', $bannerPic->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bannerPic->title)]) ?> </li>
        <li><?= $this->Html->link(__('List Banner Pics'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Banner Pic'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bannerPics view large-9 medium-8 columns content">
   
    <table class="vertical-table">
        
        <tr>
            <th></th>
            <td><?php 
                if(!empty($bannerPic->pic_path)){
                $pic=$this->request->webroot.$bannerPic->pic_path;
            }else{
                $pic=$this->request->webroot.'/upload/empty.png';
            }
             echo '<img src="'.$pic.'">'; ?></td>
        </tr>
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($bannerPic->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= $this->Text->autoParagraph(h($bannerPic->description)); ?></td>
        </tr>
        
    </table>
    
</div>
