<?php
    $this->Html->addCrumb('Banner', ['controller' => 'BannerPics', 'action' => 'index']);
    $this->Html->addCrumb('Edit', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'trash', $bannerPic->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bannerPic->title)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Banner Pics'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bannerPics form large-9 medium-8 columns content">
    <?= $this->Form->create($bannerPic, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Edit Banner Pic') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('description');
            echo $this->Form->input('status', array(
                                'options' => array('0'=>'Inactive', '1'=>'Active'),
                                'empty' => '(choose one)',
                                'label' => false
                                ));
            echo $this->Form->input('position', array(
                                'options' => array('1'=>'Frist', '2'=>'Second', '3'=>'third', '4'=>'fourth'),
                                'empty' => '(choose one)',
                                'label' => false
                                ));
            echo $this->Form->input('upload', ['type' => 'file']);
            if(!empty($bannerPic->pic_path)){
                $pic=$this->request->webroot.$bannerPic->pic_path;
            }else{
                $pic=$this->request->webroot.'/upload/empty.png';
            }
            echo '<div id="image-holder"><img src="'.$pic.'"></div>';
        ?>
    </fieldset>
     <?= $this->Form->button(__('Submit'), ['class' => 'button submit']) ?>
    <?= $this->Form->end() ?>
</div>
