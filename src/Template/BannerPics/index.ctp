<?php $this->Html->addCrumb('Banner', null); ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Banner Picture'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bannerPics index large-9 medium-8 columns content">
    
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('status') ?></th>
                <th><?= $this->Paginator->sort('position') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bannerPics as $bannerPic): ?>
            <tr>
                
                <td><?= h($bannerPic->title) ?></td>
                <td><?php if($bannerPic->status==0){
                            echo 'Inactive';
                        }else{
                            echo 'Active';
                        }
                    ?></td>
                <td><?= $this->Number->format($bannerPic->position) ?></td>
                <td><?= h($bannerPic->created) ?></td>
                <td><?= h($bannerPic->modified) ?></td>
                <td class="actions">
                   
                    <a href="<?php echo $this->Url->build(['controller' => 'bannerPics', 'action' => 'view/'.$bannerPic->id]) ?>"><i class="fi-eye"></i></a>
                  
                    <a href="<?php echo $this->Url->build(['controller' => 'bannerPics', 'action' => 'edit/'.$bannerPic->id]) ?>"><i class="fi-pencil"></i></a>

                    <a href="<?php echo $this->Url->build(['controller' => 'bannerPics', 'action' => 'trash/'.$bannerPic->id]) ?>" onclick="if (confirm('Are you sure you wish to delete this banner picture?')) { return true; } return false;"><i class="fi-trash"></i></a>
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
