<?php $this->Html->addCrumb('Offers', null); ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Products Discount'), ['action' => 'add']) ?></li>
        
    </ul>
</nav>
<div class="productsDiscounts index large-9 medium-8 columns content">
    <h3><?= __('Offers') ?></h3>
    <table cellpadding="0" cellspacing="0" class="text-center">
        <thead>
            <tr>
                
                <th class="text-center"><?= $this->Paginator->sort('name') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('percentage') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('product_id') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('init_date') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('end_date') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productsDiscounts as $productsDiscount): ?>
            <tr>
                
                <td><?= $this->Html->link(h($productsDiscount->discount_name), ['action' => 'view', $productsDiscount->id]) ?></td>
                <td><span class="price"><?= $this->Number->toPercentage($productsDiscount->discount_percentage) ?></span></td>
                <td><?= $productsDiscount->has('product') ? $this->Html->link($productsDiscount->product->product_name, ['controller' => 'Products', 'action' => 'view', $productsDiscount->product->id]) : '' ?></td>
                <td class="center"><?= h($productsDiscount->init_date) ?></td>
                <td class="center"><?= h($productsDiscount->end_date) ?></td>
                <td class="actions">
                    
                    <a href="<?php echo $this->Url->build(['controller' => 'productsDiscounts', 'action' => 'view/'.$productsDiscount->id]) ?>"><i class="fi-eye"></i></a>
                     <a href="<?php echo $this->Url->build(['controller' => 'productsDiscounts', 'action' => 'edit/'.$productsDiscount->id]) ?>"><i class="fi-pencil"></i></a>
                    <a href="<?php echo $this->Url->build(['controller' => 'productsDiscounts', 'action' => 'trash/'.$productsDiscount->id]) ?>" onclick="if (confirm('Are you sure you wish to delete this Offer?')) { return true; } return false;"><i class="fi-trash"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
        
    </div>
</div>
