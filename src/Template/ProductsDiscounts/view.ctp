<?php
    $this->Html->addCrumb('Offers', ['controller' => 'ProductsDiscounts', 'action' => 'index']);
    $this->Html->addCrumb('View', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Products Discount'), ['action' => 'edit', $productsDiscount->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Products Discount'), ['action' => 'delete', $productsDiscount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsDiscount->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Products Discounts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Products Discount'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="productsDiscounts view large-9 medium-8 columns content">
    <h3><?= h($productsDiscount->discount_name) ?></h3>
    <table class="vertical-table">
        
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $productsDiscount->has('product') ? $this->Html->link($productsDiscount->product->product_name, ['controller' => 'Products', 'action' => 'view', $productsDiscount->product->id]) : '' ?></td>
        </tr>
        
        <tr>
            <th><?= __('Discount Percentage') ?></th>
            <td><?= $this->Number->format($productsDiscount->discount_percentage) ?></td>
        </tr>
        <tr>
            <th><?= __('Init Date') ?></th>
            <td><?= h($productsDiscount->init_date) ?></td>
        </tr>
        <tr>
            <th><?= __('End Date') ?></th>
            <td><?= h($productsDiscount->end_date) ?></td>
        </tr>
    </table>
</div>
