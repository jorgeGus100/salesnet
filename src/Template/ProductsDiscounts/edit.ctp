<?php
    $this->Html->addCrumb('Offers', ['controller' => 'ProductsDiscounts', 'action' => 'index']);
    $this->Html->addCrumb('Edit', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $productsDiscount->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $productsDiscount->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Products Discounts'), ['action' => 'index']) ?></li>
       
    </ul>
</nav>
<div class="productsDiscounts form large-9 medium-8 columns content">
    <?= $this->Form->create($productsDiscount) ?>
    <fieldset>
        <legend><?= __('Edit Offers') ?></legend>
        <?php
            echo $this->Form->input('discount_name');
            echo $this->Form->input('discount_percentage');
            echo $this->Form->input('product_id', ['options' => $products]);
            echo $this->Form->input('init_date');
            echo $this->Form->input('end_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
