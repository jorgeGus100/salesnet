<?php   $this->Html->addCrumb('Offers', null); ?>
<div class="large-12 columns featured" id="featured-products">
          <?php foreach($allDisc as $key => $disc): ?>
            <?php //pr($disc); ?>
        <?php if((($key + 5) % 5) == 0): ?>
          <div class="row-discount row">
        <?php endif; ?>
          <div class="col5-unit columns discounts">
          <a href="<?php
                echo $this->Url->build([
                  "controller" => "Products",
                  "action" => "guestView",
                  $disc->product->id
                ]);
              ?>">
          <div class="image"><?php if(!empty($disc->product->products_pics)): ?>
                    <img src="<?php echo $this->request->webroot.'upload/products_img/'.$disc->product->products_pics[0]['pic_path']; ?>" class="cover">
                  <?php else: ?>
                    <img src="http://placehold.it/150x150" alt="" class="cover">
                  <?php endif; ?></div>
                <div class="product-data">
                  <p class="product-name text-center"><?php echo $disc->product->product_name; ?></p>
                  <p class="produc-price text-center">price: <?php echo $this->Number->currency($disc->product->product_price); ?></p>
                  <p class="product-rate">
                    <?php if($disc->product->getRating()): ?>
                      <div class="product-rating" data-rating="<?php echo $disc->product->getRating(); ?>" data-readonly="true"></div>
                    <?php else: ?>
                      <span class="star-rating"><?php echo $this->Html->image('blank-star-rating.png'); ?></span>
                    <?php endif; ?>
                  </p>
                  <p class="product-discount text-center label success">Discount: <?php echo $disc->discount_percentage; ?>%</p>
                </div>
                <?php echo $this->element('add_cart_btn', ['product' => $disc->product, 'loggedUser' => (isset($user) ? $user : '')]); ?>
          </a>
          </div><!-- col5-unit -->
        <?php if((($key + 5) % 5) == 4): ?>
          </div>
        <?php endif; ?>
           <?php endforeach; ?>
</div>
<div class="row">
  <ul class="pagination text-center" role="navigation" aria-label="Pagination">
   <?php if($this->Paginator->counter('{{pages}}') > 1):// if pagination is required ?>
    <?php echo $this->Paginator->prev('&laquo; previous', [
      'class' => 'pagination-previous disabled',
      'tag' => 'li',
      'escape' => false
      ]); ?>
      <?php echo $this->Paginator->numbers();// prints out the numbers ?>
      <?php echo $this->Paginator->next('next &raquo;', [
       'tag' => 'li',
       'class' => 'pagination-next',
       'escape' => false
       ]); ?>
     <?php endif; ?>
   </ul>
</div>
<?php echo $this->Html->script('jquery.star-rating-svg', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('star-rating-svg'); ?>
<?php echo $this->Html->script('jquery.zoom', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->script('jquery-ui.min', ['block' => 'scriptBottom']); ?>
<?php echo $this->Html->css('jquery-ui.min'); ?>