<?php
    $this->Html->addCrumb('Offers', ['controller' => 'ProductsDiscounts', 'action' => 'index']);
    $this->Html->addCrumb('New', null);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Products Discounts'), ['action' => 'index']) ?></li>
        
    </ul>
</nav>
<div class="productsDiscounts form large-9 medium-8 columns content">
    <?= $this->Form->create($productsDiscount) ?>
    <fieldset>
        <legend><?= __('Add Products Discount') ?></legend>
        <?php
            echo $this->Form->input('discount_name');
            echo $this->Form->input('discount_percentage');
            echo $this->Form->input('product_id', ['options' => $products]);
            echo $this->Form->input('init_date');
            echo $this->Form->input('end_date');
        ?>
    </fieldset>
<?= $this->Form->button(__('Submit'), [
        'class' => 'button submit'
    ]) ?>
    <?= $this->Form->end() ?>
</div>
