<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Products Pic'), ['action' => 'edit', $productsPic->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Products Pic'), ['action' => 'delete', $productsPic->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsPic->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Products Pics'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Products Pic'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="productsPics view large-9 medium-8 columns content">
    <h3><?= h($productsPic->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pic Path') ?></th>
            <td><?= h($productsPic->pic_path) ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $productsPic->has('product') ? $this->Html->link($productsPic->product->id, ['controller' => 'Products', 'action' => 'view', $productsPic->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($productsPic->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= $this->Number->format($productsPic->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $this->Number->format($productsPic->status) ?></td>
        </tr>
    </table>
</div>
