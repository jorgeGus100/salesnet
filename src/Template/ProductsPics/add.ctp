<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Products Pics'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productsPics form large-9 medium-8 columns content">
    <?= $this->Form->create($productsPic, ['enctype' => 'multipart/form-data']); ?>
    <fieldset>
        <legend><?= __('Add Products Pic') ?></legend>
        <?php
            echo $this->Form->input('upload', ['type' => 'file']);
            echo '<div id="image-holder"></div>';
           // echo $this->Form->input('pic_path');
            echo $this->Form->input('products_id', ['options' => $products]);
            echo $this->Form->input('description');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), [
        'class' => 'button submit'
    ]) ?>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->script('upload',['block' => true]); ?>