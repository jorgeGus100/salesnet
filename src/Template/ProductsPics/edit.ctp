<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $productsPic->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $productsPic->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Products Pics'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productsPics form large-9 medium-8 columns content">
    <?= $this->Form->create($productsPic) ?>
    <fieldset>
        <legend><?= __('Edit Products Pic') ?></legend>
        <?php
            echo $this->Form->input('pic_path');
            echo $this->Form->input('products_id', ['options' => $products]);
            echo $this->Form->input('description');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), [
        'class' => 'button submit'
    ]) ?>
    <?= $this->Form->end() ?>
</div>
