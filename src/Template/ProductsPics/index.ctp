<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Products Pic'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productsPics index large-9 medium-8 columns content">
    <h3><?= __('Products Pics') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pic_path') ?></th>
                <th><?= $this->Paginator->sort('products_id') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th><?= $this->Paginator->sort('status') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productsPics as $productsPic): ?>
            <tr>
                <td><?= $this->Number->format($productsPic->id) ?></td>
                <td><?= h($productsPic->pic_path) ?></td>
                <td><?= $productsPic->has('product') ? $this->Html->link($productsPic->product->id, ['controller' => 'Products', 'action' => 'view', $productsPic->product->id]) : '' ?></td>
                <td><?= $this->Number->format($productsPic->description) ?></td>
                <td><?= $this->Number->format($productsPic->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $productsPic->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $productsPic->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $productsPic->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsPic->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev(' ') ?>
            <?= $this->Paginator->numbers() ?>
          
            <?= $this->Paginator->next(' ') ?>
            
        </ul>
        
    </div>
</div>
