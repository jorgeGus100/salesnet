<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Brands Controller
 *
 * @property \App\Model\Table\BrandsTable $Brands
 */
class BrandsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['productsBrand', 'guestView']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $brandsList = $this->Brands->find('all')
        ->where(['brand_status' => 1]);

        $brands = $this->paginate($brandsList);
        $this->set(compact('brands'));
        $this->set('_serialize', ['brands']);
        }
    }
   public function guestView()
    {
        $user = null;
        if($this->Auth->user() != null){
            $user = TableRegistry::get('Users')->get($this->Auth->user()['id']);
        }
        $brandsList = $this->Brands->find('all')
        ->where(['brand_status' => 1])->contain(['Products' => function($q){
            return $q->where(['Products.product_status' => 1]);
        }, 'Products.Brands', 'Products.Kardexes', 'Products.ProductsPics', 'Products.Categories', 'Products.ProductsDiscounts']);
        $this->loadModel('Products');
        $this->loadModel('ProductsPics');
        $brands = $this->paginate($brandsList);
        $allBrands = $brandsList->toArray();
        $allBrands2 = $brandsList->toArray();

        $this->set(compact('allBrands', 'allBrands2', 'user'));
        $this->set('_serialize', ['allBrands']);
    }

    /**
     * View method
     *
     * @param string|null $id Brand id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $brand = $this->Brands->get($id, [
            'contain' => []
        ]);

        $this->set('brand', $brand);
        $this->set('_serialize', ['brand']);
        }
    }

    public function productsBrand($id = null)
    {
        $user = null;
        if($this->Auth->user() != null){
            $user = TableRegistry::get('Users')->get($this->Auth->user()['id']);
        }
        $brand = $this->Brands->get($id, [
            'contain' => ['Products' => function($q){
                return $q->where(['Products.product_status' => 1]);
            }, 'Products.Brands', 'Products.Kardexes', 'Products.ProductsPics', 'Products.Categories', 'Products.ProductsDiscounts']
        ]);

        $this->loadModel('Products');
        $this->loadModel('ProductsPics');
        $this->loadModel('ProductsCategories');
        $this->loadModel('ProductsDiscounts');
        $prodBrand = $this->Products->find('all', array('conditions'=>array('brands_id'=>$id, 'product_status'=>1)));
        $prodBrandList = $prodBrand->toArray();
        $productsList = array();
        $picList = array();
        $n = 0;
       // pr($prodBrandList);
        foreach ($prodBrandList as $prod) {
            $productsList[$n]['product']= $this->Products->get($prod->id);
            $pic= $this->ProductsPics->find()->where(['products_id'=>$prod->id]);
            $productsList[$n]['product']['pic']= $pic->toArray();
            $n++;
        }

        //$products = $this->Products->find('all', array('conditions'=>array($prodCat[products_id]=>$id)));
        $this->set('brand', $brand);
        $this->set('user', $user);
        $this->set('productsList', $productsList);
        $this->set('_serialize', ['brand']);
        $this->set('_serialize', ['productsList']);
    }


    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $brand = $this->Brands->newEntity();
        if ($this->request->is('post')) {
            $brand = $this->Brands->patchEntity($brand, $this->request->data);
            if (!empty($this->request->data['upload']['name'])) {

                    $file = $this->request->data['upload']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                    $setNewFileName = "upload\brands_img\\".time() . "_" . rand(000000, 999999);
                   if (in_array($ext, $arr_ext)) {
                    
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . $setNewFileName . '.' . $ext);
                    $brand->logo = $setNewFileName . '.' . $ext;
                    
                    }else{
                        $this->Flash->error(__('The extension of your image must be png, jpg, jpeg or gif.'));
                    }
            }

            if ($this->Brands->save($brand)) {
                $this->Flash->success(__('The brand has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The brand could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('brand'));
        $this->set('_serialize', ['brand']);
    }
}

    /**
     * Edit method
     *
     * @param string|null $id Brand id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $brand = $this->Brands->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $brand = $this->Brands->patchEntity($brand, $this->request->data);
            if (!empty($this->request->data['upload']['name'])) {

                    $file = $this->request->data['upload']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                    $setNewFileName = "upload\brands_img\\".time() . "_" . rand(000000, 999999);
                   if (in_array($ext, $arr_ext)) {
                    
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . $setNewFileName . '.' . $ext);
                    $brand->logo = $setNewFileName . '.' . $ext;
                    
                    }else{
                        $this->Flash->error(__('The extension of your image must be png, jpg, jpeg or gif.'));
                    }
            }
            if ($this->Brands->save($brand)) {
                $this->Flash->success(__('The brand has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The brand could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('brand'));
        $this->set('_serialize', ['brand']);
       }
    }

    public function trash($id = null){
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $brand = $this->Brands->get($id, [
            'contain' => []
        ]);
        $brand->brand_status = 2;
        if ($this->Brands->save($brand)) {
                $this->Flash->success(__('The brand has been eliminated.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The brand could not be eliminated. Please, try again.'));
            }
        }    
    }

    /**
     * Delete method
     *
     * @param string|null $id Brand id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $this->request->allowMethod(['post', 'delete']);
        $brand = $this->Brands->get($id);
        if ($this->Brands->delete($brand)) {
            $this->Flash->success(__('The brand has been deleted.'));
        } else {
            $this->Flash->error(__('The brand could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
}
