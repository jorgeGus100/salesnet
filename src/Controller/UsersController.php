<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
// use Cake\Mailer\Email;
use Cake\Routing\Router;
use App\Lib\Fonts;
use App\Model\Entity\User;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Network\Email\Email;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
    class UsersController extends AppController
    {

        /**
         * Index method
         *
         * @return \Cake\Network\Response|null
         */
        //private $user
        public function initialize()
        {
            parent::initialize();
            $this->Auth->allow(['add','logout','forgetPass','activate','resetPassUser','welcome','newActivationTok']);
            $this->loadComponent('RequestHandler');
            //$this->loadComponent('Captcha', ['field'=>'securitycode']);
        }
        /*public function beforeFilter(Event $event){
         //  $this->viewBuilder()->layout(false);

           
        }*/
        

        public function index()
        {

            if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'welcome']);
            }else{
                $this->viewBuilder()->layout('adminDash');
                $this->paginate = [
                    'contain' => ['Roles']
                ];
                $userList = $this->Users->find('all')
                ->where(['status' => 1]);

        //$categories = $this->paginate($categoriesList);
                $users = $this->paginate($userList);

                $this->set(compact('users'));
                $this->set('_serialize', ['users']);
            }
        }
        public function welcome(){
            if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'welcome']);
            }else{
                $this->viewBuilder()->layout('adminDash');
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }
        }

        /**
         * View method
         *
         * @param string|null $id User id.
         * @return \Cake\Network\Response|null
         * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
         */
        public function view($id = null)
        {
            if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
            $this->viewBuilder()->layout('adminDash');
            $user = $this->Users->get($id, [
                'contain' => ['Roles']
            ]);

            $this->set('user', $user);
            $this->set('_serialize', ['user']);
            }
        }    

        /**
         * Add method
         *
         * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
         */
        public function add()
        {
            if($this->Auth->user('role_id')==1){
                
                $this->viewBuilder()->layout('adminDash');
            }
            $this->loadComponent('CakeCaptcha.Captcha', [
                'captchaConfig' => 'LoginCaptcha'
            ]);
            $user = $this->Users->newEntity();
            if ($this->request->is('post')) {
                $user = $this->Users->patchEntity($user, $this->request->data);
                    if(!isset($user['role_id'])){
                        $user['role_id']=3;
                    }
                    if(!isset($user['phone'])){
                        $user['phone']=0;
                    }
                    if(!isset($user['location'])){
                        $user['location']='-';
                    }
                    if(!isset($user['address'])){
                        $user['address']='-';
                    }
                    if(!isset($user['preferences'])){
                        $user['preferences']='-';
                    }
                   
                    if($this->duplicateEmail($this->request->data['email'])){

                      $userList=$this->Users->find('all',
                       array('conditions'=>array('Users.email'=>$this->request->data['email'])));
                       $user = $userList->toArray();
                       $this->loadModel('InterTokens');
                       $tokList=$this->InterTokens->find('all',array('conditions'=>array('InterTokens.users_id'=>$user[0]['id'])));
                       $token = $tokList->toArray();
                       foreach ($token as $tok) {
                             $tokMod = $this->InterTokens->get($tok['id']);
                             $tokMod->status = 0;
                             $this->InterTokens->save($tokMod);
                         }  
                      $dupEmail = $this->request->data['email'];
                      $title = 'Duplicate registration';
                      $content = '<p>We have your email in our records, if you follow this link we will send another email to activate your account </p><p>New email to activate your account <a href="'.Router::url(['controller' => 'users', 'action' => 'newActivationTok/'.$dupEmail]) .'">New Email</a></p>'; 

                      return $this->newLinkReq($title, $content);
                    }
                $user['status']=4;

                $isHuman = captcha_validate($this->request->data['CaptchaCode']);
                unset($this->request->data['CaptchaCode']);
                if($isHuman):
                if ($this->Users->save($user)) {
                    $sendEmail= $user->email;
                    $token = $this->__generateToken($user->id);
                    /*
                    $url2 = Router::url([
                      'controller' => 'Users',
                      'action' => 'activate',
                      $token
                    ]);
                    $ms='To activate your account please follow this link: http://192.168.88.30/'.$url2;
                    $email = new Email();
                    $email->transport('mailjet');
                    try{
                        $res = $email->from(['christian.xperius@gmail.com' => 'SalesNet site'])
                        ->to($sendEmail)
                        ->subject('Activate account')
                        ->send($ms);
                    }catch (Exception $e) {
                        echo 'Exception : ',  $e->getMessage(), "\n";
                    }*/
                    $this->Flash->success(__('The user has been saved.'));

                    if($this->Auth->user('role_id')==1){
                        return $this->redirect(['action' => 'index']);
                    }else{
                        $title = 'Succefull Registration';
                        $content = '<p>We send an email to <h3>'.$sendEmail.'</h3> with a link for activate your account.</p>';
                        return $this->standardMs($title, $content);
                    }
                } else {
                        
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));

                }
                else:
                    $this->Flash->error(__('The captcha code is wrong.'));
                endif;
            }
            $roles = $this->Users->Roles->find('list', ['limit' => 200]);
            $role = 0;
            if(!empty($this->Auth->user('role_id'))){
                $role = $this->Auth->user('role_id');
            }
            $this->set(compact('user', 'roles','role'));
            $this->set('_serialize', ['user']);
            }
       function duplicateEmail($email){
               
                $userList=$this->Users->find('all',
                       array('conditions'=>array('Users.email'=>$email)));
                $user = $userList->toArray();
                if(empty($user)){
                    return false;
                }else{
                    return true;
                }
               
       }     
       function standardMs($title, $content){
             $this->set(compact('title', 'content'));
             $this->set('_serialize', ['ms']);
             $this->render('standardMs');
        } 
        
        function newLinkReq($title, $content){
             $this->set(compact('title', 'content'));
             $this->set('_serialize', ['ms']);
             $this->render('standardMs');
        } 

        /**
         * Edit method
         *
         * @param string|null $id User id.
         * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
         * @throws \Cake\Network\Exception\NotFoundException When record not found.
         */
        public function edit($id = null)
        {
            if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
            $this->viewBuilder()->layout('adminDash');    
            $user = $this->Users->get($id, [
                'contain' => []
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $user = $this->Users->patchEntity($user, $this->request->data);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The user has been saved.'));

                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                }
            }
            $roles = $this->Users->Roles->find('list', ['limit' => 200]);
            $this->set(compact('user', 'roles'));
            $this->set('_serialize', ['user']);
        }
       } 
       public function trash($id = null){
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $user = $this->Users->get($id);

        $user->status = 2;
        if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been eliminated.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be eliminated. Please, try again.'));
            }

        }
    }
        /**
         * Delete method
         *
         * @param string|null $id User id.
         * @return \Cake\Network\Response|null Redirects to index.
         * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
         */
        public function delete($id = null)
        {
            if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
            $this->request->allowMethod(['post', 'delete']);
            $user = $this->Users->get($id);
            if ($this->Users->delete($user)) {
                $this->Flash->success(__('The user has been deleted.'));
            } else {
                $this->Flash->error(__('The user could not be deleted. Please, try again.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }
        /*
         *reset by user   
         */
        public function changePass($id = null)
        {
            $user = $this->Users->get($id, [
                'contain' => []
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $sendPass= $this->request->data['password'];
                $sendNewPass = $this->request->data['new_password'];
                $sendRepPass = $this->request->data['confirm_password'];
                $verify = new DefaultPasswordHasher();
                $comp=$verify->check($sendPass, $user->password);
                if(!$comp){
                     $this->Flash->error(__('The old password is incorrect'));
                     return $this->redirect(['action' => 'index']);
                }else{
                    if(empty($sendNewPass)||empty($sendRepPass)){
                        $this->Flash->error(__('Fill new password and confirm'));
                    }else{
                        if($sendNewPass!==$sendRepPass){
                            $this->Flash->error(__('New password and Confirm have to match'));
                        }else{
                            $user->password = $sendNewPass;
                            $this->Users->save($user);
                            $this->Flash->success(__('The password was update.'));
                            return $this->redirect(['action' => 'index']);
                        }

                    }
                }
                
            }
            $roles = $this->Users->Roles->find('list', ['limit' => 200]);
            $this->set(compact('user', 'roles'));
            $this->set('_serialize', ['user']);
        }

        public function login()
        {
            if(($this->Auth->user() !== null) && !empty($this->Auth->user())){
                $this->Flash->success(__('You are already logged.'));
                if($this->Auth->user()['role_id'] == 3){
                    $this->redirect(['controller' => 'Pages', 'action' => 'home']);
                } else{
                    // admin case
                    $this->redirect(['controller' => 'Users', 'action' => 'dashBoard']);
                }
            }
            $session = $this->request->session();
            $this->loadComponent('CakeCaptcha.Captcha', [
                'captchaConfig' => 'LoginCaptcha'
            ]);
            if (!$session->check('Login.attemp')) {
                    $session->write('Login.attemp', '1');
                }
            if ($this->request->is('post')) {

                if($this->request->is('ajax')) {
                   
                    $isLogged['response'] = false;
                    $isLogged['user'] = $this->request->data['username'];
                    $isLogged['activation'] = false;
                    $isLogged['reset'] = false;
                    $isLogged['admin'] = false;
                    //$isLogged['admin'] = false;

                    $hasher = new DefaultPasswordHasher();

                    $user = $this->Users->find()
                        ->where(['email' => $this->request->data['username']])
                    ;
                    if($user->count()){
                        if($user->first()->status===4){
                             $isLogged['activation'] = true;
                             //$this->Flash->error('Your password was reset by admin, check your email.');
                              echo json_encode($isLogged);
                              die();
                        }elseif($user->first()->status===5){
                             $isLogged['reset'] = true;
                             //$this->Flash->error('Your password was reset by admin, check your email.');
                              echo json_encode($isLogged);
                              die();
                        }else{
                            $passwordHashed = $hasher->check($this->request->data['password'], $user->first()->password);
                        }
                    } else{
                        echo json_encode($isLogged);
                        die();
                    }
                    if($passwordHashed){
                        $this->Auth->setUser($user->first());
                        $isLogged['response'] = true;
                        if($this->Auth->user('role_id')==1){
                            $isLogged['admin'] = true;
                            
                        }
                        echo json_encode($isLogged);
                    } else{
                        echo json_encode($isLogged);
                    }
                    die();
                }
                
                $isHuman = captcha_validate($this->request->data['CaptchaCode']);
                unset($this->request->data['CaptchaCode']);
                $user = $this->Auth->identify();
                
                if(isset($user['status'])&&($user['status']==5)){
                            
                            $this->Flash->error('Your password was reset by admin, check your email.');
                            //return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
                
                }elseif (isset($user['status'])&&($user['status']==4)) { 
                        $this->Flash->error('Check your email to activate your account.');
                }else{
                    if ($user && $isHuman) {
                        $this->Auth->setUser($user);
                          if($user['role_id']===1)
                            {   
                                
                                return $this->redirect(['action' => 'dashBoard']);
                          }else{    
                                
                                return $this->redirect($this->Auth->redirectUrl());
                           }
                    }else{
                         $attemps = $session->read('Login.attemp');
                         if($attemps<30){
                            $attemps+=1;
                            $session->write('Login.attemp', $attemps);
                            $this->Flash->error('Your username or password is incorrect.');
                        }else{
                            $this->Flash->error('The attemps to login are more than 3.');
                        }
                    } 
                }

            }
        }
        public function loginCaptcha()
        {
            $session = $this->request->session();
            $this->loadComponent('Captcha');
             $this->Users->setCaptcha('securitycode', $this->Captcha->getCode('securitycode')); //captcha
            if (!$session->check('Login.attemp')) {
                    $session->write('Login.attemp', '1');
                }
            if ($this->request->is('post')) {
               
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                }else{
                     $attemps = $session->read('Login.attemp');
                     if($attemps<3){
                        $attemps+=1;
                        $session->write('Login.attemp', $attemps);  
                        $this->Flash->error('Your username or password is incorrect.');
                    }else{
                        $this->Flash->error('The attemps to login are more than 3.');
                    }
                }
            }// is post
        }

        public function forgetPass(){
            if ($this->request->is(['patch', 'post', 'put'])) {
                $sendEmail= $this->request->data['email'];
                $userList=$this->Users->find('all',
                       array('conditions'=>array('Users.email'=>$sendEmail)));
                $user = $userList->toArray();
                $token = $this->__generateToken($user[0]['id']);
                $url2 = Router::url([
                    'controller' => 'Users',
                    'action' => 'resetPass',
                    $token
                ]);
                // supplying provisionally the ip address for testing only
                $ms='To recover your account please follow this link: http://192.168.88.30/'.$url2;

                if(empty($user)){
                    $this->Flash->error('There are not user associated with this email.');
                }else{
                    $email = new Email();
                    $email->transport('mailjet');
                    try{
                        $res = $email->from(['christian.xperius@gmail.com' => 'SalesNet site'])
                        ->to($sendEmail)
                        ->subject('Forget Password')
                        ->send($ms);
                    }catch (Exception $e) {
                        echo 'Exception : ',  $e->getMessage(), "\n";
                    }


                    $msg = __('An email has been sended to your account, please check your inbox.');
                    $this->set(compact('msg'));
                 }
                }
        }
        function __generateToken($user_id=null)
        {
            if (empty($user_id)) {
                return null;
            }
            $length = 50;
            $bytes = openssl_random_pseudo_bytes($length);
            $token  = bin2hex($bytes);
            $this->loadModel('InterTokens');
            $intToken = $this->InterTokens->newEntity();
            $intToken->users_id = $user_id;
            $intToken->token= $token;
            $intToken->generate = $time = strtotime("now");
            // pr($intToken);
            $this->InterTokens->save($intToken);
            return $token;
        }
        function __validToken($token_created_at) {
            $expired = strtotime($token_created_at) + 86400;
            $time = strtotime("now");
            if ($time < $expired) {
                return true;
            }
            return false;
        }

        public function activate($tok=null){
            if(is_null($tok)){
                $this->Flash->error('Incorrect information.');
                return $this->redirect(['action' => 'login']);
            }else{
                $this->loadModel('InterTokens');

                $tokList=$this->InterTokens->find('all', 
                       array('conditions'=>array('InterTokens.token'=>$tok)));
                $token = $tokList->toArray();
                $tokMod = $this->InterTokens->get($token[0]['id']);

                if(!empty($token[0]['generate'])){
                    $valTok = $token[0]['generate'];
                 }else{
                    $this->Flash->error('Your token is invalid.');
                     return $this->redirect(['action' => 'login']);
                 }   
                if($this->__validToken($valTok)){
                    if($this->__notUsedToken($valTok)){

                      $users_id = $token[0]['users_id'];
                      $user = $this->Users->get($users_id, [
                        'contain' => []
                      ]);
                     $user->status = 1;
                     $this->Users->save($user);
                     $tokMod->status = 0;
                     $this->InterTokens->save($tokMod);
                     $this->Flash->success('Your account was activated.');
                     return $this->redirect(['action' => 'welcome']);
                    }else{
                        //used toked message      
                    }
                }else{
                    //new token request
                    $this->Flash->error('Your token is older that 24 hours.');
                }

            }
        }

        public function resetPass($tok=null){
            if(is_null($tok)){
                $this->Flash->error('Incorrect path.');
                return $this->redirect(['action' => 'login']);
            }else{
                $this->loadModel('InterTokens');
                $tokList=$this->InterTokens->find('all', 
                       array('conditions'=>array('InterTokens.token'=>$tok)));
                $token = $tokList->toArray();
                $valTok = $token[0]['generate'];
                if($this->__validToken($valTok)){
                    
                     return $this->redirect(['action' => 'newPassword', $token[0]['users_id']]);
                }else{
                    $this->Flash->error('Your token is older that 24 hours.');
                }

            }
        }

        public function newPassword($users_id=null)
        {
            $user = $this->Users->get($users_id, [
                'contain' => []
            ]);
             if ($this->request->is(['patch', 'post', 'put'])) {
                $sendNewPass = $this->request->data['new_password'];
                $sendRepPass = $this->request->data['confirm_password'];
                if(empty($sendNewPass)||empty($sendRepPass)){
                        $this->Flash->error(__('Fill new password and confirm'));
                    }else{
                        if($sendNewPass!==$sendRepPass){
                            $this->Flash->error(__('New password and Confirm have to match'));
                        }else{
                            if(!preg_match('/^[a-zA-Z0-9 \! \( \) \[ \] \{ \} \? \- \_ \/ \. \Q \ \E ]*$/',$sendNewPass)){
                                $this->Flash->error(__('The password must have only letters and characters "!()[]{}.\"'));

                            }else if(!preg_match('/^(?=.*[\! \( \) \[ \] \{ \} \- \_ \. \/ \Q \ \E \?])(?=.*[a-z])(?=.*[A-Z])/',$sendNewPass)){
                                $this->Flash->error(__('Include one character "!()[]{}.\" one capital letter and one lower case'));

                            }else if(strlen($sendNewPass)<6){
                                $this->Flash->error(__('Password needs to be 6 characters or more'));
                            }else{

                            $user->password = $sendNewPass;
                            $user->status = 1;
                            $this->Users->save($user);
                            $this->Flash->success(__('The password was update.'));
                            return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
                            }
                        }

                    }

             }
            $this->set(compact('user'));
            $this->set('_serialize', ['user']);
        }

    public function resetPassAdmin ($id = null){
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
             $user = $this->Users->get($id);
             $user->status = 5;
             if ($this->Users->save($user)) {
                    $sendEmail= $user->email;
                    $token = $this->__generateToken($user->id);
                    $url2 = Router::url([
                      'controller' => 'Users',
                      'action' => 'resetPass',
                      $token
                    ]);
                    // pr($url2);

                // supplying provisionally the ip address for testing only
                $ms='To recover your account please follow this link: http://192.168.88.30/'.$url2;
                $email = new Email();
                    $email->transport('mailjet');
                    try{
                        $res = $email->from(['christian.xperius@gmail.com' => 'SalesNet site'])
                        ->to($sendEmail)
                        ->subject('Reset Password')
                        ->send($ms);
                    }catch (Exception $e) {
                        echo 'Exception : ',  $e->getMessage(), "\n";
                    }
                    $this->Flash->success(__('The password was reset.'));
                    return $this->redirect(['action' => 'dashBoard']);
                } else {
                    $this->Flash->error(__('The password could not be reset. Please, try again.'));
                }   

         }
    }
     public function newActivationTok ($email){
        $userList=$this->Users->find('all',
                       array('conditions'=>array('Users.email'=>$email)));
        $user = $userList->toArray();
        //pr($user);
        $sendEmail= $user[0]['email'];
        $token = $this->__generateToken($user[0]['id']);
        $url2 = Router::url([
                'controller' => 'Users',
                'action' => 'activate',
                $token]);
                $ms='To ACTIVATE your account please follow this link: http://192.168.88.30/'.$url2;
                $email = new Email();
                    $email->transport('mailjet');
                    try{
                        $res = $email->from(['christian.xperius@gmail.com' => 'SalesNet site'])
                        ->to($sendEmail)
                        ->subject('Activate account')
                        ->send($ms);
                    }catch (Exception $e) {
                        echo 'Exception : ',  $e->getMessage(), "\n";
                    }
                    $this->Flash->success(__('The new email for activate your account was send.'));
                    //return $this->redirect(['action' => 'dashBoard']);
            }   
   

public function resetPassUser ($id = null){
           
            $user = $this->Users->get($id);
            if($user->id==$this->Auth->user()->id){
             $user->status = 5;
             if ($this->Users->save($user)) {
                    $sendEmail= $user->email;
                    $token = $this->__generateToken($user->id);
                    $url2 = Router::url([
                      'controller' => 'Users',
                      'action' => 'resetPass',
                      $token
                    ]);
                    // pr($url2);

                // supplying provisionally the ip address for testing only
                $ms='To recover your account please follow this link: http://192.168.88.30/'.$url2;
                $email = new Email();
                    $email->transport('mailjet');
                    try{
                        $res = $email->from(['christian.xperius@gmail.com' => 'SalesNet site'])
                        ->to($sendEmail)
                        ->subject('Reset Password')
                        ->send($ms);
                    }catch (Exception $e) {
                        echo 'Exception : ',  $e->getMessage(), "\n";
                    }
                    $this->Flash->success(__('The password was reset.'));
                    $this->Auth->logout();
                    return $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error(__('The password could not be reset. Please, try again.'));
                }   
         } else{
                 $this->Flash->error(__('You are not authorized to perform this action.'));
            }   
       }   

    public function dashBoard(){
        //$user = $this->Auth->identify();
        if($this->Auth->user('role_id')==1)
        {   
                            //$this->viewBuilder()->layout(false);
                            //$this->viewBuilder()->layout('adminDash');
           $this->viewBuilder()->layout('adminDash'); 
        }else{    
            
            return $this->redirect($this->Auth->redirectUrl());
        }

        $featuredProducts = TableRegistry::get('Products')->getFeaturedProducts();
        $featuredBrands = TableRegistry::get('Brands')->getFeaturedBrands();
       $this->paginate = [
                'contain' => ['Roles']
            ];
            $users = $this->paginate($this->Users);

            $this->set(compact('users','featuredProducts', 'featuredBrands'));
            $this->set('_serialize', ['users']); 
    }    
    
    public function logout()
    {
        //$this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }
    /**
     * user profile
     */
    public function profile($id = null){
        if($this->Auth->user('role_id')==1){
            $this->viewBuilder()->layout('adminDash'); 
        }
        if($this->request->is(['patch', 'post', 'put'])){
            $user = $this->Users->get($this->request->data['id']);
            $prefData = array('categories' => $this->request->data['pref_categories'], 'brands' => $this->request->data['pref_brands'], 'interest' => ((isset($this->request->data['pref_interest'])) ? $this->request->data['pref_interest'] : ''), 'email_sending' => $this->request->data['pref_emailsending']);

            $this->request->data['preferences'] = json_encode($prefData);
            $user = $this->Users->patchEntity($user, $this->request->data());
            // pr('entering');die();
            if($this->Users->save($user)) {
                $this->Flash->success(__('The user profile has been saved.'));

            }
        }//else{
            $user = $this->Users->get($id, [
                'contain' => []
            ]);
            if($user->phone == 0){
                $user->phone = null;
            }
            $userEntity = new User();
            $genderValues = $userEntity->getGenderValues();
            $choosedPreferences = json_decode($user->preferences);
            // categories
            $categories = TableRegistry::get('Categories')->find('all')->toArray();
            $categoryValues = array();

            foreach ($categories as $cat) {
                $categoryValues[$cat->id] = $cat->category_name;
            }
            // brands
            $brands = TableRegistry::get('Brands')->find('all')->toArray();
            $brandsValues = array();
            foreach ($brands as $brand) {
                $brandsValues[$brand->id] = $brand->brand_name;
            }
            // $choosedBrand = $choosedPreferences->brands;

            // interests
            $interests = TableRegistry::get('Preferences')->find('all')->toArray();
            $interestsValues = array();
            foreach ($interests as $intval) {
                $interestsValues[$intval->value] = $intval->name;
            }
            // sending mail
            $sendingOptions = array(0 => 'never', 1 => 'every week', 2 => 'every month');
            $this->set(compact('user', 'genderValues', 'categoryValues', 'brandsValues', 'interestsValues', 'sendingOptions', 'choosedPreferences'));
        //}
    }
    public function isAuthorized($user){
        if(in_array($this->request->action, ['edit', 'profile'])){
            $profileId = (int)$this->request->params['pass'][0];
            $authUser = $this->Auth->user();
            if($this->Users->isOwnedBy($profileId, $authUser)){
                return true;
            }
        }
        return parent::isAuthorized($user);
    }
}

