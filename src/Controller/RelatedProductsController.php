<?php
	namespace App\Controller;

	use App\Controller\Controller;
	use Cake\Event\Event;
	use Cake\ORM\TableRegistry;

	class RelatedProductsController extends AppController {

		public function manage(){
			$productId = $this->request->pass[0];
			$currentEntity = $this->RelatedProducts->Products->get($productId);
			$this->viewBuilder()->layout('adminDash');
			$products = $this->RelatedProducts->Products->find('all')->where(['product_status' => 1])
			->toArray();
			// customizing output
			$categories = TableRegistry::get('Categories')->find()
				->where(['Categories.category_status' => 1])
				->contain(['Products' => function(\Cake\ORM\Query $query){
					return $query->select(['id', 'product_name'])
		        ->where(['Products.product_status' => 1, 'Products.id IS NOT' => $this->request->pass[0] ]);
				}])//->toArray()
			;
			$template = array();
			foreach ($categories->toArray() as $category) {
				// pr($category->products);
				$template[$category->id] = array();
				foreach ($category->products as $product) {
					$template[$category->id][$product->id] = $product->product_name;
				}
			}
			$template2 = $this->paginate($categories, ['scope' => 'categories', 'limit' => 4]);
			//
			$productValues = array();

			foreach ($products as $product) {
				if($product->id == $productId){
					continue;
				}
				$productValues[$product->id] = $product->product_name;
			}

			$currentData = $this->RelatedProducts->find('all')->where(['RelatedProducts.product_id' => $currentEntity->id])->toArray();
			// pr($currentData);
			$matched = array();
			// verifying the data, for updating
			foreach ($currentData as $data) {
				array_push($matched, $data->related_product_id);
			}
			$clearAll = false;
			$allCollection = array();

			if($this->request->is('post', 'put')) {
				$productId = $this->request->data['product_id'];
				// go through data
				foreach ($this->request->data as $key => $data) {
					if(is_array($data) && !empty($data)){
						if(!empty($data)) {
							foreach ($data as $eachValue) {
								array_push($allCollection, $eachValue);
							}
						}
					} else{
						if($key == 'product_id'){
							$productId = $data;
						}
						if($productId != null && empty($key)){
							$clearAll = true;
						}
					}
				}// foreach v2 each category
				if($clearAll){
					$this->RelatedProducts->deleteAll(['product_id' => $productId]);// ok
				}
				// pr($allCollection);
				$toSaving = array();
				foreach ($allCollection as $value) {
					if(in_array($value, $matched)) {
						// nothing
					} else{
						$toSaving[] = array('product_id' => $productId, 'related_product_id' => $value);
					}
				}
				$toDeleteGlobal = array_diff($matched, $allCollection);
				$prodIdsPerpage = array();
				foreach ($template2->toArray() as $cat) {
					foreach ($cat->products as $product) {
						array_push($prodIdsPerpage, $product->id);
					}
				}
				// verifying if id is in page
				$toDeletingPerPage = array();
				foreach ($toDeleteGlobal as $val) {
					if(in_array($val, $prodIdsPerpage)){
						array_push($toDeletingPerPage, $val);
					}
				}

				if(!empty($toDeletingPerPage)) {
					$this->deleteByIds($toDeletingPerPage, $productId);
				}
				$entities = $this->RelatedProducts->newEntities($toSaving);
				/*pr($toSaving);// ok
				die();*/
				if($this->RelatedProducts->saveMany($entities)) {
				}
				$this->Flash->success(__('The product association has been saved.'));
				return $this->redirect(['controller' => 'Products', 'action' => 'edit', $productId]);
			}

			$this->set('products', $productValues);
			$this->set(compact('productId', 'matched', 'currentEntity', 'template', 'template2'));
		}

		private function deleteByIds($ids, $productId){
	    $query = $this->RelatedProducts->find()
		    ->where(['related_product_id IN' => $ids, 'product_id' => $productId]);
			foreach ($query->toArray() as $id) {
				$this->RelatedProducts->delete($id);
			}
		}
	}