<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Roles Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 */
class RolesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
         $this->viewBuilder()->layout('adminDash');         
        $roles = $this->paginate($this->Roles);

        $this->set(compact('roles'));
        $this->set('_serialize', ['roles']);
    }
}
    /**
     * View method
     *
     * @param string|null $id Role id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $role = $this->Roles->get($id, [
           
        ]);

        $this->set('role', $role);
        $this->set('_serialize', ['role']);
    }
}

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $role = $this->Roles->newEntity();
        if ($this->request->is('post')) {
            $role = $this->Roles->patchEntity($role, $this->request->data);
            if ($this->Roles->save($role)) {
                $this->Flash->success(__('The role has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The role could not be saved. Please, try again.'));
            }
        }
        $internalfunctions = $this->Roles->Internalfunctions->find('list', ['limit' => 200]);
        $this->set(compact('role', 'internalfunctions'));
        $this->set('_serialize', ['role']);
    }
}

    /**
     * Edit method
     *
     * @param string|null $id Role id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $role = $this->Roles->get($id, [
            'contain' => ['Internalfunctions']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $role = $this->Roles->patchEntity($role, $this->request->data);
            if ($this->Roles->save($role)) {
                $this->Flash->success(__('The role has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The role could not be saved. Please, try again.'));
            }
        }
        $internalfunctions = $this->Roles->Internalfunctions->find('list', ['limit' => 200]);
        $this->set(compact('role', 'internalfunctions'));
        $this->set('_serialize', ['role']);
    }
}
    /**
     * Delete method
     *
     * @param string|null $id Role id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $this->request->allowMethod(['post', 'delete']);
        $role = $this->Roles->get($id);
        if ($this->Roles->delete($role)) {
            $this->Flash->success(__('The role has been deleted.'));
        } else {
            $this->Flash->error(__('The role could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
}
