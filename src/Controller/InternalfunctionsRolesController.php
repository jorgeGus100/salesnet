<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * InternalfunctionsRoles Controller
 *
 * @property \App\Model\Table\InternalfunctionsRolesTable $InternalfunctionsRoles
 */
class InternalfunctionsRolesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Roles', 'Internalfunctions']
        ];
        $internalfunctionsRoles = $this->paginate($this->InternalfunctionsRoles);

        $this->set(compact('internalfunctionsRoles'));
        $this->set('_serialize', ['internalfunctionsRoles']);
    }

    /**
     * View method
     *
     * @param string|null $id Internalfunctions Role id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $internalfunctionsRole = $this->InternalfunctionsRoles->get($id, [
            'contain' => ['Roles', 'Internalfunctions']
        ]);

        $this->set('internalfunctionsRole', $internalfunctionsRole);
        $this->set('_serialize', ['internalfunctionsRole']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $internalfunctionsRole = $this->InternalfunctionsRoles->newEntity();
        if ($this->request->is('post')) {
            $internalfunctionsRole = $this->InternalfunctionsRoles->patchEntity($internalfunctionsRole, $this->request->data);
            if ($this->InternalfunctionsRoles->save($internalfunctionsRole)) {
                $this->Flash->success(__('The internalfunctions role has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The internalfunctions role could not be saved. Please, try again.'));
            }
        }
        $roles = $this->InternalfunctionsRoles->Roles->find('list', ['limit' => 200]);
        $internalfunctions = $this->InternalfunctionsRoles->Internalfunctions->find('list', ['limit' => 200]);
        $this->set(compact('internalfunctionsRole', 'roles', 'internalfunctions'));
        $this->set('_serialize', ['internalfunctionsRole']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Internalfunctions Role id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $internalfunctionsRole = $this->InternalfunctionsRoles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $internalfunctionsRole = $this->InternalfunctionsRoles->patchEntity($internalfunctionsRole, $this->request->data);
            if ($this->InternalfunctionsRoles->save($internalfunctionsRole)) {
                $this->Flash->success(__('The internalfunctions role has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The internalfunctions role could not be saved. Please, try again.'));
            }
        }
        $roles = $this->InternalfunctionsRoles->Roles->find('list', ['limit' => 200]);
        $internalfunctions = $this->InternalfunctionsRoles->Internalfunctions->find('list', ['limit' => 200]);
        $this->set(compact('internalfunctionsRole', 'roles', 'internalfunctions'));
        $this->set('_serialize', ['internalfunctionsRole']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Internalfunctions Role id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $internalfunctionsRole = $this->InternalfunctionsRoles->get($id);
        if ($this->InternalfunctionsRoles->delete($internalfunctionsRole)) {
            $this->Flash->success(__('The internalfunctions role has been deleted.'));
        } else {
            $this->Flash->error(__('The internalfunctions role could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
