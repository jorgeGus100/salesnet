<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * ProductsPics Controller
 *
 * @property \App\Model\Table\ProductsPicsTable $ProductsPics
 */
class ProductsPicsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products']
        ];
        $productsPics = $this->paginate('ProductsPics');

        $this->set(compact('productsPics'));
        $this->set('_serialize', ['productsPics']);
    }

    /**
     * View method
     *
     * @param string|null $id Products Pic id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productsPic = TableRegistry::get('ProductsPics')->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('productsPic', $productsPic);
        $this->set('_serialize', ['productsPic']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('ProductsPics');
        $productsPic = $this->ProductsPics->newEntity();
        if(!empty($this->request->data))
        {
            if (!empty($this->request->data['upload']['name'])) {
                $file = $this->request->data['upload']; 
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                $setNewFileName = time() . "_" . rand(000000, 999999);
               if (in_array($ext, $arr_ext)) {
               
                move_uploaded_file($file['tmp_name'], WWW_ROOT . 'upload\products_img\\' . $setNewFileName . '.' . $ext);
                $imageFileName = $setNewFileName . '.' . $ext;
                }else{
                    $this->Flash->error(__('The extension of your image must be png, jpg, jpeg or gif.'));
                }
            }
        }else{
            $this->Flash->error(__('Please add an image.'));
        }
        if ($this->request->is('post')) {
            $productsPic = $this->ProductsPics->patchEntity($productsPic, $this->request->data);
            $productsPic->pic_path = $imageFileName;
            if ($this->ProductsPics->save($productsPic)) {
                $this->Flash->success(__('The products pic has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The products pic could not be saved. Please, try again.'));
            }
        }
        $products = $this->ProductsPics->Products->find('list', ['limit' => 200]);
        $this->set(compact('productsPic', 'products'));
        $this->set('_serialize', ['productsPic']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Products Pic id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('ProductsPics');
        $productsPic = $this->ProductsPics->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsPic = $this->ProductsPics->patchEntity($productsPic, $this->request->data);
            if ($this->ProductsPics->save($productsPic)) {
                $this->Flash->success(__('The products pic has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The products pic could not be saved. Please, try again.'));
            }
        }
        $products = $this->ProductsPics->Products->find('list', ['limit' => 200]);
        $this->set(compact('productsPic', 'products'));
        $this->set('_serialize', ['productsPic']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Products Pic id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productsPic = $this->ProductsPics->get($id);
        if ($this->ProductsPics->delete($productsPic)) {
            $this->Flash->success(__('The products pic has been deleted.'));
        } else {
            $this->Flash->error(__('The products pic could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
