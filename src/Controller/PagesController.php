<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Form\SearchForm;
use Cake\Routing\Router;
use App\Form\ContactForm;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function initialize(){
        parent::initialize();
        $this->Auth->allow(['display', 'contactUs', 'aboutUs', 'home','frecuentlyQuestions']);
        $this->loadComponent('CakeCaptcha.Captcha', [
          'captchaConfig' => 'ContactCaptcha'
        ]);
    }

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $user = null;
        if($this->Auth->user() != null){
            $user = TableRegistry::get('Users')->get($this->Auth->user()['id']);
        }
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $search = new SearchForm();

        $featuredProducts = TableRegistry::get('Products')->getFeaturedProducts();
        $featuredBrands = TableRegistry::get('Brands')->getFeaturedBrands();
        $bannerPics = TableRegistry::get('BannerPics')->getFeaturedBannerPics();
       
        $this->set(compact('page', 'subpage', 'featuredProducts', 'featuredBrands','bannerPics', 'listCategories', 'search', 'user'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    function aboutUs(){
       // $this->render('aboutUs');
    }

    function frecuentlyQuestions(){
       // $this->render('aboutUs');
    }
    function welcome(){
       // $this->render('aboutUs');
    }
    /**
     * contact us action
     */
    public function contactUs(){
        if($this->Auth->user() != null)
            $user = TableRegistry::get('Users')->get($this->Auth->user()['id']);
        $form = new ContactForm();
        if($this->request->is('post')){
            $isHuman = captcha_validate($this->request->data['CaptchaCode']);
            unset($this->request->data['CaptchaCode']);

            if(isset($isHuman) && $isHuman == true){
                $result = $form->execute($this->request->data);
                $msg = __('Your message has been successfully sent <br> and we appreciate you contacting us. <br> We\'ll be in touch soon.');
                if($result) {
                    $this->Flash->success(__('Thanks for your time, your message has been sended.'));

                    $this->set(compact('msg'));
                } else{
                    $this->Flash->error(__('There are server issues, please try again.'));
                }
            }
        }
        $this->set(compact('form', 'user'));
    }
}
