<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Proformas Controller
 *
 * @property \App\Model\Table\ProformasTable $Proformas
 */
class ProformasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Sellers']
        ];
        $proformas = $this->paginate($this->Proformas);

        $this->set(compact('proformas'));
        $this->set('_serialize', ['proformas']);
    }

    /**
     * View method
     *
     * @param string|null $id Proforma id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $proforma = $this->Proformas->get($id, [
            'contain' => ['Users', 'Sellers', 'ProformasItem']
        ]);

        $this->set('proforma', $proforma);
        $this->set('_serialize', ['proforma']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $proforma = $this->Proformas->newEntity();
        if ($this->request->is('post')) {
            $proforma = $this->Proformas->patchEntity($proforma, $this->request->data);
            if ($this->Proformas->save($proforma)) {
                $this->Flash->success(__('The proforma has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The proforma could not be saved. Please, try again.'));
            }
        }
        $users = $this->Proformas->Users->find('list', ['limit' => 200]);
        $sellers = $this->Proformas->Sellers->find('list', ['limit' => 200]);
        $this->set(compact('proforma', 'users', 'sellers'));
        $this->set('_serialize', ['proforma']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Proforma id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $proforma = $this->Proformas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $proforma = $this->Proformas->patchEntity($proforma, $this->request->data);
            if ($this->Proformas->save($proforma)) {
                $this->Flash->success(__('The proforma has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The proforma could not be saved. Please, try again.'));
            }
        }
        $users = $this->Proformas->Users->find('list', ['limit' => 200]);
        $sellers = $this->Proformas->Sellers->find('list', ['limit' => 200]);
        $this->set(compact('proforma', 'users', 'sellers'));
        $this->set('_serialize', ['proforma']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Proforma id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $proforma = $this->Proformas->get($id);
        if ($this->Proformas->delete($proforma)) {
            $this->Flash->success(__('The proforma has been deleted.'));
        } else {
            $this->Flash->error(__('The proforma could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
