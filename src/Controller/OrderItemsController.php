<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Order;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Routing\Router;

/**
 * OrderItems Controller
 *
 * @property \App\Model\Table\OrderItemsTable $OrderItems
 */
class OrderItemsController extends AppController
{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->deny();
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products', 'Orders']
        ];
        $orderItems = $this->paginate($this->OrderItems);

        $this->set(compact('orderItems'));
        $this->set('_serialize', ['orderItems']);
    }

    /**
     * View method
     *
     * @param string|null $id Order Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $orderItem = $this->OrderItems->get($id, [
            'contain' => ['Products', 'Orders']
        ]);

        $this->set('orderItem', $orderItem);
        $this->set('_serialize', ['orderItem']);
    }
    /**
     * for array outputting
     */
    protected function prepareArrayForView($data){
        $productsIds = array();
        foreach ($data as $key => $value) {
            $productsIds[] = $value;
        }
        $productsData = $this->OrderItems->Products->find('all', [
            'condition' => $data
        ]);
        $output = array();
        foreach ($productsData as $pdata) {
            $output[$pdata->id] = $pdata->product_name;
        }
        return $output;
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $orderItem = $this->OrderItems->newEntity();
        // to add product by id
        if(isset($this->request->pass[0])){
            if($this->OrderItems->Products->exists(['id' => $this->request->pass[0]])){
                $itemToAdd = $this->OrderItems->Products->get($this->request->pass[0], ['contain' => ['ProductsPics', 'Brands', 'ProductsDiscounts', 'Kardexes', 'Categories', 'RelatedProducts']]);
                $listDiscounts = $itemToAdd->products_discounts;
                $listPics = $itemToAdd->products_pics;
                $orderItem->product_id = $itemToAdd->id;
                $loggedUserId = $this->request->session()->read('Auth.User')['id'];
                if(isset($loggedUserId)){
                    $loggedUser = TableRegistry::get('Users')->get($loggedUserId, ['contain' => ['Orders']]);
                    $userCart = $loggedUser->getCart();
                    if($userCart != false){
                        $inCart = $userCart->isProductInCart($itemToAdd->id);
                        $cartQuantity = $userCart->inCartQuantity($itemToAdd->id);
                    }
                    $loggedUserC = TableRegistry::get('Users')->get($loggedUserId);
                }
                $relatedProducts = $itemToAdd->getRelatedProducts();
            } else{
                $this->Flash->error(__('The product has not be found, please try again.'));

                return $this->redirect(['controller' => 'products', 'action' => 'index']);
            }
        }
        if ($this->request->is('post')) {
            if($this->request->data['product_id']){
                $itemToAdd = $this->OrderItems->Products->get($this->request->data['product_id'], ['contain' => ['ProductsPics', 'Brands', 'ProductsDiscounts', 'Kardexes', 'Categories']]);
            }
            $orderItem = $this->OrderItems->patchEntity($orderItem, $this->request->data);
            $userId = $this->request->session()->read('Auth.User');
            $userEntity = TableRegistry::get('Users')->get($userId['id']);
            $ordersAssociated = TableRegistry::get('Orders')->find()
                ->where([
                    'Orders.users_id' => $userEntity->id,
                    'Orders.status' => 0
                ])->toArray()
            ;
            if(empty($ordersAssociated)){
                $order = TableRegistry::get('Orders')->newEntity();
                $order->users_id = $userEntity->id;
                $order->status = 0;// on hold by default
                // saving for getting the order_id
                $savedOrder = TableRegistry::get('Orders')->save($order);
                $orderItem->order_id = $savedOrder->id;
            } else{
                $orderItem->order_id = $ordersAssociated[0]->id;
            }

            if ($this->OrderItems->save($orderItem)) {
                if($this->request->is('ajax')){
                    $itemToAdd = $this->OrderItems->Products->get($this->request->data['product_id'], ['contain' => ['Kardexes']]);
                }
                /*// updating inventory
                $updatedInventory = $itemToAdd->kardexes[0]['quantity'] - $this->request->data['quantity'];
                $itemToAdd->kardexes[0]->quantity = $updatedInventory;
                $this->OrderItems->Products->Kardexes->save($itemToAdd->kardexes[0]);*/

                $this->Flash->success(__('The product has been saved.'));
                if($this->request->is('ajax')) {
                    $response = array('response' => 'true');
                    echo json_encode($response);
                    die();
                }

                return $this->redirect(['controller' => 'Orders', 'action' => 'view', $orderItem->order_id]);
            } else {
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
        $products = $this->OrderItems->Products->find('list', ['limit' => 200]);
        $products = $this->prepareArrayForView($products->toArray());
        $orders = $this->OrderItems->Orders->find('list', ['limit' => 200]);
        $this->set(compact('orderItem', 'products', 'orders', 'itemToAdd', 'listPics', 'listDiscounts', 'relatedProducts', 'loggedUserC'));
        $this->set('_serialize', ['orderItem']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order Item id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $orderItem = $this->OrderItems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($this->request->is('ajax')){
                $response = array();
                $orderItem = $this->OrderItems->patchEntity($orderItem, $this->request->data);
                if ($this->OrderItems->save($orderItem)){
                    $response['result'] = true;
                    $response['msg'] = __('The order item has been saved.');
                } else{
                    $response['result'] = false;
                    $response['msg'] = __('The order item could not be saved. Please, try again.');
                }
                echo json_encode($response);
                die();
            }
            $orderItem = $this->OrderItems->patchEntity($orderItem, $this->request->data);
            if ($this->OrderItems->save($orderItem)) {
                $this->Flash->success(__('The order item has been saved.'));

                return $this->redirect(['controller' => 'Orders', 'action' => 'view', $orderItem->order_id]);
            } else {
                $this->Flash->error(__('The order item could not be saved. Please, try again.'));
            }
        }
        $products = $this->OrderItems->Products->find('list', ['limit' => 200]);
        $products = $this->prepareArrayForView($products->toArray());
        $orders = $this->OrderItems->Orders->find('list', ['limit' => 200]);
        $this->set(compact('orderItem', 'products', 'orders'));
        $this->set('_serialize', ['orderItem']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orderItem = $this->OrderItems->get($id);
        if ($this->OrderItems->delete($orderItem)) {
            $this->Flash->success(__('The order item has been deleted.'));
        } else {
            $this->Flash->error(__('The order item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Orders', 'action' => 'view', $orderItem->order_id]);
    }
    /**
     * delete from order form
     */
    public function deleteFromOrderForm(){
        $this->autoRender = false;
        $response = array();
        $response['result'] = false;
        if($this->request->is('ajax')){
            $orderItem = $this->OrderItems->get($this->request->data['order_item_id']);
            if ($this->OrderItems->delete($orderItem)) {
                $response['msg'] = __('The order item has been deleted.');
                $response['result'] = true;
            } else {
                $response['msg'] = __('The order item could not be deleted. Please, try again.');
                $response['result'] = false;
            }
            echo json_encode($response);
        }
    }
    public function isAuthorized($user){
        if(in_array($this->request->action, ['view', 'edit', 'delete', 'add'])){
            return true;
        }
        return parent::isAuthorized($user);
    }
}
