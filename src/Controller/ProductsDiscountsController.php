<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
/**
 * ProductsDiscounts Controller
 *
 * @property \App\Model\Table\ProductsDiscountsTable $ProductsDiscounts
 */
class ProductsDiscountsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['guestView']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $this->paginate = [
            'contain' => ['Products']
        ];
        $productsDiscounts = $this->paginate($this->ProductsDiscounts);

        $this->set(compact('productsDiscounts'));
        $this->set('_serialize', ['productsDiscounts']);
    }
}
     public function guestView()
    {
        $user = null;
        if($this->Auth->user() != null){
            $user = TableRegistry::get('Users')->get($this->Auth->user()['id']);
        }
        $discList = $this->ProductsDiscounts->find('all')
        ->where(['status' => 1, 'discount_percentage IS NOT' => 0])->contain(['Products' => function($q){
            return $q->where(['Products.product_status' => 1]);
        }, 'Products.ProductsPics', 'Products.Brands', 'Products.Kardexes', 'Products.Categories', 'Products.ProductsDiscounts']);
        $discounts = $this->paginate($discList, [
            'limit' => 10
        ]);
        $allDisc = $discList->toArray();
        // pr($allDisc);

        $this->set(compact('allDisc', 'user'));
        $this->set('_serialize', ['allDisc']);
    }

    /**
     * View method
     *
     * @param string|null $id Products Discount id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
          }else{
            $this->viewBuilder()->layout('adminDash');
            $productsDiscount = $this->ProductsDiscounts->get($id, [
                'contain' => ['Products']
            ]);

            $this->set('productsDiscount', $productsDiscount);
            $this->set('_serialize', ['productsDiscount']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $productsDiscount = $this->ProductsDiscounts->newEntity();
        if ($this->request->is('post')) {
            $productsDiscount = $this->ProductsDiscounts->patchEntity($productsDiscount, $this->request->data);
            if ($this->ProductsDiscounts->save($productsDiscount)) {
                $this->Flash->success(__('The products discount has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The products discount could not be saved. Please, try again.'));
            }
        }
        $products = $this->ProductsDiscounts->Products->find('list', ['limit' => 200]);
        $this->set(compact('productsDiscount', 'products'));
        $this->set('_serialize', ['productsDiscount']);
    }
}

    /**
     * Edit method
     *
     * @param string|null $id Products Discount id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $productsDiscount = $this->ProductsDiscounts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsDiscount = $this->ProductsDiscounts->patchEntity($productsDiscount, $this->request->data);
            if ($this->ProductsDiscounts->save($productsDiscount)) {
                $this->Flash->success(__('The products discount has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The products discount could not be saved. Please, try again.'));
            }
        }
        $products = $this->ProductsDiscounts->Products->find('list', ['limit' => 200]);
        $this->set(compact('productsDiscount', 'products'));
        $this->set('_serialize', ['productsDiscount']);
    }
}

    /**
     * Delete method
     *
     * @param string|null $id Products Discount id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $this->request->allowMethod(['post', 'delete']);
        $productsDiscount = $this->ProductsDiscounts->get($id);
        if ($this->ProductsDiscounts->delete($productsDiscount)) {
            $this->Flash->success(__('The products discount has been deleted.'));
        } else {
            $this->Flash->error(__('The products discount could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
}
