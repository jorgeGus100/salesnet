<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BannerPics Controller
 *
 * @property \App\Model\Table\BannerPicsTable $BannerPics
 */
class BannerPicsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');
        $bannerList = $this->BannerPics->find('all')
        ->where(['status IN' => [0,1]]);
        $bannerPics = $this->paginate($bannerList);

        $this->set(compact('bannerPics'));
        $this->set('_serialize', ['bannerPics']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Banner Pic id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash'); 
        $bannerPic = $this->BannerPics->get($id, [
            'contain' => []
        ]);

        $this->set('bannerPic', $bannerPic);
        $this->set('_serialize', ['bannerPic']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash'); 
        $bannerPic = $this->BannerPics->newEntity();
        if ($this->request->is('post')) {
            $bannerPic = $this->BannerPics->patchEntity($bannerPic, $this->request->data);
            if (!empty($this->request->data['upload']['name'])) {

                    $file = $this->request->data['upload']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                    $setNewFileName = "upload\banner_img\\".time() . "_" . rand(000000, 999999);
                   if (in_array($ext, $arr_ext)) {
                    
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . $setNewFileName . '.' . $ext);
                    $bannerPic->pic_path = $setNewFileName . '.' . $ext;
                    
                    }else{
                        $this->Flash->error(__('The extension of your image must be png, jpg, jpeg or gif.'));
                    }
            }else{
                $bannerPic->pic_path = "upload/empty.png";
            }
            if ($this->BannerPics->save($bannerPic)) {
                $this->Flash->success(__('The banner pic has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The banner pic could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('bannerPic'));
        $this->set('_serialize', ['bannerPic']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Banner Pic id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash'); 
        $bannerPic = $this->BannerPics->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bannerPic = $this->BannerPics->patchEntity($bannerPic, $this->request->data);
            if ($this->BannerPics->save($bannerPic)) {
                $this->Flash->success(__('The banner pic has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The banner pic could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('bannerPic'));
        $this->set('_serialize', ['bannerPic']);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Banner Pic id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bannerPic = $this->BannerPics->get($id);
        if ($this->BannerPics->delete($bannerPic)) {
            $this->Flash->success(__('The banner pic has been deleted.'));
        } else {
            $this->Flash->error(__('The banner pic could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function trash($id = null){
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $bannerPic = $this->BannerPics->get($id, [
            'contain' => []
        ]);
        $bannerPic->status = 2;
        if ($this->BannerPics->save($bannerPic)) {
                $this->Flash->success(__('The banner pic has been eliminated.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The banner pic could not be eliminated. Please, try again.'));
            }
        }    
    }
}