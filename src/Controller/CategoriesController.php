<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 */
class CategoriesController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['productsCategory', 'guestView']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $this->paginate = [
            'contain' => ['Users']
        ];
        $categoriesList = $this->Categories->find('all')
        ->where(['category_status' => 1]);

        $categories = $this->paginate($categoriesList);

        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
        }
    }
    public function guestView()
    {
        $user = null;
        if($this->Auth->user() != null){
            $user = TableRegistry::get('Users')->get($this->Auth->user()['id']);
        }
        $categoriesList = $this->Categories->find('all')
        ->where(['category_status' => 1])->contain(['Products'=> ['Brands', 'Kardexes', 'ProductsPics', 'Categories', 'ProductsDiscounts']]);
        $this->loadModel('Products');
        $this->loadModel('ProductsPics');
        $this->loadModel('ProductsCategories');
        //$brands = $this->paginate($brandsList);

        $allCategories = $categoriesList->toArray();
        $allCategories2 = $categoriesList->toArray();
        // to show categories that have product only
        foreach ($allCategories2 as $keyC => $cat) {
            foreach ($cat->products as $keyP => $product) {
                if($product->product_status == 2){
                    unset($cat->products[$keyP]);
                }
            }
            if(empty($cat->products)) {
                unset($allCategories2[$keyC]);
            }
        }


        $this->set(compact('allCategories', 'user', 'allCategories2'));
        $this->set('_serialize', ['allCategories']);
    }
    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->layout('adminDash');
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $category = $this->Categories->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('category', $category);
        $this->set('_serialize', ['category']);
        }
    }   

    public function productsCategory($id = null)
    {
        $user = null;
        if($this->Auth->user() != null){
            $user = TableRegistry::get('Users')->get($this->Auth->user()['id']);
        }

        $category = $this->Categories->get($id, [
            'contain' => ['Users', 'Products'=> ['Brands', 'Kardexes', 'ProductsPics', 'Categories', 'ProductsDiscounts']]
        ]);

        $this->loadModel('Products');
        $this->loadModel('ProductsPics');
        $this->loadModel('ProductsCategories');
        $this->loadModel('ProductsDiscounts');
        $prodCat = $this->ProductsCategories->find('all', array('conditions'=>array('ProductsCategories.categories_id'=>$id)));
        $prodCatList = $prodCat->toArray();
        $productsList = array();
        $picList = array();
        $n = 0;
        foreach ($prodCatList as $prod) {
            $productsList[$n]['product']= $this->Products->get($prod->products_id);
            $pic= $this->ProductsPics->find()->where(['products_id'=>$prod->products_id]);
            $discount = $this->ProductsDiscounts->find()->where(['product_id'=>$prod->products_id]);
            $productsList[$n]['product']['pic']= $pic->toArray();
            $productsList[$n]['product']['disc']= $discount->toArray();
            $n++;
        }
        

        //$products = $this->Products->find('all', array('conditions'=>array($prodCat[products_id]=>$id)));
        $this->set('category', $category);
        $this->set('productsList', $productsList);
        $this->set('picList', $picList);
        $this->set('user', $user);
        $this->set('_serialize', ['category']);
        $this->set('_serialize', ['productsList']);
        $this->set('_serialize', ['picList']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');         
        $category = $this->Categories->newEntity();

        if ($this->request->is('post')) {
            $category->users_id = $this->Auth->user('id');
            $category = $this->Categories->patchEntity($category, $this->request->data);
            if (!empty($this->request->data['upload']['name'])) {

                    $file = $this->request->data['upload']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                    $setNewFileName = "upload\categories_img\\".time() . "_" . rand(000000, 999999);
                   if (in_array($ext, $arr_ext)) {
                    
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . $setNewFileName . '.' . $ext);
                    $category->category_pic = $setNewFileName . '.' . $ext;
                    
                    }else{
                        $this->Flash->error(__('The extension of your image must be png, jpg, jpeg or gif.'));
                    }
            }else{
                $category->category_pic = "upload/empty.png";
            }

            if ($this->Categories->save($category)) {
                //$this->Menu->getMenuItems();
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'index']);
                 
            } else {
                $this->Flash->error(__('The category could not be saved. Please, try again.'));
            }
        }
        $users = $this->Categories->Users->find('list', ['limit' => 200]);
        $this->set(compact('category', 'users'));
        $this->set('_serialize', ['category']);
    }
}

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->layout('adminDash');
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $category = $this->Categories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->data);
            if (!empty($this->request->data['upload']['name'])) {

                    $file = $this->request->data['upload']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                    $setNewFileName = "upload\categories_img\\".time() . "_" . rand(000000, 999999);
                   if (in_array($ext, $arr_ext)) {
                    
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . $setNewFileName . '.' . $ext);
                    $category->category_pic = $setNewFileName . '.' . $ext;
                    
                    }else{
                        $this->Flash->error(__('The extension of your image must be png, jpg, jpeg or gif.'));
                    }
            }
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The category could not be saved. Please, try again.'));
            }
        }
        $users = $this->Categories->Users->find('list', ['limit' => 200]);
        $this->set(compact('category', 'users'));
        $this->set('_serialize', ['category']);
    }
}

    /**
     * Delete method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function trash($id = null){
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $category = $this->Categories->get($id, [
            'contain' => []
        ]);
        $category->category_status = 2;
        if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been eliminated.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The category could not be eliminated. Please, try again.'));
            }

        }
    }

    public function delete($id = null)
    {
        if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->request->allowMethod(['post', 'delete']);
        $category = $this->Categories->get($id);
        
        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('The category has been deleted.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
}
