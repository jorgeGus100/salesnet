<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Kardexes Controller
 *
 * @property \App\Model\Table\KardexesTable $Kardexes
 */
class KardexesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products']
        ];
        $kardexes = $this->paginate($this->Kardexes);

        $this->set(compact('kardexes'));
        $this->set('_serialize', ['kardexes']);
    }

    /**
     * View method
     *
     * @param string|null $id Kardex id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $kardex = $this->Kardexes->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('kardex', $kardex);
        $this->set('_serialize', ['kardex']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $kardex = $this->Kardexes->newEntity();
        if ($this->request->is('post')) {
            $kardex = $this->Kardexes->patchEntity($kardex, $this->request->data);
            if ($this->Kardexes->save($kardex)) {
                $this->Flash->success(__('The kardex has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The kardex could not be saved. Please, try again.'));
            }
        }
        $products = $this->Kardexes->Products->find('list', ['limit' => 200]);
        $this->set(compact('kardex', 'products'));
        $this->set('_serialize', ['kardex']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Kardex id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $kardex = $this->Kardexes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $kardex = $this->Kardexes->patchEntity($kardex, $this->request->data);
            if ($this->Kardexes->save($kardex)) {
                $this->Flash->success(__('The kardex has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The kardex could not be saved. Please, try again.'));
            }
        }
        $products = $this->Kardexes->Products->find('list', ['limit' => 200]);
        $this->set(compact('kardex', 'products'));
        $this->set('_serialize', ['kardex']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Kardex id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $kardex = $this->Kardexes->get($id);
        if ($this->Kardexes->delete($kardex)) {
            $this->Flash->success(__('The kardex has been deleted.'));
        } else {
            $this->Flash->error(__('The kardex could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
