<?php
	namespace App\Controller;

	use App\Controller\AppController;
	use App\Form\RatingForm;
	use Cake\Routing\Router;
	use Cake\ORM\TableRegistry;

	class RatingsController extends AppController {
		public function initialize() {
			parent::initialize();
			$this->Auth->allow(['rating']);
		}
		/**
		 * ajax is used for this feature
		 */
		public function rating(){
			if ($this->request->is('ajax')) {
				$this->viewBuilder()->layout('ajax');
				$rating = new RatingForm();
				$result = $rating->execute($this->request->data);
				echo json_encode($result);
				die();
			}
		}
	}