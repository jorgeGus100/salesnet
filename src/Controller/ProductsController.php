<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use App\Form\AddCartForm;
/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController
{

     public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('guestView');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $this->paginate = [
            'contain' => ['Brands']
        ];
        $productsList = $this->Products->find('all')
        ->where(['product_status' => 1])->contain(['Kardexes', 'Categories']);
        // pr($productsList->toArray());
        $products = $this->paginate($productsList);


        $this->set(compact('products'));
        $this->set('_serialize', ['products']);
    }
}

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $product = $this->Products->get($id, [
            'contain' => ['Brands', 'ProductsDiscounts']
        ]);
        $this->loadModel('Categories');
        $this->loadModel('Kardexes');
        $this->loadModel('ProductsPics');
        $this->loadModel('ProductsDiscounts');
        $this->loadModel('ProductsCategories');
        $intKardex=$this->Kardexes->find('all', 
                       array('conditions'=>array('Kardexes.products_id'=>$id)));
        $listKardex = $intKardex->toArray(); 
        //print_r($listKardex[0]);
        //die();
        $intPic = $this->ProductsPics->find('all', 
                       array('conditions'=>array('ProductsPics.products_id'=>$id)));
        $listPics = $intPic->toArray(); 
        $intDisc = $this->ProductsDiscounts->find('all', 
                       array('conditions'=>array('ProductsDiscounts.product_id'=>$id)));
        $listDiscounts = $intDisc->toArray(); 
        $intCateg = $this->ProductsCategories->find('all', 
                       array('conditions'=>array('ProductsCategories.products_id'=>$id)));
        $listCategories = $intCateg->toArray();
        if(!empty($listCategories))
            $categories = $this->Categories->get($listCategories[0]['categories_id']);

        $loggedUserId = $this->request->session()->read('Auth.User')['id'];
        if(isset($loggedUserId)){
            $loggedUser = TableRegistry::get('Users')->get($loggedUserId, ['contain' => ['Orders']]);
            $userCart = $loggedUser->getCart();
            if($this->Auth->user('role_id') != 1)
                $inCart = $userCart->isProductInCart($product->id);
        }
        $this->set(compact('product', 'brands', 'categories', 'listKardex', 'listPics', 'listDisc', 'listCategories', 'inCart'));
        $this->set('_serialize', ['product']);
    }
}
    public function guestView($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Brands', 'ProductsDiscounts', 'Kardexes', 'RelatedProducts', 'Categories']
        ]);
        $this->loadModel('Categories');
        $this->loadModel('Kardexes');
        $this->loadModel('ProductsPics');
        $this->loadModel('ProductsDiscounts');
        $this->loadModel('ProductsCategories');
        $intKardex=$this->Kardexes->find('all', 
                       array('conditions'=>array('Kardexes.products_id'=>$id)));
        $listKardex = $intKardex->toArray(); 
        //print_r($listKardex[0]);
        //die();
        $intPic = $this->ProductsPics->find('all', 
                       array('conditions'=>array('ProductsPics.products_id'=>$id)));
        $listPics = $intPic->toArray(); 
        $intDisc = $this->ProductsDiscounts->find('all', 
                       array('conditions'=>array('ProductsDiscounts.product_id'=>$id)));
        $listDiscounts = $intDisc->toArray();
        
         
        $intCateg = $this->ProductsCategories->find('all', 
                       array('conditions'=>array('ProductsCategories.products_id'=>$id)));
        $listCategories = $intCateg->toArray();
        if(!empty($listCategories))
        $categories = $this->Categories->get($listCategories[0]['categories_id']);
        $loggedUserId = $this->request->session()->read('Auth.User')['id'];
        if(isset($loggedUserId)){
            $loggedUser = TableRegistry::get('Users')->get($loggedUserId, ['contain' => ['Orders']]);
            $userCart = $loggedUser->getCart();
            if($userCart != false){
                $inCart = $userCart->isProductInCart($product->id);
                $cartQuantity = $userCart->inCartQuantity($product->id);
            }
            $loggedUserC = TableRegistry::get('Users')->get($loggedUserId);
        }
        $relatedProducts = $product->getRelatedProducts();
        $inCart = null;// to letting add product to cart

       $this->set(compact('product', 'brands', 'categories', 'listKardex', 'listPics', 'listDiscounts', 'listCategories', 'inCart', 'loggedUserId', 'loggedUserC', 'cartQuantity', 'relatedProducts'));
        $this->set('_serialize', ['product']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $product = $this->Products->newEntity();
        $this->loadModel('Categories');
        $this->loadModel('Kardexes');
        $this->loadModel('ProductsPics');
        $this->loadModel('ProductsDiscounts');
        $this->loadModel('ProductsCategories');
        $intKardex = $this->Kardexes->newEntity();
        $intPic = $this->ProductsPics->newEntity();
        $intDisc = $this->ProductsDiscounts->newEntity();
        $intCateg = $this->ProductsCategories->newEntity();

        if ($this->request->is('post')) {
            //print_r($this->request->data);
            //die();
            $product = $this->Products->patchEntity($product, $this->request->data);
            if ($this->Products->save($product)) {
                $intKardex->quantity = $this->request->data['intKardex'];
                $intKardex->products_id = $product['id'];
                $intKardex->unity = 'Units';
                $this->Kardexes->save($intKardex);
                $intDisc->discount_name ="discount";
                //pr($this->request->data['intDisc']);
                if(($this->request->data['intDisc'] != "")){
                    $intDisc->discount_percentage = $this->request->data['intDisc'];
                } else{
                    $intDisc->discount_percentage = 0;
                }
                $intDisc->product_id = $product['id'];
                $intDisc->init_date = date('Y-m-d');
                $intDisc->end_date = date('Y-m-d');
                $this->ProductsDiscounts->save($intDisc);
                $intCateg->products_id = $product['id'];
                $intCateg->categories_id = $this->request->data['categories_id'];
                $this->ProductsCategories->save($intCateg);

                if (!empty($this->request->data['upload']['name'])) {

                    $file = $this->request->data['upload']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                   if (in_array($ext, $arr_ext)) {
                    
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . 'upload\products_img\\' . $setNewFileName . '.' . $ext);
                    $imageFileName = $setNewFileName . '.' . $ext;
                    $intPic->pic_path = $imageFileName;
                    $intPic->products_id = $product['id'];
                    $intPic->description = $product['product_name'];
                    $this->ProductsPics->save($intPic);
                    }else{
                        $this->Flash->error(__('The extension of your image must be png, jpg, jpeg or gif.'));
                    }
                }
                //echo $product['id'];
               // die($intKardex);

                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
        $brands = $this->Products->Brands->find('list',array('fields' => array('id','brand_name')));
        $categories=$this->Categories->find('list',array('fields' => array('id','category_name')));
        $this->set(compact('product', 'brands', 'categories', 'intKardex', 'intPic', 'intDisc'));
        $this->set('_serialize', ['product']);
    }
}


    //$vehicles = $this->Lift->Vehicle->find('list', array('conditions' => array('Vehicle.user_id' => $this->Auth->user('id')), 'recursive' => 0, 'fields' => array('Vehicle.id', 'Vehicle.model', 'Manufacturer.title')));

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $product = $this->Products->get($id, [
            'contain' => []
        ]);
        $this->loadModel('Categories');
        $this->loadModel('Kardexes');
        $this->loadModel('ProductsPics');
        $this->loadModel('ProductsDiscounts');
        $this->loadModel('ProductsCategories');
        $intKardex=$this->Kardexes->find('all', 
                       array('conditions'=>array('Kardexes.products_id'=>$id)));
        $listKardex = $intKardex->toArray(); 
        $intPic = $this->ProductsPics->find('all', 
                       array('conditions'=>array('ProductsPics.products_id'=>$id)));
        $listPics = $intPic->toArray(); 
        $intDisc = $this->ProductsDiscounts->find('all', 
                       array('conditions'=>array('ProductsDiscounts.product_id'=>$id)));
        $listDiscounts = $intDisc->toArray(); 
        $intCateg = $this->ProductsCategories->find('all', 
                       array('conditions'=>array('ProductsCategories.products_id'=>$id)));
        $listCategories = $intCateg->toArray(); 
       
       if ($this->request->is(['patch', 'post', 'put'])) {
            
            $product = $this->Products->patchEntity($product, $this->request->data);
            if ($this->Products->save($product)) {
                if(isset($listKardex[0]['id'])){
                    $kardex = $this->Kardexes->get($listKardex[0]['id']);
                    $kardex->quantity = $this->request->data['quantity'];
                    $this->Kardexes->save($kardex);
                } else{
                    // saving new kardex
                    $kardex = TableRegistry::get('Kardexes')->newEntity();
                    $kardex->products_id = $id;
                    $kardex->quantity = $this->request->data['quantity'];
                    $this->Kardexes->save($kardex);
                }
                if(!empty(($listDiscounts))){
                    $disc = $this->ProductsDiscounts->get($listDiscounts[0]['id']);
                    $disc->discount_percentage = $this->request->data['discount'];
                    $this->ProductsDiscounts->save($disc);
                } else if(isset($this->request->data['discount'])){
                    $disc = $this->ProductsDiscounts->newEntity();
                    $disc->discount_name ="discount";
                    $disc->discount_percentage = $this->request->data['discount'];
                    $disc->product_id = $product->id;
                    $disc->init_date = date('Y-m-d');
                    $disc->end_date = date('Y-m-d');
                    $this->ProductsDiscounts->save($disc);
                }
                else{
                    $disc = null;
                }
                if((empty($listCategories)) && (isset($this->request->data['categories_id']))){
                    $cat = $this->ProductsCategories->newEntity();
                    $cat->products_id = $product->id;
                    $cat->categories_id = $this->request->data['categories_id'];
                    $this->ProductsCategories->save($cat);
                } else{
                    $cat = $this->ProductsCategories->get($listCategories[0]['id']);
                    if(isset($this->request->data['categories_id'])){
                        //pr($this->request->data['categories_id']);
                        $cat->categories_id = $this->request->data['categories_id'];
                        $this->ProductsCategories->save($cat);
                    }
                }
                
                if (!empty($this->request->data['upload']['name'])) {
                    
                    $file = $this->request->data['upload']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                   if (in_array($ext, $arr_ext)) {
                    
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . 'upload\products_img\\' . $setNewFileName . '.' . $ext);
                    $imageFileName = $setNewFileName . '.' . $ext;
                    $pic = $this->ProductsPics->get($listPics[0]['id']);
                    $pic->pic_path = $imageFileName;
                    $this->ProductsPics->save($pic);
                    }else{
                        $this->Flash->error(__('The extension of your image must be png, jpg, jpeg or gif.'));
                    }
                }
                //echo $product['id'];
               // die($intKardex);

                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
        $brands = $this->Products->Brands->find('list',array('fields' => array('id','brand_name')));
        $categories=$this->Categories->find('list',array('fields' => array('id','category_name')));

        $this->set(compact('product', 'brands','categories','listKardex','listDiscounts','listPics','listCategories'));
        $this->set('_serialize', ['product']);
    }
}

    public function trash($id = null){
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
        $this->viewBuilder()->layout('adminDash');        
        $product = $this->Products->get($id, [
            'contain' => []
        ]);
        $product->product_status = 2;
        if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been eliminated.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product could not be eliminated. Please, try again.'));
            }

    }
}
    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
         if($this->Auth->user('role_id')!=1){
                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }else{
            $this->viewBuilder()->layout('adminDash');        
            $this->request->allowMethod(['post', 'delete']);
            $product = $this->Products->get($id);
            if ($this->Products->delete($product)) {
                $this->Flash->success(__('The product has been deleted.'));
            } else {
                $this->Flash->error(__('The product could not be deleted. Please, try again.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }
}
