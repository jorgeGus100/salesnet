<?php
	namespace App\Controller;

	use App\Controller\AppController;
	use App\Form\SearchForm;
	use Cake\Routing\Router;
	use Cake\ORM\TableRegistry;

	class SearchesController extends AppController
	{
		private $searched;
		public $paginate = [
        'limit' => 5,
        /*'order' => [
            'Articles.title' => 'asc'
        ]*/
    ];
		public function initialize() {
			parent::initialize();
			$this->Auth->allow(['search']);
		}
		public function search(){
			if ($this->request->is('post')){
				if ($this->request->is('ajax')){
					$response = $this->ajaxSearch($this->request->data);
					echo json_encode($response);
					die();
				}
			}
			$subject = $this->request->data;
			// do a normal request
			if(isset($this->request->data['search'])){
				$this->Cookie->write('searched', $this->request->data['search']);
				$this->searched = $this->request->data['search'];
			} else{
				$this->searched = $this->Cookie->read('searched');
			}
			$subject = $this->Cookie->read('searched');
			$result = TableRegistry::get('Products')->find()
				->where(['Products.product_name LIKE' => $this->searched.'%', 'Products.product_status' => 1])
				->contain(['ProductsPics', 'Kardexes', 'ProductsDiscounts'])
			;
			$result = $this->paginate($result);
			$this->set(compact('result', 'subject'));
		}

		private function ajaxSearch($query){
			$search = new SearchForm();
			$response = array();
			$result = $search->execute($query);
			$formatting = '<ul>';
			foreach ($result as $item) {
			    $formatting .= '<li><a href="'.
			        Router::url([
			            'controller' => 'Products',
			            'action' => 'guestView',
			            $item->id
			        ])
			    .'">'.$item->product_name.'</a></li>';
			}
			$formatting .= '</ul>';
			$response['result'] = $result;
			$response['formatting'] = $formatting;
			return $response;
		}
	}