<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Order;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->deny();
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $orders = $this->paginate($this->Orders);

        $this->set(compact('orders'));
        $this->set('_serialize', ['orders']);
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $orderQuery = $this->Orders->find()
            ->contain(['Users', 'OrderItems'])
            ->where([
                'Orders.id' => $id,
                // getting the unconfirmed ones
                'Orders.status' => 0
            ])
        ;
        if($orderQuery->count()){
            $order = $orderQuery->first();
        }
        if($orderQuery->count()):

        $productIds = array();
        foreach ($order->order_items as $item) {
            $productIds[] = $item->product_id;
        }
        $productsTable = TableRegistry::get('Products');
        $productsSet = $productsTable->find('all')
                        ->where(['id IN' => $productIds])
                        ->contain(['ProductsPics', 'ProductsDiscounts', 'OrderItems', 'Kardexes'])
                        ;
        $filteredProducts = array();
        foreach($order->order_items as $orderItems){
            foreach ($productsSet as $product) {
                if($product->id == $orderItems->product_id){
                    array_push($filteredProducts, $product);
                }
            }
        }
        /**
         * logic for calculating the total discounts
         * currently it is done per each product item
         */
        $discounts = array();
        foreach ($filteredProducts as $p) {
            // if a discount is available
            $productsDiscount = array();
            $productsDiscount['product_id'] = $p->id;

            if(!empty($p->products_discounts)){
                $unitDiscount = (($p->products_discounts[0]->discount_percentage * $p->product_price) / 100);
                $productQ = 0;
                foreach ($order->order_items as $item) {
                    if($item->product_id == $p->id){
                        $productQ = $item->quantity;
                    }
                }
                $productsDiscount['discount'] = $unitDiscount * $productQ;
            } else{
                // the product has no discounts
                $productsDiscount['discount'] = 0;
            }
            array_push($discounts, $productsDiscount);
        }
        $totalDiscounts = 0;
        foreach ($discounts as $disc) {
            $totalDiscounts += $disc['discount'];
        }
        $orderTotal = $order->total - $totalDiscounts;

        else:
            $order = null;
        endif;// order count

        $this->set('order', $order);
        $this->set(compact('productsSet', 'filteredProducts', 'totalDiscounts', 'orderTotal'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEntity();
        $orderEntity = new Order();
        $statusValues = $orderEntity->getStatus();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->data);
            $order->users_id = $this->Auth->user('id');

            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        }
        $users = $this->Orders->Users->find('list', ['limit' => 200]);
        $this->set(compact('order', 'users', 'statusValues'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        }
        $users = $this->Orders->Users->find('list', ['limit' => 200]);
        $this->set(compact('order', 'users'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id, ['contain' => ['OrderItems']]);
        // emptying the cart
        if ($this->Orders->OrderItems->deleteAll(['OrderItems.order_id' => $id])) {
            $this->Flash->success(__('The order has been emptied.'));
            $count = $this->Orders->OrderItems->find('all', ['conditions' => ['OrderItems.order_id' => $id]]);
            if($count->count() == 0){
                $order->total = 0;
                $this->Orders->save($order);
            }
        } else {
            $this->Flash->error(__('The order could not be emptied. Please, try again.'));
        }

        return $this->redirect(['action' => 'view', $id]);
    }
    /**
     * order confirmation
     */
    public function orderForm($orderId){
        $user = null;
        if($this->Auth->user() != null){
            $user = TableRegistry::get('Users')->get($this->Auth->user()['id']);
        }
        if($this->request->is(['post', 'put'])){
            $order = $this->Orders->get($orderId, ['contain' => ['OrderItems' => ['Products' => ['ProductsPics', 'Kardexes', 'ProductsDiscounts']], 'Users']]);
            // pr($order->order_items);
            $filteredProducts = array();
            foreach($order->order_items as $orderItem){
                array_push($filteredProducts, $orderItem->product);
            }
            $discounts = $this->calculateDiscount($filteredProducts, $order);
            // pr($discounts);
            $orderTotal = $discounts['orderTotal'];
            $totalDiscounts = $discounts['totalDiscounts'];
            $orderItems = $order->order_items;
            $this->set(compact('order', 'orderItems', 'orderTotal', 'totalDiscounts'));
        } else{
            $this->redirect(['action' => 'view', $orderId]);
        }
        $this->set(compact('user'));
    }
    /**
     * logic for calculating the total discounts
     * currently it is done per each product item
     */
    private function calculateDiscount($products, $order){
        $discounts = array();
        foreach ($products as $p) {
            // if a discount is available
            $productsDiscount = array();
            $productsDiscount['product_id'] = $p->id;

            if(!empty($p->products_discounts)){
                $unitDiscount = (($p->products_discounts[0]->discount_percentage * $p->product_price) / 100);
                $productQ = 0;
                foreach ($order->order_items as $item) {
                    if($item->product_id == $p->id){
                        $productQ = $item->quantity;
                    }
                }
                $productsDiscount['discount'] = $unitDiscount * $productQ;
            } else{
                // the product has no discounts
                $productsDiscount['discount'] = 0;
            }
            array_push($discounts, $productsDiscount);
        }
        $totalDiscounts = 0;
        foreach ($discounts as $disc) {
            $totalDiscounts += $disc['discount'];
        }
        $orderTotal = $order->total - $totalDiscounts;
        $result = array();
        $result['orderTotal'] = $orderTotal;
        $result['totalDiscounts'] = $totalDiscounts;
        return $result;
    }

    public function confirmOrder($id = null){
        // to do a real kardexes updating
        $order = $this->Orders->get($id, ['contain' => ['OrderItems' => ['Products' => ['ProductsPics', 'Kardexes', 'ProductsDiscounts']], 'Users']]);
        $msg = 'Your order containing '. count($order->order_items) .' products has been confirmed';
        if($this->request->is(['post', 'put'])){
            if($order->status == 1){
                $msg = __('Your order is already confirmed.');
            } else{
                $email = new Email();
                $email->transport('mailjet');
                $toSend = $order->user->email;
                try{
                    $res = $email->from(['christian.xperius@gmail.com' => 'SalesNet site'])
                    ->to($toSend)
                    ->subject('Order Confirmation - SalesNet')
                    ->send($msg);
                }catch (Exception $e) {
                    echo 'Exception : ',  $e->getMessage(), "\n";
                }
                $order->status = 1;// set order status to sold
                $this->Orders->save($order);
                $msg = __('An email has been sended to your account, please check your inbox.');
            }
            $this->set(compact('msg'));
        } else{
            $this->redirect(['action' => 'view', $id]);
        }
    }

    public function isAuthorized($user){
        if(in_array($this->request->action, ['view', 'edit', 'delete'])){
            $orderId = (int)$this->request->params['pass'][0];
            if($this->Orders->isOwnedBy($orderId, $user['id'])){
                return true;
            }
        }
        if(in_array($this->request->action, ['orderForm', 'confirmOrder'])){
            return true;
        }
        return parent::isAuthorized($user);
    }
}
