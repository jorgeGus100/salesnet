<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProformasItem Controller
 *
 * @property \App\Model\Table\ProformasItemTable $ProformasItem
 */
class ProformasItemController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Proformas', 'Products', 'Discounts']
        ];
        $proformasItem = $this->paginate($this->ProformasItem);

        $this->set(compact('proformasItem'));
        $this->set('_serialize', ['proformasItem']);
    }

    /**
     * View method
     *
     * @param string|null $id Proformas Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $proformasItem = $this->ProformasItem->get($id, [
            'contain' => ['Proformas', 'Products', 'Discounts']
        ]);

        $this->set('proformasItem', $proformasItem);
        $this->set('_serialize', ['proformasItem']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $proformasItem = $this->ProformasItem->newEntity();
        if ($this->request->is('post')) {
            $proformasItem = $this->ProformasItem->patchEntity($proformasItem, $this->request->data);
            if ($this->ProformasItem->save($proformasItem)) {
                $this->Flash->success(__('The proformas item has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The proformas item could not be saved. Please, try again.'));
            }
        }
        $proformas = $this->ProformasItem->Proformas->find('list', ['limit' => 200]);
        $products = $this->ProformasItem->Products->find('list', ['limit' => 200]);
        $discounts = $this->ProformasItem->Discounts->find('list', ['limit' => 200]);
        $this->set(compact('proformasItem', 'proformas', 'products', 'discounts'));
        $this->set('_serialize', ['proformasItem']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Proformas Item id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $proformasItem = $this->ProformasItem->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $proformasItem = $this->ProformasItem->patchEntity($proformasItem, $this->request->data);
            if ($this->ProformasItem->save($proformasItem)) {
                $this->Flash->success(__('The proformas item has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The proformas item could not be saved. Please, try again.'));
            }
        }
        $proformas = $this->ProformasItem->Proformas->find('list', ['limit' => 200]);
        $products = $this->ProformasItem->Products->find('list', ['limit' => 200]);
        $discounts = $this->ProformasItem->Discounts->find('list', ['limit' => 200]);
        $this->set(compact('proformasItem', 'proformas', 'products', 'discounts'));
        $this->set('_serialize', ['proformasItem']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Proformas Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $proformasItem = $this->ProformasItem->get($id);
        if ($this->ProformasItem->delete($proformasItem)) {
            $this->Flash->success(__('The proformas item has been deleted.'));
        } else {
            $this->Flash->error(__('The proformas item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
