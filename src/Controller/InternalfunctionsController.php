<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Internalfunctions Controller
 *
 * @property \App\Model\Table\InternalfunctionsTable $Internalfunctions
 */
class InternalfunctionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $internalfunctions = $this->paginate($this->Internalfunctions);

        $this->set(compact('internalfunctions'));
        $this->set('_serialize', ['internalfunctions']);
    }

    /**
     * View method
     *
     * @param string|null $id Internalfunction id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $internalfunction = $this->Internalfunctions->get($id, [
            'contain' => ['Roles']
        ]);

        $this->set('internalfunction', $internalfunction);
        $this->set('_serialize', ['internalfunction']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $internalfunction = $this->Internalfunctions->newEntity();
        if ($this->request->is('post')) {
            $internalfunction = $this->Internalfunctions->patchEntity($internalfunction, $this->request->data);
            if ($this->Internalfunctions->save($internalfunction)) {
                $this->Flash->success(__('The internalfunction has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The internalfunction could not be saved. Please, try again.'));
            }
        }
        $roles = $this->Internalfunctions->Roles->find('list', ['limit' => 200]);
        $this->set(compact('internalfunction', 'roles'));
        $this->set('_serialize', ['internalfunction']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Internalfunction id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $internalfunction = $this->Internalfunctions->get($id, [
            'contain' => ['Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $internalfunction = $this->Internalfunctions->patchEntity($internalfunction, $this->request->data);
            if ($this->Internalfunctions->save($internalfunction)) {
                $this->Flash->success(__('The internalfunction has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The internalfunction could not be saved. Please, try again.'));
            }
        }
        $roles = $this->Internalfunctions->Roles->find('list', ['limit' => 200]);
        $this->set(compact('internalfunction', 'roles'));
        $this->set('_serialize', ['internalfunction']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Internalfunction id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $internalfunction = $this->Internalfunctions->get($id);
        if ($this->Internalfunctions->delete($internalfunction)) {
            $this->Flash->success(__('The internalfunction has been deleted.'));
        } else {
            $this->Flash->error(__('The internalfunction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
