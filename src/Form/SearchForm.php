<?php
    namespace App\Form;

    use Cake\Form\Form;
    use Cake\Form\Schema;
    use Cake\Validation\Validator;
    use Cake\ORM\TableRegistry;

    class SearchForm extends Form
    {

        protected function _buildSchema(Schema $schema)
        {
            return $schema->addField('search', 'string');
        }

        protected function _buildValidator(Validator $validator)
        {
            return $validator->add('search', 'length', [
                    'rule' => ['minLength', 3],
                    'message' => 'A term with at least 3 characters is required'
                ]);
        }

        protected function _execute(array $data)
        {
            $result = TableRegistry::get('Products')->find()
                ->where(['Products.product_name LIKE' => $data['searchword'].'%', 'Products.product_status' => 1])
                // ->contain(['ProductsDiscounts'])
            ;
            return $result;
        }
    }