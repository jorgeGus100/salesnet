<?php
    namespace App\Form;

    use Cake\Form\Form;
    use Cake\Form\Schema;
    use Cake\Validation\Validator;
    use Cake\ORM\TableRegistry;

    class RatingForm extends Form{
    	protected function _buildSchema(Schema $schema) {
    		return $schema->addField('rate', 'integer')
    								->hidden('product_id')
    								->hidden('user_id')
    		;
    	}
    	protected function _execute(array $data) {
    		$user = TableRegistry::get('Users')->get($data['user_id']);
    		$isRated = $user->isProductRated($data['product_id']);
    		if($isRated->count()){
    			$productRating = TableRegistry::get('UsersProductsRatings')->get($isRated->first()->id);
    		} else{
	    		$productRating = TableRegistry::get('UsersProductsRatings')->newEntity();
    		}
    		$productRating->user_id = $data['user_id'];
    		$productRating->product_id = $data['product_id'];
    		$productRating->rate_value = $data['rate'];
    		if(TableRegistry::get('UsersProductsRatings')->save($productRating)){
    			return true;
    		} else{
    			return false;
    		}
    	}
    }