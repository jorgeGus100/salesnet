<?php
    namespace App\Form;

    use Cake\Form\Form;
    use Cake\Form\Schema;
    use Cake\Validation\Validator;
    use Cake\ORM\TableRegistry;

    class ContactForm extends Form{
    	protected function _buildSchema(Schema $schema) {
    		return $schema->addField('name', 'string')
    								->addField('email', 'email')
    								->addField('message', 'text')
    		;
    	}

    	protected function _execute(array $data) {
    		$message = TableRegistry::get('ContactMessages')->newEntity();
    		$message->name = $data['name'];
    		$message->email = $data['email'];
    		$message->message = $data['message'];
    		if(TableRegistry::get('ContactMessages')->save($message)){
    			return true;
    		} else{
    			return false;
    		}
    	}
    }