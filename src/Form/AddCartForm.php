<?php
    namespace App\Form;

    use Cake\Form\Form;
    use Cake\Form\Schema;
    use Cake\Validation\Validator;
    use Cake\ORM\TableRegistry;

    class AddCartForm extends Form {
    	protected function _buildSchema(Schema $schema) {
    		return $schema->addField('quantity', 'integer')
	    								->hidden('product_id')
	    								->hidden('order_id')
    		;
    	}

    	protected function _execute(array $data) {
    		return 'uy calling';
    	}
    }