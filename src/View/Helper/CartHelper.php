<?php
	/**
	 * helper to get data regarding to shopping cart state
	 */
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;

class CartHelper extends Helper{
	/**
	 * to get cart items
	 */
	public function getCartItems($userId = null) {
		$orderQ = TableRegistry::get('Orders')->find()
				->where([
						'Orders.users_id' => $userId,
						// getting the unconfirmed ones
            'Orders.status' => 0
					])
				;
		if($orderQ->count()){
			$order = $orderQ->first();
		} else{
			$order = null;
		}
		$response = array();
		if($order){
			$items = TableRegistry::get('OrderItems')
				->find('all', [
						'conditions' => ['order_id' => $order->id],
					])
				;
			$response['count'] = 0;
			$response['cart_id'] = $order->id;
			$response['total_order'] = $order->getTotal();
			foreach ($items->toArray() as $item) {
				$response['count'] += $item->quantity;
			}
		} else{
			$response['count'] = 0;
			$response['total_order'] = 0;
		}
		return $response;
	}

	public function getCartId($userId = null){
		$orderQ = TableRegistry::get('Orders')->find()
				->where([
						'Orders.users_id' => $userId,
						// getting the unconfirmed ones
            'Orders.status' => 0
					])
				;
		if($orderQ->count()){
			$order = $orderQ->first();
		} else{
			$order = null;
		}
		if($order){
			return $order->id;
		} else{
			return false;
		}
	}
	public function getCartSummary($cartId = null){
		$items = TableRegistry::get('OrderItems')->find()
							->where([
									'OrderItems.order_id' => $cartId
								])
							->contain(['Products', 'Products.ProductsPics', 'Products.ProductsDiscounts'])
						;
		return $items->toArray();
	}
}