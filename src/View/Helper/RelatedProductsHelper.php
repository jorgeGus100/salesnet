<?php
	/**
	 * helper to get data regarding to shopping cart state
	 */
	namespace App\View\Helper;

	use Cake\View\Helper;
	use Cake\View\View;
	use Cake\ORM\TableRegistry;

	class RelatedProductsHelper extends Helper{
		/**
		 * to getting category name by its id
		 */
		public function getCatNameById($id = null){
			// return $id;
			$cat = TableRegistry::get('Categories')->get($id);
			return $cat;
		}
	}