<?php
	/**
	 * helper to get data regarding to shopping cart state
	 */
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;

class MenuHelper extends Helper{
	/**
	 * to get cart items
	 */
	public function getMenuItems() {
		$session = $this->request->session();
        $this->loadModel('Categories');
        $intCategories=$this->Categories->find('all')->where(['category_status' => 1]);
        $listCategories = $intCategories->toArray();
        $session->write('listCategories', $listCategories);

        $this->loadModel('Brands');
        $intBrands=$this->Brands->find('all')->where(['brand_status' => 1]);
        $listBrands = $intBrands->toArray();
        $session->write('listBrands', $listBrands);

        $this->loadModel('ProductsDiscounts');
        $intDiscounts=$this->ProductsDiscounts->find('all')->where(['status' => 1]);
        $listDiscounts = $intDiscounts->toArray();
        $session->write('listDiscounts', $listDiscounts);
	}
}