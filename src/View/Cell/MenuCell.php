<?php
	namespace App\View\Cell;

	use Cake\View\Cell;

	class MenuCell extends Cell
	{
		/**
		 * get categories for main menu
		 */
    public function renderCategoriesMenu()
    {
    	$categories = $this->loadModel('Categories')->find('all')->where(['category_status' => 1])->contain(['Products' => function ($q){
            return $q->where(['Products.product_status' => 1]);
        }]);
        // to show categories that have product only
        $categories = $categories->toArray();
        foreach ($categories as $key => $cat) {
            if(empty($cat->products)) {
                unset($categories[$key]);
            }
        }

    	$this->set(compact('categories'));
    }
    
    /**
     * get brands for main menu
     * @return [type] [description]
     */
    public function renderBrandsMenu(){
    	$brands = $this->loadModel('Brands')->find('all')->where(['brand_status' => 1]);
    	$this->set(compact('brands'));
    }
    /**
     * get discounts for main menu
     */
    public function renderDiscountsMenu(){
    	$discounts = $this->loadModel('ProductsDiscounts')->find('all')->where(['status' => 1]);
    	$this->set(compact('discounts'));
    }
	}