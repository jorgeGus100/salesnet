<?php
	return [
	    'Session' => [
	        'defaults' => 'php',
	        'timeout'=>24*60,
	        'ini' =>[
            'session.cookie_lifetime'=>1800
        ]
	    ],
	];