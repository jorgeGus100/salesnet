<?php

use Phinx\Migration\AbstractMigration;

class CreateTestingTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->table('testing_table', ['id' => false, 'primary_key' => ['id']])
        // identity true, attribute means the field will be auto_incremented with a unique id each time a new row is created.
        // signed false, means that it’s cannot have a – or + symbol in front of it
            ->addColumn('id', 'integer', ['identity' => true, 'signed' => false])
            ->addColumn('name', 'string', ['length' => 255, 'null' => false])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->save();

        // inserting dummy data
        $this->execute('INSERT INTO testing_table (id, name, created, modified) VALUES (1, "jojo", NOW(), NOW())');
        $this->execute('INSERT INTO testing_table (id, name, created, modified) VALUES (2, "tutu", NOW(), NOW())');
        $this->execute('INSERT INTO testing_table (id, name, created, modified) VALUES (3, "lulu", NOW(), NOW())');
        $this->execute('INSERT INTO testing_table (id, name, created, modified) VALUES (4, "uy", NOW(), NOW())');
    }
    public function down(){
        $this->dropTable('testing_table');
    }
}
