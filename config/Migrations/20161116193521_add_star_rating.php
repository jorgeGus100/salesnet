<?php

use Phinx\Migration\AbstractMigration;

class AddStarRating extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $orderItems = $this->table('products');
        $orderItems->addColumn('star_rating', 'integer', array('null' => true, 'after' => 'product_description'))
                ->save();
    }

    public function down(){
        $orderItems = $this->table('products');
        $orderItems->removeColumn('star_rating')
                ->save();
    }
}
