<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersProductsRatings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $table = $this->table('users_products_ratings');
        $table->addColumn('user_id', 'integer');
        $table->addColumn('product_id', 'integer');
        $table->addColumn('rate_value', 'integer');
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addForeignKey('user_id', 'users', 'id');
        $table->addForeignKey('product_id', 'products', 'id');
        $table->create();
    }
    public function down(){
        $this->dropTable('users_products_ratings');
    }
}
