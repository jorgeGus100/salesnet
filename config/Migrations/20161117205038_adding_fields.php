<?php

use Phinx\Migration\AbstractMigration;

class AddingFields extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $table = $this->table('brands');
        $table->addColumn('brand_status', 'integer', array('default' => 1));
        $table->save();

        $categories = $this->table('categories');
        $categories->addColumn('category_status', 'integer', array('default' => 1));
        $categories->save();

        $products = $this->table('products');
        $products->addColumn('product_status', 'integer', array('default' => 1));
        $products->save();
    }
    public function down(){
        $table = $this->table('brands');
        $table->removeColumn('brand_status');
        $table->save();

        $categories = $this->table('categories');
        $categories->removeColumn('category_status');
        $categories->save();

        $products = $this->table('products');
        $products->removeColumn('product_status');
        $products->save();
    }
}
