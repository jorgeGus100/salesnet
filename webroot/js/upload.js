var Handler = (function(){
    var checkVersion = function(){
        if (typeof jQuery != 'undefined') {  
        // jQuery is loaded => print the version
        console.log(jQuery.fn.jquery);
        }
            console.log( "ready2!" );
        };

    var upload = function(){
        $("#upload").on('change', function () {
         var countFiles = $(this)[0].files.length;
         var imgPath = $(this)[0].value;
         var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
         var image_holder = $("#image-holder"); 
         image_holder.empty();
         if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof (FileReader) != "undefined") {
                 //loop for each file selected for uploaded.
                 for (var i = 0; i < countFiles; i++) {
                     var reader = new FileReader();
                     reader.onload = function (e) {
                         $("<img />", {
                             "src": e.target.result,
                                 "class": "thumb-image"
                         }).appendTo(image_holder);
                     }
                     image_holder.show();
                     reader.readAsDataURL($(this)[0].files[i]);
                 }

             } else {
                 alert("This browser does not support FileReader.");
             }
         } else {
             alert("Pls select only images");
         }
     });
     };   
 
    return{
        onReady: function(){
            checkVersion();
            upload();
        }
    };

})();
$(document).ready(Handler.onReady());
