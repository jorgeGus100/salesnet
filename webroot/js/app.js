var Handler = (function(){
	var login = function (){
		var attempValue;
		var attemps;
		$("#top-nav-login").on('submit', function (){
			$.ajax({
					url: $(this).attr('action'),
					method: 'post',
					data: $(this).serialize(),
					success: function(data) {
						data = JSON.parse(data);
						$('#top-nav-login .loading-gif').hide();
						if(data.response === true){
							console.log(data);
							
							if(data.admin === true){
								var dashBoardPage = $('#login-dropdown').find('.dashboard-page').attr('href');
								window.location.replace(dashBoardPage);
							}else{
								location.reload(true);
							}
						} else{
							if(isNaN(attempValue)){
								attempValue = 0;
							}
							if(Cookies.get('login-attemps-' + data.user)){
								attempValue = Cookies.get('login-attemps-' + data.user);
								attempValue++;
							} else{
								if(isNaN(attempValue)){
									attempValue = 0;
								}
								attempValue = 1;
							}

							Cookies.set('login-attemps-' + data.user, attempValue, { expires: 1 });
							if(data.reset === true){
									$('#login-dropdown #reset-data').fadeIn(1000).delay(3000).fadeOut(500);
								}else if(data.activation === true){
									$('#login-dropdown #activate-data').fadeIn(1000).delay(3000).fadeOut(500);
								}else{
									$('#login-dropdown #wrong-data').fadeIn(1000).delay(3000).fadeOut(500);
								
								}
							attemps = Cookies.get('login-attemps-' + data.user);
							if(attemps >= 3){
								var mainLogin = $('#login-dropdown').find('.main-login-page').attr('href');
								window.location.replace(mainLogin);
							}
						}
					},
					beforeSend: function (){
						$('#top-nav-login .loading-gif').show();
					}
				});
			return false;
		});
	};
	var messageFadeOut = function (){
		if($('.message').length){
			$('.message').delay(500).fadeIn('normal', function() {
				$(this).delay(3000).fadeOut('slow');
			});
		}
	};
	var searchBar = function (){
		$('.search-input #search').on('keyup', function (e){
			var inputSearch = $(this).val();
			var dataString = 'searchword='+ inputSearch;
			if((inputSearch != '')) {
				$.ajax({
					type: "POST",
					url: $(this).closest('form').attr('action'),
					data: dataString,
					cache: false,
					success: function(html)
					{
						html = JSON.parse(html);
						if(html.result.length){
							$('.search-section #search-results').html(html.formatting).show();
						}
					}
				});
			} else{
				$('.search-section #search-results').empty().hide();
			}
			if(e.keyCode === 27){
				$('.search-section #search-results').empty().hide(function (){
					$('.search-section .search-input .input.text input').val('');
				});
			}
			return false;
		});
	};
	var starRating = function (){
		$('form.star-form').on('submit', function (ev, ratingValue){
			$(this).find('.input.number input').val(ratingValue);
			data = $(this).serialize();
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: JSON.stringify(data),
				success: function (data){
					$('.rating-wrapper').find('.loading-gif-wrapper').hide();
				},
				beforeSend: function (e){
					$('.rating-wrapper').find('.loading-gif-wrapper').show();
				}
			});
			return false;
		});
		if($(".product-rating").length){
			$(".product-rating").starRating({
				initialRating: 0,
				starSize: 40,
				useFullStars: true,
				callback: function (currentRating, $el){
					$('form.star-form').trigger('submit', currentRating);
				}
			});
			$(".product-rating").each(function (i, ob){
				if(typeof $(ob).attr('data-readonly') !== 'undefined'){
					$(this).starRating('setReadOnly', true);
				}
			});
			if($('body').hasClass('home')){
				$(".product-rating").each(function (i, ob){
					var target = $(this).parent().find('.product-rate');
					target.append($(this));
				});
			}
		}
	};
	var addToCart = function (){
		$('.add-cart-button').on('click', function (){
			if($('.product-info .flag-auth').length === 0){
				$('form.add-cart-form').trigger('submit');
				return;
			}
			var url = $('form.add-cart-form').attr('action');
			if($('form.add-cart-form #quantity').val() === ''){
				$('form.add-cart-form .error').appendTo('form.add-cart-form .input.number').each(function (){
					$(this).show('slow');
				});
			} else{
				var data = $('form.add-cart-form').serialize();
				$.ajax({
					method: 'POST',
					url: url,
					data: data,
					success: function (data){
						$('.price .loading-gif').hide();
						if(data.indexOf('response') >= 0){
							location.reload(true);
						}
					},
					error: function (er){
						console.log(er);
					},
					beforeSend: function (){
						$('.price .loading-gif').show();
					}
				});
			}
			return false;
		});
	};
	var deleteOrderFormItem = function (){
		$('.orders.order_form .remove a').on('click', function (){
			var itemId = $(this).siblings('form').find('input').val();
			var url = $(this).siblings('form').attr('action');
			var parent = $(this).closest('.product-item');
			$.ajax({
				method: 'POST',
				url: url,
				data: $(this).siblings('form').serialize(),
				success: function (data){
					// console.log(data);
					data = JSON.parse(data);
					if(data.result === true){
						parent.find('.loading-gif').hide();
						parent.html('<div class="delete-msg callout success"><div class="row"><p>'+data.msg+'</p></div></div>');
					} else{
						parent.append('<div class="delete-msg alert"><div class="row"><p>'+data.msg+'</p></div></div>');
					}
				},
				beforeSend: function (){
					parent.find('.loading-gif').show('slow');
				}
			});
			return false;
		});
	};
	var maxValues = function (){
		$('input[type="number"]').on('keydown, keyup', function (e){
			if((typeof $(this).attr('max') !== 'undefined') && ($(this).attr('max') !== '')){
				if((parseInt($(this).val()) > parseInt($(this).attr('max'))) || ($(this).val().length > $(this).attr('max').length)){
					e.preventDefault();
					if($(this).val().indexOf('-') > -1){
						$(this).val($(this).attr('min'));
					} else{
						$(this).val($(this).attr('max'));
					}
				}
			}
			if($(this).val().indexOf('-') > -1){
				$(this).val($(this).attr('min'));
			}
		});
	};
	var updatingQuantity = function (){
		$('.update-quantity.button').on('click', function (){
			var form = $(this).closest('form');
			$.ajax({
				method: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function (data){
					form.find('.loading-gif').hide();
					data = JSON.parse(data);
					console.log(data);
					if(data.result === true){
						form.find('.success').fadeIn('slow').delay(2000).queue(function (next){
							location.reload(true);
						});
					}
				},
				beforeSend: function (){
					form.find('.loading-gif').show();
				}
			});
			return false;
		});
	};
	var zoom = function (){
		if($('.products.guest_view').length || $('.product-images .main').length){
			$('.product-images .main').zoom({
				magnify: 1
			});
		}
		if($('.reveal.form').length){
			$('.reveal.form .add-product .image').zoom({
				magnify: 1
			});
		}
	};
	var spinners = function (){
		if($('input[type="number"]').length){
			$('input[type="number"]').spinner();
		}
	};
	var checkEmptyRate = function (){
		if($('.product-rate').length){
			$('.product-rate').each(function (i, val){
				if(isEmpty($(this))){
					$(this).css('margin-bottom', '-4px');
				}
			});
		}
		function isEmpty( el ){
			return !$.trim(el.html())
		}
	};
	var sanitizeQuantityInput = function (){
		if($('input[type="number"]').length){
			$('input[type="number"]').on('change keyup', function() {
				var sanitized = $(this).val().replace(/[^0-9]/g, '');
				$(this).val(sanitized);
			});
		}
	};
	var cartSticky = function (){
		if($('#cart-sticky').length){
		function showing(){
			$('#cart-sticky').show();
		};
		function hidding(){
			$('#cart-sticky').hide();
		};
		var inview = new Waypoint.Inview({
			element: $('#header #cart')[0],
			enter: function(direction) {
				hidding();
			},
			entered: function(direction) {
				hidding();
			},
			exit: function(direction) {
				// showing();
			},
			exited: function(direction) {
				showing();
			}
		});
		}
	};
	var manageProductsUI = function (){
		if($('.manage.admDash').length){
			$('form .large-6 p span').on('click', function (){
				$(this).parent().next('div').toggle('blind');
			});
		}
	};
	var toggleAllCheckboxes = function (){
		if($('.manage').length){
			$('.manage .options p .check-all').on('click', function (){
				if($(this).is(':checked')){
					$(this).parent().next().find('input[type="checkbox"]').each(function (){
						$(this).prop('checked', true);
					});
				} else{
					$(this).parent().next().find('input[type="checkbox"]').each(function (){
						$(this).prop('checked', false);
					});
				}
			});
			$('.manage .options').each(function (i, val){
				var optionsArray = [];
				var parentBox = $(this);
				$(this).find('.input.select input[type="checkbox"]').each(function (i, val){
					optionsArray.push($(this).prop('checked'));
				});
				$(optionsArray).each(function (i, val){
					if(val === true){
						parentBox.find('.check-all').prop('checked', true);
					} else{
						parentBox.find('.check-all').prop('checked', false);
					}
				});
			});// options
		}
	};

	return {
		onReady: function(){
			$(document).foundation();
			login();
			messageFadeOut();
			searchBar();
			starRating();
			addToCart();
			deleteOrderFormItem();
			maxValues();
			updatingQuantity();
			zoom();
			spinners();
			checkEmptyRate();
			sanitizeQuantityInput();
			cartSticky();
			manageProductsUI();
			toggleAllCheckboxes();
		}
	};
})();
$(document).ready(Handler.onReady());