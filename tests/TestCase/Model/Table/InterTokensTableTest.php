<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InterTokensTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InterTokensTable Test Case
 */
class InterTokensTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InterTokensTable
     */
    public $InterTokens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.inter_tokens',
        'app.users',
        'app.roles',
        'app.internalfunctions',
        'app.internalfunctions_roles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InterTokens') ? [] : ['className' => 'App\Model\Table\InterTokensTable'];
        $this->InterTokens = TableRegistry::get('InterTokens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InterTokens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
