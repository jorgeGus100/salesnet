<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductsPicsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductsPicsTable Test Case
 */
class ProductsPicsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductsPicsTable
     */
    public $ProductsPics;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.products_pics',
        'app.products',
        'app.brands',
        'app.kardexes',
        'app.products_discounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductsPics') ? [] : ['className' => 'App\Model\Table\ProductsPicsTable'];
        $this->ProductsPics = TableRegistry::get('ProductsPics', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductsPics);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
