<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InternalfunctionsRolesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InternalfunctionsRolesTable Test Case
 */
class InternalfunctionsRolesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InternalfunctionsRolesTable
     */
    public $InternalfunctionsRoles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.internalfunctions_roles',
        'app.roles',
        'app.users',
        'app.internalfunctions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InternalfunctionsRoles') ? [] : ['className' => 'App\Model\Table\InternalfunctionsRolesTable'];
        $this->InternalfunctionsRoles = TableRegistry::get('InternalfunctionsRoles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InternalfunctionsRoles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
