<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProformasItemTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProformasItemTable Test Case
 */
class ProformasItemTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProformasItemTable
     */
    public $ProformasItem;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.proformas_item',
        'app.proformas',
        'app.users',
        'app.roles',
        'app.internalfunctions',
        'app.internalfunctions_roles',
        'app.preferences',
        'app.sellers',
        'app.products',
        'app.brands',
        'app.kardexes',
        'app.products_discounts',
        'app.discounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProformasItem') ? [] : ['className' => 'App\Model\Table\ProformasItemTable'];
        $this->ProformasItem = TableRegistry::get('ProformasItem', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProformasItem);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
