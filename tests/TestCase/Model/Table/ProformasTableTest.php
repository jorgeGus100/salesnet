<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProformasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProformasTable Test Case
 */
class ProformasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProformasTable
     */
    public $Proformas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.proformas',
        'app.users',
        'app.roles',
        'app.internalfunctions',
        'app.internalfunctions_roles',
        'app.preferences',
        'app.sellers',
        'app.proformas_item'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Proformas') ? [] : ['className' => 'App\Model\Table\ProformasTable'];
        $this->Proformas = TableRegistry::get('Proformas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Proformas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
