<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InternalfunctionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InternalfunctionsTable Test Case
 */
class InternalfunctionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InternalfunctionsTable
     */
    public $Internalfunctions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.internalfunctions',
        'app.roles',
        'app.internalfunctions_roles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Internalfunctions') ? [] : ['className' => 'App\Model\Table\InternalfunctionsTable'];
        $this->Internalfunctions = TableRegistry::get('Internalfunctions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Internalfunctions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
