<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ProformasItemController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ProformasItemController Test Case
 */
class ProformasItemControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.proformas_item',
        'app.proformas',
        'app.users',
        'app.roles',
        'app.internalfunctions',
        'app.internalfunctions_roles',
        'app.preferences',
        'app.sellers',
        'app.products',
        'app.brands',
        'app.kardexes',
        'app.products_discounts',
        'app.discounts'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
