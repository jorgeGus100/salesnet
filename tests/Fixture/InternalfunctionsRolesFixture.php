<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InternalfunctionsRolesFixture
 *
 */
class InternalfunctionsRolesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'roles_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'internalfunctions_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'FK_internalfunctions_roles_roles' => ['type' => 'index', 'columns' => ['roles_id'], 'length' => []],
            'FK_internalfunctions_roles_internalfunctions' => ['type' => 'index', 'columns' => ['internalfunctions_id'], 'length' => []],
        ],
        '_constraints' => [
            'FK_internalfunctions_roles_internalfunctions' => ['type' => 'foreign', 'columns' => ['internalfunctions_id'], 'references' => ['internalfunctions', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_internalfunctions_roles_roles' => ['type' => 'foreign', 'columns' => ['roles_id'], 'references' => ['roles', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'roles_id' => 1,
            'internalfunctions_id' => 1
        ],
    ];
}
